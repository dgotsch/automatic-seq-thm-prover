<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


foreach( array('Thue-Morse' => $TM,  'Rudin-Shapiro' => $RS, 'Period-Doubling' => $PD,  'Paper-folding' => $PF  ) as $name => $seq ) {
	echo $name . ":\n";



/*
a word is rich if every nonempty prefix has a nonempty palindromic suffix that appears only once in that prefix.


x[i..i+n-1] is rich if for all j, 1 <= j <= n, there exists
        t, 0 <= t < j such that x[i+t..i+j-1] is a palindrome
        and if x[i+t..i+j-1] = x[i'..i'+j-t-1] for i <= i' <= i+t
        then i' = i+t
*/

	$filename = 'Palindrome' . '_' . $name;
	$p = process_request(
		$filename, 
		$seq, 
		array('i', 'n'),
		array('\forall', 'k', 
			array('\or', 
				array('>=', '2*k', 'n'),
				array('\out=', 'i+k', 'i+n-k-1')
			)
		)
	);

	//show_and_save($p, $filename);
	
	$filename = 'Rich' . '_' . $name;
	$r = process_request(
		$filename, 
		$seq, 
		array('i', 'n'),
		array('\and',
			array('>', 'n', '0'),
			array('\forall', 'j', 
				array('\or', 
					array('>', 'j', 'n'),
					array('<', 'j', '1'),
					array('\exists', 't',
						array('\and',
							array('<', 't', 'j'),	
							array('\machine', array('i+t', 'j-t'), $p),
							array('\forall', 'l',
								array('\or', 
									array('>=', 'l', 't'),
									array('\not', array('\factor', 'j-t' ,'i+t', 'i+l'))
								)
							)						
						)
					)
				)
			)
		)
	);
	
	$filename = 'Rich-Distinct' . '_' . $name;
	$d = process_request(
		$filename, 
		$seq, 
		array('n', 'i'),
		array('\and',
			array('\machine', array('i', 'n'), $r),
			array('\not',
				array('\exists', 'j', 
					array('\and', 
						array('<', 'j', 'i'),
						array('\factor', 'n', 'i', 'j')
					)
				)
			)
		)
	);

	//show_and_save($d, $filename,'png');
	
	$m =brzozowski(fsa_reverse($d));

	$A = fsa_matrix($m, 0, 2);
	echo "v:[".count($A['v'])."]\n";
	vector_write($A['v']);
	echo "w:[".count($A['w'])."]\n";
	vector_write($A['w']);
	echo "M_0:\n";
	matrix_write($A['0']);
	echo "M_1:\n";
	matrix_write($A['1']);
	
}



?>
