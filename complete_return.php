<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


foreach( array('Thue-Morse' => $TM,  'Rudin-Shapiro' => $RS, 'Period-Doubling' => $PD,  'Paper-folding' => $PF  ) as $name => $seq ) {
	echo $name . ":\n";



/*
"y is a complete return to x" means that y begins with x, ends with x (possibly overlapping), and x occurs only twice in y.


        { (i, n, j, m) : x[j..j+m-1] is a complete return to
        x[i..i+n-1] and is the first occurrence of that factor }
*/

	$filename = 'Complete-Return' . '_' . $name;
	$c = process_request(
		$filename, 
		$seq, 
		array('i', 'm', 'j', 'n'),
		array('\and',
			array('\factor', 'm', 'i', 'j'),
			array('\factor', 'm', 'i', 'j+n-m'),
			array('\forall', 'k', 
				array('\or', 
					array('>=', 'k', 'n-m'),
					array('=', 'k', '0'),
					array('\not', array('\factor', 'm', 'i', 'j+k'))
				)
			)
		)
	);

	

	$filename = 'Complete-Return-Distinct' . '_' . $name;
	$d = process_request(
		$filename, 
		$seq, 
		array('i', 'm', 'j', 'n'),
		array('\and',
			array('\machine', array('i', 'm' ,'j', 'n'), $c),
			array('\not',
				array('\exists', 'l', 
					array('\and', 
						array('<', 'l', 'j'),
						array('\factor', 'n', 'l', 'j')
					)
				)
			)
		)
	);
	
	
	$filename = 'Complete-Return-Check' . '_' . $name;
	$d = process_request(
		$filename, 
		$seq, 
		array('m'),
		array('\forall', 'i',
			array('\exists', 'a',
			array('\exists', 'b',
			array('\exists', 'c',
			array('\exists', 'd',			
				array('\and',
					array('\exists', 'n',
						array('\machine', array('i', 'm' ,'a', 'n'), $d)
					),
					array('\exists', 'n',
						array('\machine', array('i', 'm' ,'b', 'n'), $d)
					),
					array('\exists', 'n',
						array('\machine', array('i', 'm' ,'c', 'n'), $d)
					),
					array('\exists', 'n',
						array('\machine', array('i', 'm' ,'d', 'n'), $d)
					),					
					array('<' , 'a' , 'b'),
					array('<' , 'b' , 'c'),
										array('<' , 'c' , 'd'),
					array('\forall', 'z',
						array('\or', 
							array('=', 'z', 'a'),
							array('=', 'z', 'b'),
							array('=', 'z', 'c'),
							array('=', 'z', 'd'),
							array('\not',
								array('\exists', 'n',
									array('\machine', array('i', 'm' ,'z', 'n'), $d)
								)
							)
						)
					)
				)
			)
			)
			)
			)
		)
	);


	show_and_save($d, $filename);
continue;
	
	$m =brzozowski(fsa_reverse($d));

	$A = fsa_matrix($m, array(0,1), 2);
	echo "v:[".count($A['v'])."]\n";
	vector_write($A['v']);
	echo "w:[".count($A['w'])."]\n";
	vector_write($A['w']);
	echo "M_0,0:\n";
	matrix_write($A['0,0']);
	echo "M_0,1:\n";
	matrix_write($A['0,1']);
	echo "M_1,0:\n";
	matrix_write($A['1,0']);
	echo "M_1,1:\n";
	matrix_write($A['1,1']);

	continue;

/*
Another thing we could try is figuring out how long the complete
returns are.  To do this we find the matrices

        { (m, n) :  there exist i, j such that
         x[j..j+n-1] is a complete return to x[i..i+m-1] }

I think this should allow us to show it is bounded by 6m-2.
*/

	


	//show_and_save($d, $filename,'png');

	
	
}



?>
