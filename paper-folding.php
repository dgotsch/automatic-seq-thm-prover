<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');



$PaFold = FSA::from_array(array(
	'start' => array('0'),
	'final' => array('0','1','2','3','4'),
	'delta' => array(
		'0' => array('0,0' => array('0'), '0,1' => array('0'), '1,0' => array('1'), '1,1' => array('2')),
		'1' => array('0,0' => array('4'), '0,1' => array('4'), '1,0' => array('3'), '1,1' => array('3')),
		'2' => array('0,0' => array('3'), '0,1' => array('3'), '1,0' => array('4'), '1,1' => array('4')),
		'3' => array('0,0' => array('3'), '0,1' => array('3'), '1,0' => array('3'), '1,1' => array('3')),
		'4' => array('0,0' => array('4'), '0,1' => array('4'), '1,0' => array('4'), '1,1' => array('4'))
	),
	'output' => array('0'=>'X', '1'=>'0', '2'=>'1', '3'=>'1', '4'=>'0' ),
	'k' => 2,
	'w' => 2,
	'ks' => array(2 ,'2$'),
	'mask' => array('+1', '')
));

//fsa_visualize($PaFold);

/*
$PaFold = FSA::from_array(array(
	'start' => array('0'),
	'final' => array(),
	'delta' => array(
		'0' => array('0,0' => array('1'), '0,1' => array('2'), '1,0' => array('0'), '1,1' => array('0')),
		'1' => array('0,0' => array('3'), '0,1' => array('3'), '1,0' => array('4'), '1,1' => array('4')),
		'2' => array('0,0' => array('4'), '0,1' => array('4'), '1,0' => array('3'), '1,1' => array('3')),
		'3' => array('0,0' => array('3'), '0,1' => array('3'), '1,0' => array('3'), '1,1' => array('3')),
		'4' => array('0,0' => array('4'), '0,1' => array('4'), '1,0' => array('4'), '1,1' => array('4'))
	),
	'output' => array('0'=>'0', '1'=>'0', '2'=>'1', '3'=>'0', '4'=>'1' ),
	'w' => 2,
	'ks' => array(2 ,2),
	'k' => 2
));*/



$positionOfDifference = FSA::from_array(array(
	'start' => array('0'),
	'final' => array('1'),
	'delta' => array(
		'0' => array('0,0,0' => array('0'), '1,1,0' => array('0'),'0,1,1' => array('1'), '1,0,1' => array('1')),
		'1' => array('0,0,0' => array('1'), '1,1,0' => array('1'),'0,1,0' => array('1'), '1,0,0' => array('1'))
	),
	'sigma' => array('0,0,0', '0,0,1', '0,1,0', '0,1,1', '1,0,0', '1,0,1', '1,1,0', '1,1,1')
));

//fsa_visualize($positionOfDifference);

$TERNARY_LOGIC = true;
$TERNARY_ZEROES = true;

foreach( array('General-Paper-Folding' => $PaFold) as $name => $seq ) {
	echo $name . ":\n";
//$REUSE = false;

	$filename = 'Recurrence' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('f$','n','i','j'),

		array('\or', 
			array('<=','i','48*n'),
//			array('\not',
				array('\exists', 't',
					array('\and', 
						array('\forall', 'k',
							array('\or',
								array('>=', 'k', 'n'),
								array('\out=', array('i+k+t','f$'), array('j+k','f$'))
							)
						),
						array('>=', 'm', 't+n'),
					)
				)
//			)
		)
	);

	for($i=0; $i<30; $i++) {
			for($j=0; $j<13*$i; $j++ ){
				//$tape = make_tape(array($i,$j),2); 
				//print_r($tape); continue;

			 	$ans = dfa_accepts ($d, array($i,$j),  2 );
				if ($ans['accept']) echo decbin($i). ":". $i.",".$j.";\n";

			}
		}

	show_and_save($d, $filename);
	exit;
/*$filename = 'AppearanceMin' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n$'),
		array('\not',
			array('\exists', 'i', //array('<=', 'i', '2*n'),
				array('\not',
					array('>', '2*n$','i')
				)
			)
		)
	);
	fsa_visualize($d);
	
	exit;*/

	$filename = 'AppearanceMin' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','m'),

		array('\min', 'm',
			array('\exists', 'f$',
				array('\forall', 'i',
					array('\or', 
						array('<=','i','48*n'),
						array('\exists', 'j',
							array('\and', 
								array('\forall', 'k',
									array('\or',
										array('>=', 'k', 'n'),
										array('\out=', array('i+k','f$'), array('j+k','f$'))
									)
								),
								array('>=', 'm', 'n+j')
							)
						)
					)
				)
			)
		)
	);


/*
	$filename = 'AppearanceMin' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','m'),//, 'i', 'f$'),
		//array('\min','m',
		array('\exists', 'f$',
			array('\forall', 'i', array('<=', 'i', '12*n'),
				array('\or', 
					
					array('\and', 
						array('=', 'n','3'),
						//array('=', 'm','24'),
						array('\exists', 'j', array('<=', 'j+n', 'm'),
							array('\forall', 'k', array('<', 'k', 'n'),
								array('\out=', array('i+k','f$'), array('j+k','f$'))
							)
						)
					)
				)
			)
		)
	//	)
	);
*/
		for($i=0; $i<30; $i++) {
			for($j=0; $j<13*$i; $j++ ){
				//$tape = make_tape(array($i,$j),2); 
				//print_r($tape); continue;

			 	$ans = dfa_accepts ($d, array($i,$j),  2 );
				if ($ans['accept']) echo decbin($i). ":". $i.",".$j.";\n";

			}
		}

	show_and_save($d, $filename);
	exit;

	$filename = 'AppearanceMinConfirm' . '_' . $name;
	$m = process_request( 
		$filename,
		$seq, 
		array('n','p'),
		array('\exists', 't',
			array('\exists', 'm',
				array('\and',
					array('\machine', array('n','m'), $d),
					array('>=', 'n', 7),
					array('\machine', array('n','t'), $ceil_log2),
					array('=', 'p+4*t+n', 'm+1')
				)
			)
		)
	);
show_and_save($m, $filename);

exit;

	$filename = 'Appearance' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','i'),
		array('\forall', 'f$',
			array('\or', 
				array('\exists', 't',
					array('\and', 
						array('\machine', array('n','t'), $ceil_log2),
						//array('<', 'n', '3'),
						array('\exists', 'j',
							array('\and',
								array('<=', 'j', '6*t'),
								array('\forall', 'k',
									array('\or',
										array('>=', 'k', 'n'),
										array('\out=', array('i+k','f$'), array('j+k','f$'))
									)
								)
							)
						)
					)
				)
			)
		)
	);
	$d = fsa_accept_trailing_zeros($d);
	$d = fsa_minimize($d);
	show_and_save($d, $filename);




	$filename = 'AlmostSquares' . '_' . $name;
	$d2 = process_request(
		$filename, 
		$seq, 
		array('n'),
		array('\forall', 'f$',
			array('\exists', 'i',
				array('\and',
					array('>','n','0'),
					array('\forall', 'k',
						array('\or',
							array('>=', 'k', 'n'),
							array('\out=', array('i+k','f$'), array('i+n+1+k','f$'))
						)
					)
				)
			)
		)
	);
	$d2 = fsa_accept_trailing_zeros($d2);
	$d2 = fsa_minimize($d2);
	show_and_save($d2, $filename);
	
	

	$filename = 'Cubes+' . '_' . $name;
	$d2 = process_request(
		$filename, 
		$seq, 
		array('n'),
		array('\exists', 'f$',
			array('\exists', 'i',
				array('\and',
					array('>','n','0'),
					array('\forall', 'k',
						array('\or',
							array('>', 'k', '2*n'),
							array('\out=', array('i+k','f$'), array('i+n+k','f$'))
						)
					)
				)
			)
		)
	);
	$d2 = fsa_accept_trailing_zeros($d2);
	$d2 = fsa_minimize($d2);
	show_and_save($d2, $filename);
	
	
	$filename = 'Cubes' . '_' . $name;
	$d2 = process_request(
		$filename, 
		$seq, 
		array('n'),
		array('\exists', 'f$',
			array('\exists', 'i',
				array('\and',
					array('>','n','0'),
					array('\forall', 'k',
						array('\or',
							array('>=', 'k', '2*n'),
							array('\out=', array('i+k','f$'), array('i+n+k','f$'))
						)
					)
				)
			)
		)
	);
	//'_withzeroes'
	$d2 = fsa_accept_trailing_zeros($d2);
	$d2 = fsa_minimize($d2);
	show_and_save($d2, $filename);
	
	

	$filename = 'Squares' . '_' . $name;

	$exp =array('\exists', 'f$',
			array('\exists', 'i',
				array('\and',
					array('>','n','0'),
					array('\forall', 'k',
						array('\or',
							array('>=', 'k', 'n'),
							array('\out=', array('i+k','f$'), array('i+n+k','f$'))
						)
					)
				)
			)
		);
	
	$vars =	array('n');


	$d2 = process_request(
		$filename, 
		$seq, 
		$vars,
		$exp
	);
	show_and_save($d2, $filename.'_withzeroes');
	$d2 = fsa_accept_trailing_zeros($d2);
	$d2 = fsa_minimize($d2);
	show_and_save($d2, $filename);

	exit;



	$d = process_request( 
		$filename,
		$seq, 
		array('f$','g$'),
		array('\exists', 'p',
		//array('\min', 'p',
			array('\and', 
				array('\machine', array('f$','g$','p'), $positionOfDifference),
				array('\not',
					array('\exists', 'i',
						array('\exists', 'j',
							array('\and',
								array('<', 'i', 'j'),	
								array('<', 'j', '168*p'),	
								array('\forall', 'k',
									array('\or',
										array('>=', 'k', '14*p'),
										array('\out=', array('i+k','f$'), array('j+k','g$'))
									)
								)
							)
						)
					)
				)
			)
		)
	);
//fsa_visualize($d);
	show_and_save($d, $filename);//*/

	$filename = 'Appearance' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','i'),
		array('\forall', 'f$',
			array('\or', 
				array('\exists', 't',
					array('\and', 
						array('\machine', array('n','t'), $ceil_log2),
						//array('<', 'n', '3'),
						array('\exists', 'j',
							array('\and',
								array('<=', 'j', '6*t'),
								array('\forall', 'k',
									array('\or',
										array('>=', 'k', 'n'),
										array('\out=', array('i+k','f$'), array('j+k','f$'))
									)
								)
							)
						)
					)
				)
			)
		)
	);

	fsa_visualize($d);
	show_and_save($d, $filename);

	$filename = 'AppearanceNeg' . '_' . $name;
	$u = process_request( 
		$filename,
		$seq, 
		array('n'),
		array('\exists', 'f$',
			array('\exists', 't',
				array('\and', 
					array('\machine', array('t'), $powerOfTwo),
					array('>=', 't', 'n'),
					array('<', 't', '2*n'),
					array('\exists', 'i',
						array('\and', 
							array('=','i+1', '6*t'),
							array('\forall', 'j',
								array('\and',
									array('<', 'j', 'i'),
									array('<=', '1', 'j'),
									array('\not', 
										array('\forall', 'k',
											array('\or',
												array('>=', 'k', 'n'),
												array('\out=', array('i+k','f$'), array('j+k','f$'))
											)
										)
									)
								)
							)
						)
					)
				)
			)
		)
	);
	$u = fsa_accept_trailing_zeros($u);
	fsa_visualize($u);

	$e= factor_count2D($seq, $name);
	fsa_visualize($e);

}

function factor_count2D($seq , $name, $bypass =false) {

	if( $bypass ) {
		$d = $bypass;
		$filename = $name;
	} else {
		$filename = 'FactorCount' . '_' . $name;
		$d = process_request( 
			'UniqueFactors' . '_' . $name,
			$seq, 
			array('f$', 'n', 'j'),
			array('\and',
				array('\not',
					array('\exists', 'l',
						array('\and',
							array('<', 'l', 'j'),
							array('\forall', 'k',
								array('\or',
									array('>=', 'k', 'n'),
									array('\out=', array('l+k','f$'), array('j+k','f$'))
								)
							)
						)
					)
				),
			//	array('=', 'n', '2')
			)
		);
	}

	//fsa_visualize($d); exit;
	$r = process_request( 
		"_temp_R".$filename,
		$seq, 
		array('f$', 'n', 's', 'e', 'l'), 
		array('\or', 
			array('\and',
				array('>', 'l', '0'),
				array('<=', 's', 'e'),
				array('\not', 
					array('\machine', array('f$','n','e'), $d) 
				),
				array('\exists', 'b',
					array('\and',
						array('\or', 
							array('=', 'b', '0'),
							array('\exists', 'a',
								array('\and',
									array('=', 'a+1', 'b'),
									array('\not', 
										array('\machine', array('f$','n','a'), $d) 
									)
								)
							)
						),
						array('<=', 's', 'b'),
						array('=', 'b+l', 'e'),
						array('\forall', 'i',
							array('\or',
								array('<', 'i','s'),
								array('\and',
									array('>=', 'i','s'),
									array('<', 'i','b'),
									array('\not', 
										array('\machine', array('f$','n','i'), $d) 
									)
								),
								array('\and',
									array('>=', 'i','b'),
									array('<', 'i','e'),
									array('\machine', array('f$','n','i'), $d) 
								),
								array('>=', 'i', 'e')
							)
						)
					)
				)
			),
			array('\and',
				array('=', 's', 'e'),
				array('=', 'l', '0'),
				array('\forall', 'i',
					array('\or',
						array('<', 'i', 's'),
						array('\not', 
							array('\machine', array('f$','n','i'), $d) 
						)

					)
				)
			)
		)
	);
	//fsa_visualize($r);

	$m = process_request( 
		"_temp_M_0".$filename,
		$seq, 
		array('f$', 'n', 'l', 'e'), 
		array('\and',
			array('=', 'l', '0'),
			array('=', 'e', '0')
		)
	);
	//fsa_visualize($m);
	$n = $m;

	for($i = 1; 1; $i++ ){
		$c = process_request( 
			"_temp_M_".$i.$filename,
			$seq, 
			array('f$','n', 'l', 'e'), 
			array('\exists', 's',
				array('\exists', 'k',
					array('\and',
						array('\machine',  array('f$', 'n','s','e','k'), $r),
						array('\exists', 'j',
							array('\and',
								array('=', 'k+j', 'l'),
								array('\machine', array('f$','n','j','s'), $m)
							)
						)
					)
				)
			)
		);

		echo "Computed iteration #". $i ."\n";
		if (fsa_equal($c, $m)) break;
		$m = $c;
		
	}
	echo "done.\n";

	$e = process_request( 
		$filename,
		$seq, 
		array( 'n', 'l'), 
		array('\forall', 'f$',
			array('\exists', 'e',
				array('\and',
					//array('=','n','12'),
					array('\machine', array('f$','n','l','e'), $m)
				)
			)
		)
	); 

	for($i=0; $i<30; $i++) {
		for($j=0; $j<13*$i; $j++ ){
			//$tape = make_tape(array($i,$j),2); 
			//print_r($tape); continue;

		 	$ans = dfa_accepts ($e, array($i,$j),  2 );
			if ($ans['accept']) echo decbin($i). ":". $i.",".$j.";\n";

		}
	}


	return $e;
}




?>
