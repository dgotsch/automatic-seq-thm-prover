#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Regularity of rational represantations of powers of two.
\end_layout

\begin_layout Author
Daniel Goc
\end_layout

\begin_layout Standard
Construct a DFA 
\begin_inset Formula $M$
\end_inset

 that only accepts input 
\begin_inset Formula $\frac{u}{w}$
\end_inset

 if 
\begin_inset Formula 
\[
u\notin\left\{ 0^{*}1^{*}0^{*}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
and 
\begin_inset Formula 
\[
w\in\left\{ 0^{*}1^{*}0^{*}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
Given a rational number 
\begin_inset Formula $r=\frac{a}{b}$
\end_inset

 we can express it in the form 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{x}{0^{*}1^{m}0^{n}}
\]

\end_inset

 as mentioned before.
 
\end_layout

\begin_layout Standard
Suppose 
\begin_inset Formula $r$
\end_inset

 is a rational for which when expressed this way the numerator 
\begin_inset Formula $x$
\end_inset

 is always of the form 
\begin_inset Formula $0^{*}1^{*}0^{*}$
\end_inset

 
\end_layout

\begin_layout Standard
Let 
\begin_inset Formula 
\[
\frac{0^{a}1^{b}0^{c}}{0^{a'}1^{b'}0^{c'}}
\]

\end_inset


\end_layout

\begin_layout Standard
be one such representation of r.
\end_layout

\begin_layout Standard
We multiply the numerator and denominator by 
\begin_inset Formula 
\[
2^{b'}+1
\]

\end_inset

 (which is 
\begin_inset Formula $10^{b'-1}1$
\end_inset

in binary) to get another representation of 
\begin_inset Formula $r$
\end_inset

 this time of the form:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{0^{a}y0^{c}}{0^{a'}1^{2b'}0^{c'}}
\]

\end_inset


\end_layout

\begin_layout Standard
where y is the binary expansion of 
\begin_inset Formula $\left(2^{b}-1\right)\cdot\left(2^{b'}+1\right)$
\end_inset

.
\end_layout

\begin_layout Standard
(Note that the denominator is still of the form 
\begin_inset Formula $0^{*}1^{*}0^{*}$
\end_inset

.)
\end_layout

\begin_layout Standard
In order to have 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula 
\[
0^{a}y0^{c}\in\left\{ 0^{*}1^{*}0^{*}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
we need 
\begin_inset Formula $b'=b$
\end_inset

.
 Therefore, r is a power of 2 (positive or negative).
\end_layout

\begin_layout Standard
Thus, 
\begin_inset Formula $M$
\end_inset

 accepts all rational numbers except any power of 2.
\end_layout

\end_body
\end_document
