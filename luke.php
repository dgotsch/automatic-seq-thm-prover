<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');



// Critical exponent

$expr = parse_expr('
(\min, p,
	(\and, 
		(>, p, 0),
		(\exists, i,
			(\factor, l-p, i, i+p)
		)
	)
)
');



foreach( array('Baum-Sweet'=>$BS, 'Mephisto-Waltz'=>$MW, 'Thue-Morse' => $TM, 'Paper-folding' => $PF, 'Rudin-Shapiro' => $RS,  'Period-Doubling' => $PD) as $name => $seq ) {
	$filename = 'Luke-CriticalExponent' . '_' . $name;


	$d = process_request( 
		$filename,
		$seq, 
		array('p', 'l'),
		$expr
	);

		for($i=0; $i<100; $i++) {
			for($j=0; $j<=4*$i; $j++ ){
			 	$ans = dfa_accepts ($d, array($i,$j),  $d->k );
				if ($ans['accept']) echo   $i.",".$j.";\n";
			}
		}

	echo 'results/' . $filename . ".fsa\n";
	//fsa_save($d, 'results/' . $filename . ".fsa");
//display_ratio($filename, $d); 
	show_and_save($d, $filename); exit;
}
exit;
//fsa_visualize($PD);

foreach( array( 'Thue-Morse' => $TM,'Rudin-Shapiro' => $RS,'Period-Doubling' => $PD,  'Paper-folding' => $PF) as $name => $seq ) {
	echo $name . ":\n";


	$filename = 'Luke-dfa-for-talk' . '_' . $name;

;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','i','j'),
		array('\factor', 'n', 'i', 'j')
	);



	fsa_visualize($d, 'results/'.$filename.'.dot');
	shell_exec('dot -Tpng "results/'.$filename.'.dot" -o'.$filename.'.png');

}
exit;



$filename = 'UniqueZ_Luke';
$m = fsa_load('luke/'.$filename.'.fsa');

$f = fsa_load('luke/format.fsa');


$c = fsa_load('luke/carry.fsa');

//fsa_visualize ($c);

$m = fsa_minimize($m, 3, 2);

$e = fsa_accept_trailing_zeros($m, 3);

echo fsa_equal($m, $e) ? "They are equal\n": "Not Equal\n"; 
/*fsa_write($m);
fsa_visualize ($m);*/

//$REUSE = false;
$d = process_request( 
	$filename,
	$TM, 
	array('x','y','z'),
	array('\exists', 'b',
			array('\and',
				array('\not', array('=', 'z', 'b')),
				array('\machine',  array('x','y','z'), $e),
				array('\machine',  array('x','y','b'), $e)
			)
		)
);
//fsa_visualize ($d);


$a = process_request( 
	$filename,
	$TM, 
	array('x','y'),
	array('\or',
		array('\not',
			array('\and',
				array('\machine', array('x'), $f),
				array('\machine', array('y'), $f)
			)
		),
		array('\exists', 'z',
			array('\and',
				array('\machine', array('z'), $f),
				array('\machine', array('x','y','z'), $c)
			)
		)
	)
);

//fsa_visualize ($a);


$f9 = fsa_load('luke/format9.fsa');
$s = fsa_load('luke/sum.fsa');
$d = fsa_load('luke/diff.fsa');


//M(z) <=> there exist x, y : F(x) and F(y) and S(x, y, z)

$m = process_request( 
	'Luke_M',
	28, 
	array('z'),
	array('\exists', 'x',
		array('\exists', 'y',
			array('\and',
				array('\machine', array('x'), $f9),
				array('\machine', array('y'), $f9),
				array('\machine', array('x', 'y', 'z'), $s)
			)
		)
	)
);

fsa_save($m, 'luke/m.fsa');
fsa_visualize ($m);


//P(r,t) <=> there exists s : F(s) and D(r,s,t) and M(r)

$p = process_request( 
	'Luke_M',
	28, 
	array('r', 't'),
	array('\exists', 's',
		array('\and',
			array('\machine', array('s'), $f9),
			array('\machine', array('r'), $m),
			array('\machine', array('r', 's', 't'), $d)
		)
	)
);

fsa_save($p, 'luke/p.fsa');

fsa_save($p2, 'luke/p_nodead.fsa');
fsa_visualize ($p2);

?>
