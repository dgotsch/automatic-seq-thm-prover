<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');
include('src/fsa_interface_grail.php');

$REUSE= false;



// weird cantor set
/*
$CS_msd = DFAO_builder( array('1' => '1001', '0' => '0000'));
$CS = DFAO_reverse( $CS_msd );
	$seq = $CS;
	
	$expr = parse_expr('
		(\out=, 1, i)
	');


	$d = process_request( 
		$filename,
		$seq, 
		array('i'),
		$expr
	);

	//fsa_visualize($d);
	show_and_save($d, $filename);
	for( $i=0; $i<200; $i++ ) {
			//print_r( make_tape(array($i) ,3));
			$ans = dfa_accepts($seq, array($i), 4);
			echo $ans['output'];
		}*/


$RESTIVO = FSA::from_array( array(
	'start' => array('0'),
	'final' => array('0','1','2','3','4','5'),
	'delta' => array(
		'0' => array('0' => array('1'), '1' => array('2') ),
		'1' => array('0' => array('1'), '1' => array('1') ),
		'2' => array('0' => array('3'), '1' => array('4') ),
		'3' => array('0' => array('2'), '1' => array('5') ),
		'4' => array('0' => array('5'), '1' => array('2') ),
		'5' => array('0' => array('5'), '1' => array('5') )
	),
	'output' => array('0'=>'0', '1'=>'0', '2'=>'1', '3'=>'1', '4'=>'0', '5'=>'0' ),
	'k' => 2
));

$seq = $RESTIVO;
	/*for( $i=0; $i<200; $i++ ) {
		//print_r( make_tape(array($i) ,3));
		$ans = dfa_accepts($seq, array($i), 2);
		echo $ans['output'];
	}*/
	
/*
	$filename = 'Restivo_local_period';
	$expr = parse_expr('
	(\min, n,
		(\and,
			(>, n, 0),
			(\forall, k,
				(\or,
					(>=, k, i),
					(<, k+n, i),
					(\out=, k, k+n)
				)
			)
		)
	)
	');


	$d = process_request( 
		$filename,
		$seq, 
		array('i','n'),
		$expr
	);
	//fsa_visualize($d);
	show_and_save($d, $filename); exit;
	
	for( $i=0; $i<200; $i++ ) {
		for( $n=0; $n<=3*$i +1; $n++ ) {
			$ans = dfa_accepts($d, array($i, $n), 2);
			if ($ans['accept']) {
				echo $n .","; //echo $i.",".$n.";\n";
				continue 2;
			}
		}
		echo "#,";
	}

	$m =brzozowski(fsa_reverse($d));

	
	$A = fsa_matrix($m, 0, 2);
	//print_r($m->delta); exit;
	//print_r($A['0']);
	echo "v:\n";
	vector_write($A['v']);
	echo "w:\n";
	vector_write($A['w']);
	echo "M_0:\n";
	matrix_write($A['0']);
	echo "M_1:\n";
	matrix_write($A['1']);
	exit;

	
exit;

/*'Thue-Morse' => $TM,'Rudin-Shapiro' => $RS,'Period-Doubling' => $PD,  'Paper-folding' => $PF*/

//$PD_5n = DFAO_builder( array('0' => '01', '1' => '23', '2'=>'45', '3'=>'43', '4'=>'05', '5'=>'21'));
// fsa_visualize($PD_5n);


for( $i=0; $i<200; $i+=1 ) {
	//print_r( make_tape(array($i) ,3));
	$ans = dfa_accepts($RS, array($i), 2);
	echo $ans['output'];
}
echo "\n";
exit;

foreach( array( 'Period-Doubling' => $PD2 ) as $name => $seq ) {


	for( $index = 0; $index < 5; $index++ ) {
		
		$filename = 'Fifth_'. ($index +1) .'_of_' . '' . $name;
		echo $filename . ":\n";

		/*for( $i=0; $i<200; $i++ ) {
			//print_r( make_tape(array($i) ,3));
			$ans = dfa_accepts($seq, array($i), 2);
			echo $ans['output'];
		}*/

	
		/*$expr = parse_expr('
		(\and,
			(>=, p, 1),
			(\forall, i,
				(\or,
					(\out=, n+i, n+p+i),
					(>=, i, p)
				)
			)
		)');*/
		
		$expr = parse_expr('
	
			(\out=, 1, 5*i+'. $index .')
		');
	

		$d = process_request( 
			$filename,
			$seq, 
			array('i'),
			$expr
		);

		//fsa_visualize($d);
		show_and_save($d, $filename);
		
	}




}


?>

