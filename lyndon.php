<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');



for($i=0; $i<30; $i++) {
 	$ans = dfa_accepts ($RS, array($i),  2 );
//	print_r($ans);
	echo $ans['output'];
}
echo "\n";


$expr = parse_expr('
(\forall, s,
	(\or,
		(>=, s, i+n),
		(<=, s, i),
		(\exists, d,
			(\and,
				(<, s+d, i+n), 
				(\factor, d, i, s),
				(\out<, i+d, s+d)
			)
		)
	)
)
');

//print_r($expr); exit;

foreach( array('Thue-Morse' => $TM,  'Rudin-Shapiro' => $RS,'Thue-Morse-neg' => $TM_neg,    'Period-Doubling' => $PD,  'Paper-folding' => $PF ) as $name => $seq ) {
	echo $name . ":\n";
//$REUSE=false;
	$filename = 'Lyndon' . '_' . $name;
	$lyndon = process_request(
		$filename, 
		$seq, 
		array('n','i'),
		$expr
	);
	$filename = 'LyndonFactors' . '_' . $name;
	$d = process_request(
		$filename,
		$seq,
		array('n', 'i'),
		array('\and',
			array('\machine', array('n','i'), $lyndon),
			array('\not',
				array('\exists','j',
					array('\exists', 'm',
						array('\and', 
							array('<=', 'j', 'i'),
							array('>', 'm', 'n'),
							array('>=', 'j+m', 'i+n'),
							array('\machine', array('m','j'), $lyndon)
						)
					)
				)
			)
		)
	);
								

		for($i=0; $i<100; $i++) {
			for($n=0; $n<30*$i+10; $n++ ){

			 	$ans = dfa_accepts ($d, array($n,$i),  2 );
				if ($ans['accept']) echo decbin($i). ":". $i.",".$n.";\n";

			}
		}

	//show_and_save($d, $filename);
//continue;

	$filename = 'LyndonPrefixFactors' . '_' . $name;
	$p = process_request(
		$filename,
		$seq,
		array('l', 'i'),
		array('\exists', 'n',
			array('\and',
				array('>', 'n', '0'),
				array('<=', 'i+n', 'l'),
				array('\machine', array('n','i'), $lyndon),
				array('\not',
					array('\exists','j',
						array('\exists', 'm',
							array('\and', 
								array('<=', 'j', 'i'),
								array('>', 'm', 'n'),
								array('>=', 'j+m', 'i+n'),
								array('<=', 'j+m', 'l'),
								array('\machine', array('m','j'), $lyndon)
							)
						)
					)
				)
			)
		)
	);
	
	//show_and_save($p, $filename);
/*
	$A = fsa_matrix($m, 0, 2);
	echo "v:\n";
	vector_write($A['v']);
	echo "w:\n";
	vector_write($A['w']);
	echo "M_0:\n";
	matrix_write($A['0']);
	echo "M_1:\n";
	matrix_write($A['1']);

	continue;
//*/


	$filename = 'LyndonPrefixLastFactor' . '_' . $name;
	$last = process_request(
		$filename,
		$seq,
		array('n', 'i'),
		array('\max', 'i',
			array('\machine', array('n','i'), $p)
		)
	);
	show_and_save($last, $filename, 'eps');
	
//continue;
	$filename = 'LyndonPrefixBeforeLastFactor' . '_' . $name;
	$before = process_request(
		$filename,
		$seq,
		array('n', 'j'),
		array('\exists', 'i',
			array('\and',
				array('\machine', array('n','i'), $last),
				array('<', 'j', 'i')
			)
		)
	);
	//show_and_save($before, $filename);
	

	$filename = 'LyndonFactorSuffix' . '_' . $name;
	$before = process_request(
		$filename,
		$seq,
		array('i', 'j', 'l'),
		array('\min', 'l',
			array('\exists', 'n',
				array('\and',
					array('=', 'l+n', 'j'),
					array('<=', 'i', 'l'),
					array('<', 'l', 'j'), 
					array('\machine', array('n','l'), $lyndon)
				)
			)
		)
	);
	show_and_save($before, $filename,'eps');
	

	
continue;

	$filename = 'LyndonUnique' . '_' . $name;	
	$c = process_request(
		$filename,
		$seq,
		array('n', 'i'),
		array('\and',
			array('\machine', array('n','i'), $lyndon),
			array('\not',
				array('\exists','j',
					array('\and', 
						array('<', 'j', 'i'),
						array('\factor', 'n', 'j', 'i')
					)
				)
			)
		)
	);


	for($n=0; $n<30; $n++) {
		for($i=0; $i<80; $i++ ){
			//$tape = make_tape(array($i,$j),2); 
			//print_r($tape); continue;

		 	$ans = dfa_accepts ($c, array($n,$i),  2 );
			echo ($ans['accept']) ? '#' :' ';

		}
		echo "\n";
	}
	

	$m =brzozowski(fsa_reverse($c));
	fsa_visualize($m, 'results/'.$filename.'.dot');
continue;
	shell_exec('dot -Tpng "results/'.$filename.'.dot" -o'.$filename.'.png');

	$A = fsa_matrix($m, 0, 2);
	echo "v:\n";
	vector_write($A['v']);
	echo "w:\n";
	vector_write($A['w']);
	echo "M_0:\n";
	matrix_write($A['0']);
	echo "M_1:\n";
	matrix_write($A['1']);

//continue;						
	$filename = 'LyndonCount' . '_' . $name;
	$r = factor_count($seq , $filename, $c);
	show_and_save($r, $filename);
	
//	exit;		

}



?>
