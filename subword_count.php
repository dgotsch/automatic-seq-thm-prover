<?php
gc_enable();
include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


/*
$filename = 'partial/9b3a37add7c26245e6edfc887c304527.fsa';

//$filename = 'partial/90602d23e01ac7f6e323705f4f967ce2.fsa';
$fp = fopen($filename, 'r');
echo  "size=" .filesize($filename)  ."bytes\n";
$min = fread($fp, filesize($filename));

fclose($fp);
//$min = file_get_contents('temp2.fsa');
echo "reading\n";
gc_collect_cycles();
$time_start = microtime(true);
$d= fsa_read($min, true); 
$time_end = microtime(true);
echo  round($time_end - $time_start, 3) ."s\n";
echo count($d->delta). " states read\n";

fsa_accept_trailing_zeros($d);
exit;*/


foreach( array( 'Thue-Morse' => $TM,'Rudin-Shapiro' => $RS , 'Period-Doubling' => $PD, 'Paper-folding' => $PF) as $name => $seq ) {
	echo $name . ":\n";

	$filename = 'FactorCount' . '_' . $name;
$REUSE = false;

	
	$e = factor_count($seq, $name);
	show_and_save($e, $filename, 'eps');
	
	exit;
	if( $seq == $RS ) {

		for($i=0; $i<30; $i++) {
			for($j=0; $j<9*$i; $j++ ){
			 	$ans = dfa_accepts ($e, array($i,$j),  2 );
				if ($ans['accept']) echo decbin($i). ":". $i.",".$j.";\n";

			}
		}
		//exit;
	}
	continue;

	$r = process_request( 
		$filename."_confirmInfinite",
		$seq, 
		array('n'),
		array('\exists', 'l',
			array('\and',
				array('=', 'l+8', '8*n'),
				array('\machine', array('n','l'), $e)
			)
		)
	);
	//show_and_save($r, $filename."_confirmInfinite");



}

?>
