<?php


include("fsa_interface_vannoord.php");
include("sequences.php"); 
// constants
define('CR', 'x');

// variables
$intersection = function( $final1, $final2 ) {
	return $final1 and $final2;
};

$output_equal = function( $output1, $output2 ) {
	return ($output1 == $output2);
};

$output_lessthan = function( $output1, $output2 ) {
	return ($output1 < $output2);
};

$always_true = function( $output1, $output2 ) {
	return true;
};

function array_add(&$ar, $idx, $el) {
	if( ! isset($ar[$idx]) ){
		$ar[$idx] = array($el);
	} 
	if( ! in_array( $el, $ar[$idx]) ) {
		array_push($ar[$idx], $el);
	}
}


function state_mult( $s1, $s2, $n ) {
	assert('is_numeric($s1) && is_numeric($s2)');
	return $s1 + ($s2 * $n);
	
	return $s1 . CR . $s2;
}

function num_split( $n, $k, $w ) {
	$ret = array();
	for($i=0; $i<$w; $i++ ) {
		$ret[$i] = $n % $k;
		$n = floor( $n/ $k);
	}
	return $ret;
}

function input_implode( $ns ) {
	return implode(',', $ns);
}

function input_explode( $in ) {
	return explode(',', $in) ;
}





function fsa_comparer( $final, $k, $vars = array() ) {

	// for readability
	// < -> 0
	// = -> 1
	// > -> 2
	$state_map = array_flip(array( '<', '=', '>' )); 
	
	// create a new machine
	$m = new FSA();
	$m->k = $k;
	$m->w = 2;
	$m->vars = $vars;
	$m->states = 3;
	
	// start state
	$m->start_add( 1 );
	// final states
	foreach( $final as $f ) {
		$m->end_add( $state_map[$f] );
	}
	
	foreach( $state_map as $name => $s ) {
		$m->delta[$s] = array();
	
	
		foreach( $m->generate_sigma() as $a ) {
			$in = input_explode($a);
		
			if( $in[0] < $in[1] ) {
				$m->delta[$s][$a] = array(0);
			} else if( $in[0] == $in[1] ) {
				$m->delta[$s][$a] = array($s);
			} else {
				$m->delta[$s][$a] = array(2);
			}

		}
	}
	return $m;
}

function combiner($recipe, $k, $constant = 0) {
	$m = new FSA();
	assert( '$k != 0' );
	$m->k = $k;

	// add the start and final state
	$m->start_add( $constant );
	$m->end_add( 0 );

	$m->w = count($recipe);

	$m->states = max(array_sum($recipe) , $constant +1); //+1 is needed here because we only go to  i < total
	for($i = 0; $i < $m->states ; $i++) {
		$s= $i;
		$m->delta[$s] = array();
		$m->mealy[$s] = array();
		for($j = 0; $j < pow($k, $m->w) ; $j++) {
			$b = num_split($j, $k, $m->w);

			// mask the input
			$sum = $i;
			foreach($recipe as $idx=> $mult) {
				$sum += $mult * $b[$idx];
			} 
			$rem = $sum % $k;
			$c= ($sum - $rem)/$k;
			$in = input_implode($b);
			array_add($m->delta[$s], $in, $c);	
			$m->mealy[$s][$in] = $rem;
		}
	}
	return $m;
}

function ensure_length( $recipe, $k, $constant = 0 ) {
	$m = new FSA();
	assert( '$k != 0' );
	$m->k = $k;
	$m->w = count($recipe);

	// add the start and final state
	$m->start_add( 0 );
	$m->states = floor(log(max(array_sum($recipe) , $constant +1), $k)) +1 ;
	$m->end_add( $m->states -1 );

	//echo "\tm has " . $m->states . " states\n";
	//print_r($recipe);

	for($s = 0; $s < $m->states; $s++) {
		$next = min($s +1, $m->states-1);

		$m->delta[$s] = array();

//		for($j = 0; $j < pow($k, $m->w) ; $j++) {
//			$b = num_split($j, $k, $m->w);
		foreach( $m->generate_sigma() as $a ) {
			$in = input_explode($a);

			$skip = false;
			foreach( $recipe as $idx => $mult ) {
				if( $mult != 0 && $in[$idx] != 0 ) {
					$skip  = true;
				} 
			}
			// add the transition
			array_add($m->delta[$s], $a, $skip ? 0 : $next);	
		}

		/*foreach($recipe as $idx=> $mult) {
			// update the recipe for next iteration
			$recipe[$idx] = floor($recipe[$idx] / $k);
		} */
	}

	// start state
	foreach( $m->generate_sigma() as $a ) {
		if( !isset( $m->delta[0][$a]) ) {
			array_add($m->delta[0], $a, 0);	
		}
	}

	//fsa_visualize($m);
	return $m;

}



function remap_vars($m, $original, $new, $k) {
	$introduced = array_values(array_diff($new, $original));
	$i_flip = array_flip(array_values($introduced));
	$o_flip = array_flip(array_values($original));
	//print_r($i_flip);
	$n = map_input( $m, 
		function($in) use($o_flip, $new, $i_flip, $k) {
			$w = count($i_flip);
			$input = input_explode($in);
			$arr = array();
			for( $j = 0; $j < pow($k, $w); $j ++ ) {
				$output = array();
				$generated = num_split($j, $k, $w);
				foreach($new as $l => $var) {
					if( isset($o_flip[$var]) ) {
						assert( 'isset($input[$o_flip[$var]])');
						$output[$l] = $input[$o_flip[$var]];
					} else {	
						$output[$l] = $generated[$i_flip[$var]];
					}
				}
				//print_r($input); print_r($output); exit;
				array_push( $arr, input_implode( $output)); 
			}
			return $arr;
		},
		count($new)
	);
	$n->vars = $new;
	return($n);	
}


function remove_digit($m, $i) {
	$vars = $m->vars;
	unset($vars[$i]);
	
	return map_input( $m, function($in) use ($i) {  
		$ns = input_explode($in);
		unset($ns[$i]);
		return array(input_implode($ns));
		},
		$m->w-1
	);
}


function add_digit($m, $i, $k) {
	$n= map_input( $m, 
		function($in) use ($i, $k) { 
			$arr = array();
			for( $j = 0; $j < $k; $j ++ ) {
				$ns = input_explode($in);
				array_splice($ns, $i, 0, array($j));
				$arr[] = input_implode($ns);
			}
			return $arr;
		},
		$m->w+1
	);
	return( $n);
}

function map_input( $m, $map, $w ) {
	$n = new FSA($m);
	foreach($m->start() as $s) { $n->start_add($s); }
	foreach($m->end() as $s) { $n->end_add($s); }
	if(isset($m->output)) { $n->output = $m->output; }
	$n->w = $w;
	$n->states = $m->states;
	
	foreach( $m->delta as $s => $d) {
		$n->delta[$s] = array();
		foreach($d as $in => $ends) {
			foreach($ends as $end) {
				$new_in = $map($in);
				foreach($new_in as $new) {
					array_add($n->delta[$s], $new, $end);
				}
			}
		}
	}
	return($n);
}

function mealy_composition($m1, $m2){
/*	$ret =  fsa_product($m1, $m2, 'compose');
	fsa_write($m1);	fsa_write($m2);	fsa_visualize($ret);
	return $ret;*/

	$ret = new FSA($m1);
	assert('$m1->states != 0');
	assert('$m2->states != 0');
	foreach( $m1->delta as $s1 => $d1) {
		foreach( $m2->delta as $s2 => $d2 ) {
			$s3 = state_mult($s1, $s2, $m1->states);
			$ret->delta[$s3] = array();
			if( count($m2->mealy) > 0 ) { $ret->mealy[$s3] = array(); }
			// special case for output machines			
			if( isset($m2->output[$s2]) ) {
				$ret->output[$s3] = $m2->output[$s2];
			}
			// start state
			if( $m1->start_contains( $s1 ) && $m2->start_contains( $s2) ){
				$ret->start_add( $s3 );
			}
			if( $m1->end_contains( $s1 ) && $m2->end_contains( $s2 ) ){
				$ret->end_add( $s3 );
			}
			foreach($d1 as $in => $ends) {
				assert( 'count($ends) == 1;');
				$output =$m1->mealy[$s1][$in];
				if( isset($d2[$output])) {
					foreach($d2[$output] as $o) {
						array_add($ret->delta[$s3], $in, state_mult($ends[0] , $o, $m1->states));
					}
				}
				if( isset($m2->mealy[$s2][$output]) ) {
					$ret->mealy[$s3][$in] = $m2->mealy[$s2][$output];
				}
			}
		}
	}
	$ret->states = $m1->states * $m2->states;
	assert( '$ret->states == count($ret->delta)' );
	return($ret);
}



function fsa_cross($m1, $m2, $accept, $output_match = NULL) {
	$ret = new FSA($m1);
	if($m1->w != $m2->w) {
		echo $m1->w . ", ". $m2->w . "\n";
	}
	if( $m1->states == 0 ) { fsa_write($m1);}
	assert( '$m1->states != 0' );
	assert( '$m1->states == count($m1->delta)' );
	assert( '$m2->states != 0' );
	assert( '$m2->states == count($m2->delta)' );
	assert( '$m1->w == $m2->w' );
	assert( '$m1->vars == $m2->vars' );

	//delta
	foreach( $m1->delta as $s1 => $d1) {
		foreach( $m2->delta as $s2 => $d2 ) {
			$s3 = state_mult($s1, $s2, $m1->states);
			$ret->delta[$s3] = array();

			foreach($d1 as $in => $ends1) {
			  if(! isset( $d2[$in]) ) continue;		
			 // if(! isset( $d2[$in]) ) { print_r($d2); print_r($in); }
				assert('isset($d2[$in])');
				$ends2 = $d2[$in];
				foreach($ends1 as $end1) {
					foreach($ends2 as $end2) {
						array_add($ret->delta[$s3],$in, state_mult($end1, $end2, $m1->states));
					}
				}
			}
		}
	}
	// start
	foreach($m1->start() as $s1) {
		foreach($m2->start() as $s2) {
			$ret->start_add( state_mult($s1, $s2, $m1->states) );
		}
	}
	//final
	if(! is_null($output_match)) {
		// This is the output case
		foreach( $m1->output as $s1 => $o1) {
			foreach( $m2->output as $s2 => $o2 ) {
				if($output_match($o1, $o2) &&  $m1->end_contains( $s1 ) && $m2->end_contains( $s2 ) ){
					$ret->end_add( state_mult($s1, $s2, $m1->states) );
				}
			}
		}
	} else {
		// this is the normal cross product case
		foreach( $m1->delta as $s1 => $d1) {
			foreach( $m2->delta as $s2 => $d2 ) {
				if( $accept( $m1->end_contains( $s1 ), $m2->end_contains( $s2 ) ) ) {
					$ret->end_add( state_mult($s1, $s2, $m1->states) );
				}
			}
		}
	}

	// output - not sure if this is useful
	if( isset($m1->output) && isset($m2->output) ) {
		foreach( $m1->output as $s1 => $o1 ) {
			foreach( $m2->output as $s2 => $o2 ) {
				$ret->output[state_mult($s1, $s2, $m1->states)] = $o1 . CR . $o2;
			}
		}
	}


	// mealy - similar to above
	if( count($m1->mealy) >0  && count($m2->mealy) > 0 ) {
		foreach( $m1->mealy as $s1 => $o1 ) {
			foreach( $m2->mealy as $s2 => $o2 ) {
				$s3 = state_mult($s1, $s2, $m1->states);
				$ret->mealy[$s3] = array();
				foreach( $ret->generate_sigma() as $in ) {

					if(isset($o1[$in]) && isset( $o2[$in] ) ) {
						$ret->mealy[$s3][$in] = input_implode(array_merge( input_explode($o1[$in]), input_explode( $o2[$in])));
					} else {
						//echo $in . " is not set.\n";
						//print_r($m1->mealy);
						//	print_r($m2->mealy);
						///assert( 'false;');
					}
				}
			}
		}
	}

	$ret->states = $m1->states * $m2->states;
	assert( '$ret->states == count($ret->delta)' );
	return $ret;
}


// WARNING: this procedure assumes the input machine is zero-input stable
function fsa_accept_trailing_zeros_old($m) {
	echo "\taccept_trailing_zeroes\n";
	$newfinal = array();
	$zeroes = $m->zero_label();

	foreach( $m->delta as $s => $d) {
		$todo = array($s);
		$marked = array();
		while(count($todo) > 0) {
			$curr = array_pop($todo);

			$ma = isset($marked[$curr]);
			$nf = isset($newfinal[$curr]);
			$ze = isset($m->delta[$curr][$zeroes]);

			// see if we need to take the transition
			if ( !$ma && !$nf && $ze ) {
				$marked[$curr] = 1;
				$todo = array_merge ($todo,  $m->delta[$curr][$zeroes] );
			// see if a final state is reachable
			} else if( ( $nf || ( $ma && $m->end_contains( $curr ) ) ) && !isset($newfinal[$s]) ) {
				$newfinal[$s] = 1;
			}
		}
	}

	$m->end_copy($newfinal);
	return $m;
}

function dfa_next_state( $m, $s, $inputs ) {
	
	if( count($inputs) == 0 || !isset($m->delta[$s][$inputs[0]] ) ) { 
		return null; 
	}

	$next = $m->delta[$s][$inputs[0]]; 
	for( $i = 1; $i < count($inputs); $i++ ) {
		if( !isset($m->delta[$s][$inputs[$i]]) || 
		  $m->delta[$s][$inputs[$i]] != $next ) {
			return null;
		}
	}
	return $next;
}
// WARNING: this procedure assumes the input machine is zero-input stable
function fsa_accept_trailing_zeros($m, $mask=null) {
//	echo "\taccept_trailing_zeroes\n";
	$newfinal = array_flip($m->end());
	$zeroes = array($m->zero_label());
	if(isset($mask)) $zeroes = $mask;

	foreach( $m->delta as $s => $d) {
		$todo = array($s);
		$marked = array();
		while(count($todo) > 0) {
			$curr = array_pop($todo);
			$next = dfa_next_state( $m, $curr, $zeroes );
			//echo "\t the next of " . $curr . " state is :\n"; print_r($next);
			
			$ma = isset($marked[$curr]);
			$nf = isset($newfinal[$curr]);
			$ze = isset($next);
			
			// see if we need to take the transition
			if ( !$ma && !$nf && $ze ) {
				$marked[$curr] = 1;
				$todo = array_merge ($todo,  $next );
			// see if a final state is reachable
			} else if( ( $nf || ( $ma && $m->end_contains( $curr ) ) ) && !isset($newfinal[$s]) ) {
				$newfinal[$s] = 1;
			}
		}
	}
	//echo "\tnewfinal=\n"; print_r($newfinal);
	$m->end_copy($newfinal);
	return $m;
}


function fsa_load( $filename ) {
	$fp = fopen($filename, 'r');
	$s = fread($fp, filesize($filename));
	fclose($fp);
	return fsa_read(new FSA(), $s);
}


?>
