<?php


function fsa_write($m, $return = false) {
	$ans = array();
	$ans[] = "fa(\n";
	$ans[] = "r(fsa_frozen),";
	$ans[] = " %sigma\n";
	// states
	$ans[] = $m->states . ", %number of states\n";
	// initial	
	$ans[] = "[";
	$ans[] = implode(",", $m->start());
	$ans[] = "], %initial states\n";
	// final
	$ans[] = "[";
	$ans[] = implode(",", $m->end());
	$ans[] = "], %final states\n";
	// transitions
	$ans[] = "[ % begin transitions\n";
	$trans = array();	
	foreach( $m->delta as $s => $d) {
		foreach($d as $in => $ends) {
			$label = $in;
			if( count(input_explode($in)) > 1 ) {
				$label = '['.implode(',', input_explode($in)). ']';
			}
			if( isset($m->mealy[$s][$in]) ) {//count($m->mealy) > 0 ) {
				$label .= ",'|',[". $m->mealy[$s][$in] .']';
				//echo $label ."\n";
			}
			if( count($m->mealy) > 0  && ! isset($m->mealy[$s][$in]) ) { print_r($m->mealy); }
			foreach($ends as $end) {
				array_push($trans, 'trans('.$s.','.$label.','.$end.')');
			}
		}
	}
	$ans[] = implode(",\n", $trans);
	$ans[] = "\n], % end transitions\n";
	$ans[] = "[]). %jumps\n";

	$answer = implode($ans);
	if( $return ) { return $answer; }
	echo $answer;
}

function read_array( $s, $sep = ',' ) {
	$time_start = microtime(true);
	$start =strpos($s, '(') +1;
	$end =0;
	$ret = read_array_helper($s, $start, $end);
//echo "\tread array in ". round(microtime(true) - $time_start, 3) ."s\n";
	return $ret;
}

function read_array_helper( $s , $start, &$end, $sep = ',' ) {
	$left= '[(';
	$right='])';
	$ret = array();
	$last = $start;
	$rec = null;

	for($i = $start; $i<strlen($s); $i++){
		$c= $s[$i];
		if(strpos($right, $c) !== false) { // right bracket
			$end = $i;
			if( $rec !== null ) 	{ 	$ret[] = $rec;	} 
			else 		{	$ret[] = substr($s, $last, $i - $last);	}
			return $ret;
		} 
		if(strpos($sep, $c) !== false ) {
			if( $rec !== null ) 	{ 	$ret[] = $rec;	} 
			else 		{	$ret[] = substr($s, $last, $i - $last);	}
			$last = $i + 1;
			$rec = null;
		} 
		if(strpos($left, $c) !== false) { 
			//$name = substr($s, $last, $i - $last);
			$rec =  read_array_helper($s, $i+1, $i);
			$last = $i+1;
		}
	}
	echo "Unmatching brackets in read array :\n";  echo $s ; /*print_r(debug_backtrace());*/ exit;
}

function remove_blanks($a) {
	$non_empty = function($s) { return $s !== ''; };
	return array_filter( $a, $non_empty);
}


function array_to_obj($array, &$obj) {
	foreach ($array as $key => $value) {
		/*if (is_array($value)) {
			$obj->$key = new stdClass();
			array_to_obj($value, $obj->$key);
		} else {
			$obj->$key = $value;
		}*/
		$obj->$key = $value;
	}
	return $obj;
}

function fsa_read2($m, $in) {
	$time_start = microtime(true);
	$code = 0; #dummy variable
	$out = run_external('java -Xmx2000m -jar FSA.jar', $in, $code);
	$n = json_decode($out[0], true);
	//echo $out[1];
	if(!is_array($n)) {
		echo "this was read back!\n"; 
		$fp = fopen( "test.json", 'w');
		fwrite($fp, $in);
		fclose($fp);
		print_r($out); 
		print_r($n); 
		exit;
	}
	$m = array_to_obj($n, $m);

	//$m->_start = array_flip($m->_start);
	//$m->_final = array_flip($m->_final);
//print_r($m);
	//echo "\tread2 in ". round(microtime(true) - $time_start, 3) ."s the number of states is ". $m->states ."\n";
	return $m;

}

function fsa_read1($m, $s) {

$time_start = microtime(true);
	// remove the comments, newlines, and whitespace
	$s = preg_replace(array('/%.+/', '/\n/', '/ /'), array('','',''), $s);

	// split into sections 
	$ss = read_array($s); 
	assert('count($ss) == 6');

	// start rebuilding the fsa
	$m->states = $ss[1];
	foreach( remove_blanks($ss[2]) as $s ) { $m->start_add( $s ); }
	foreach( remove_blanks($ss[3]) as $s ) { $m->end_add( $s ); }
	
	// add the transitions
	foreach( remove_blanks($ss[4]) as $ts) {
     // if( !$ts ) continue;
		//assert('count($ts) == 3;');
		$s = $ts[0];
		if( is_array($ts[1]) ) {
			$in = input_implode($ts[1]);
			//assert('($m->w == 1) || ($m->w == count($ts[1]));');
			$m->w = count($ts[1]);
		} else {
			$in = $ts[1];
		}
		//echo $ts[2];
		if( !is_numeric($ts[2]) ) { print_r($ts); exit; }
		$e = $ts[2];
		array_add($m->delta[$s], $in, $e);
	}

	//echo "\twhole read in ". round(microtime(true) - $time_start, 3) ."s the number of states is ". $m->states ."\n";

	return $m;
}

function fsa_read($m, $s) {

	//return fsa_read1($m, $s);
	$s2 = preg_replace(array('/%.+/', '/\n/', '/ /'), array('','',''), $s);

	/*$fp = fopen( "test.json", 'w');
	fwrite($fp, $s2);
	fclose($fp);*/

	return fsa_read2($m, $s2);

	$m2 = clone $m;

	$m1 = fsa_read1($m, $s);
	$m2 = fsa_read2($m2, $s2);

	if( $m1 != $m2 ) {
		print_r($m1);
		print_r($m2);
		exit;
	}
	return $m1;
}



function run_external($cmd, $input, &$code) {
    $descriptorspec = array(
        0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
        1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
        2 => array("pipe", "w") // stderr is a file to write to
    );
   
    $pipes= array();
    $process = proc_open($cmd, $descriptorspec, $pipes);
   
    $output= array("","");
   
    if (!is_resource($process)) return false;
   
    #write input and close child's input imidiately
	fwrite($pipes[0], $input);
    fclose($pipes[0]);
   
    stream_set_blocking($pipes[1],false);
    stream_set_blocking($pipes[2],false);
   
    $todo= array($pipes[1],$pipes[2]);
   
    while( true ) {
        $read= array();
        if( !feof($pipes[1]) ) $read[0]= $pipes[1];
        if( !feof($pipes[2]) ) $read[1]= $pipes[2];

        if ( !$read ) break;
       
       /* $ready= stream_select($read, $write=NULL, $ex= NULL, 2);
       
        if ($ready === false) {
            break; #should never happen - something died
        }*/
       
		foreach ($read as $i => $r) {
           	$s= fread($r,1024);
			//if($i ==1) echo $s;
            $output[$i] .= $s;
        }
    }
   
    fclose($pipes[1]);
    fclose($pipes[2]);
   
    $code= proc_close($process);
   
    return $output;
}




function fsa_execute($m, $option, $raw = false) {
//echo "\twriting\n";
	$in = fsa_write($m, true);
//echo "\texecuting\n";
	$code = 0; #dummy variable
	$out = run_external('cd fsa6/yap/ && ./fsa '.$option, $in, $code);
	if($raw) return $out[0];
//echo "\treading\n";
	// else input the machine that was output
	$n = fsa_read(new FSA($m), $out[0]);
	return $n;
}

function fsa_execute2($m1, $m2, $option, $raw = false) {
//echo "\twriting\n";
//	unlink('fsa6/yap/a.fsa');
//	unlink('fsa6/yap/b.fsa');
	fsa_save( $m1, 'fsa6/yap/a.fsa');
	fsa_save( $m2, 'fsa6/yap/b.fsa');
//echo "\texecuting\n";
	$code = 0; #dummy variable
	$out = run_external('cd fsa6/yap/ && ./fsa -'.$option.' a.fsa b.fsa ' ,"", $code);
	if($raw) return $out[0];
//echo "\treading\n";
	if( $option == 'compose' ) { print_r( $out); echo "composition done\n"; } 
	// else input the machine that was output
	$n = fsa_read(new FSA($m1), $out[0]);
	return $n;
}


function fsa_minimize($m, $deterministic = true) {
	$option = $deterministic ? '-m' :'-m2';
	return fsa_execute($m,$option);
}

function fsa_determinimize($m) {
	return fsa_execute( $m, '-d');
}

function brzozowski($m) {
	return fsa_minimize($m, false);
}

function fsa_reverse($m) {
	return fsa_execute( $m, '-reverse');
}

function fsa_complement($m) {
	//first we have to have a valid alphabet, we do this by adding a dead state.
	$n = $m->states;

//echo "\tadding dead state\n";

	assert( '!isset($m->delta[$n])' );
	assert( '$m->start_contains( 0 )' );

	$m->delta[$n] = array();
	foreach($m->generate_sigma() as $in) {
		if( !isset($m->delta[0][$in]) || count($m->delta[0][$in]) == 0 ) {
			$m->delta[0][$in] = array($n);
		}
		$m->delta[$n][$in] = array($n);
	}
	$m->states++;
	//print_r($m);

	//fsa_visualize($m);

 	return fsa_execute( $m, '-complement');

}


function fsa_visualize($m, $filename = 'temp.dot') {
//fsa_write($m);
	$s = fsa_execute($m, '-dot', true);
	$fp = fopen($filename, 'w');
	fwrite($fp, $s);
	fclose($fp);
	shell_exec('xdot "'.$filename.'"');
}


function fsa_equal( $m1, $m2 ) {

	//echo	fsa_execute2($m1, $m2, 'identical', true); exit;
	//$m1 = fsa_minimize($m1, false);
	//$m2 = fsa_minimize($m2, false);
	$m = fsa_product($m1, $m2, 'difference');
	//fsa_visualize($m);
	$m = fsa_minimize($m);
	return (count($m->states) == 1 and count($m->end()) == 0) ;
}


function fsa_product($m1, $m2, $type) {

	$types = array('intersect','union','difference','compose');
	assert('in_array($type, $types)');

	return fsa_execute2($m1, $m2, $type);


	/* //$in = fsa_write($n1, true) . ' , ' .fsa_write($n2, true)  ;



	unlink('fsa6/yap/a.fsa');
	unlink('fsa6/yap/b.fsa');
	fsa_save( $n1, 'fsa6/yap/a.fsa');
	fsa_save( $n2, 'fsa6/yap/b.fsa');

//	posix_mkfifo('fsa6/yap/a.fsa',0777);
//	posix_mkfifo('fsa6/yap/b.fsa',0777);

	//write_async( $n1, 'fsa6/yap/a.fsa');
//	write_async( $n2, 'fsa6/yap/b.fsa');

//pcntl_wait($status);
//pcntl_wait($status);


	//$m= fsa_load('fsa6/yap/a.fsa');
	//fsa_visualize($n1);
	//fsa_visualize($m);
	$code = 0; #dummy variable
//	$out = run_external('cat fsa6/yap/a.fsa  fsa6/yap/b.fsa', "", $code);
	$out = run_external('cd fsa6/yap/ && ./fsa -'.$type.' a.fsa b.fsa ' ,"", $code);
//	$out = run_external('cd fsa6/yap/ && ./fsa' ,"", $code);
	//echo "got here too\n";


/*
//'cd fsa6/yap/ && ./fsa -r "'.$type.'(' .$in .')"'
//'cd fsa6/yap/ && ./fsa -'.$type.' a.fsa b.fsa'

	
print_r($out);



	// else input the machine that was output
	$n = fsa_read(new FSA($m1), $out[0]);
	return $n;*/
}

function fsa_save( $m, $file ) {

	$fp = fopen( $file, 'w');
	//stream_set_blocking ( $fp , false );
	$s = fsa_write($m, true);
	fwrite($fp, $s);
	fclose($fp);
}

function write_async( $m, $file) {

	$pid = pcntl_fork();
	if ($pid == -1) {
		  die('could not fork');
	} else if ($pid) {
		// we are the parent
		// pcntl_wait($status); //Protect against Zombie children
	} else {
		  // we are the child
		echo "child running " . $file."\n";
		fsa_save( $m, $file);
		echo "child finished " . $file."\n";
		exit;
	}
}



function fsa_matrix($m, $idx = 0, $k) {
	$ret = array();
	$n = $m->states;

	for($a=0; $a<$k; $a++) {
		$ret[$a] = array();
		for( $i = 0; $i < $n; $i++ ) {
			// zero-fill
			$ret[$a][$i] = array();
			for( $j = 0; $j < $n; $j++ ) {
				$ret[$a][$i][$j] = 0;
			}
		}
	}
	foreach($m->delta as $s => $d) {
		foreach($d as $in => $ends) {
			$ns = input_explode($in);
			$a = $ns[$idx];
			foreach($ends as $e) {
				$ret[$a][$s][$e] +=1;
			}
		}
	}
	$ret['v'] = array_fill(0, $n, 0);
	foreach($m->start() as $s) {
		$ret['v'][$s] = 1;
	}
	$ret['w'] = array_fill(0, $n, 0);
	foreach($m->end() as $s) {
		$ret['w'][$s] = 1;
	}
	return $ret;
}

function vector_write ($vector,  $return = false) {
	$ans = '[' . implode(',', array_map('strval', $vector)) . "]\n";
	if( $return ) { return $ans; }
	echo $ans;
}
function matrix_write ($A, $return = false) {
	$row_map = function($vector) {return implode(',', array_map('strval', $vector)); };
 	$column_map = function($vector) { return implode("],\n[",  array_map('strval', $vector)); };
	$ans = '[[' .$column_map(array_map($row_map, $A)). "]]\n";

	if( $return ) { return $ans; }
	echo $ans;
}


?>
