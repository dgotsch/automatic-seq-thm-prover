<?php

include_once('src/fsa_parse.php');

$powerOfTwo = FSA::from_array(
 	array(
		'start' => array('0'),
		'final' => array('1'),
		'delta' => array(
			'0' => array('0' => array('0'), '1' => array('1')),
			'1' => array('0' => array('1'), '1' => array('2')),
			'2' => array('0' => array('2'), '1' => array('2'))
		),
		'k' => 2	
	)
);

$powerOfThree = FSA::from_array(
	array(
		'start' => array('0'),
		'final' => array('1'),
		'delta' => array(
			'0' => array('0' => array('0'), '1' => array('1'), '2' => array('2')),
			'1' => array('0' => array('1'), '1' => array('2'), '2' => array('2')),
			'2' => array('0' => array('2'), '1' => array('2'), '2' => array('2'))
		),
		'k' => 3	
	)
);


function transpose($arr) {
   $out = array();
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => $subvalue) {
                $out[$subkey][$key] = $subvalue;
        }
    }
    return $out;
}

function display_ratio($name, $d) {
	$m = fsa_reverse($d);
	$m = brzozowski($m);


	fsa_visualize($m, 'results/'.$name.'.dot');
	echo "*** " . $name ." ratios are =" . MDFA_get_highest_ratio($m,2) . "\n";
	//print_r(MDFA_get_highest_ratio($m,2));
	

	return $m;
}



function make_tape( $input, $k ) {
	$max = max($input);
	//echo ceil(log($max+1, $k)) ."\n";
	$len = ceil(log($max+1, $k)); 
	for( $i = 0; $i < count( $input ); $i++ ) {
		$tape[$i] = num_split($input[$i], $k, $len);
		assert( count($tape[$i]) == $len );
	}
	$tape = transpose($tape);
	$tape = array_map( 'input_implode', $tape);
	return $tape;
}

function dfa_accepts ($m, $input, $k = 2 ) {
	$in = make_tape( $input, $k );
	//print_r($in);
	$curr = $m->start();
	foreach( $in as $i ) {
		$next = array();
		foreach( $curr as $s ) {
			if( isset( $m->delta[$s][$i] ) ) {
				$out = $m->delta[$s][$i];
				foreach( $out as $new ) {
			 		if( ! in_array($new, $next) ) {
						array_push($next, $new);
					}
				}
			}
		}
		$curr = $next;	
	}
//	assert('count($curr) == 1');
	//echo "final states:";
	//print_r($curr);
	
	//print_r($m->end());
	$ans = array(
		'accept' => (count(array_intersect($curr, $m->end())) > 0),
		'states' => $curr);	
	if( isset($m->output) ) {
		$ans['output'] = $m->output[$curr[0]];		//array_map(function($s) { return 	
	}
	return $ans;
}
function MDFA_remove_acyclic_states($m) {

	$ter = DFA_find_terminal_states($m);
		print_r($ter);
		fsa_visualize($m);
	while( count($ter) > 0 ){
		foreach( $ter as $s) {
			$m = DFA_remove_state($m, $s);
		}

		$ter = DFA_find_terminal_states($m);
		print_r($ter);
		
		fsa_visualize($m);
	}

	return $m;

}


function DFA_find_terminal_states($m) {
	$ter = array();
	
	$states = array_flip(array_keys($m->delta));
	// find the terminal states
	foreach($m->delta as $s => $d) {
		foreach($d as $in => $end) {
			assert('count($end) == 1');
			if( !isset( $states[$end[0]] ) ) {
				$ter[] = $end[0];
			}
		}
	}
	return array_unique($ter);
}


function DFA_remove_state($m, $target) {
	// remove all transitions to it
	foreach($m->delta as $s => $d) {
		foreach($d as $in => $end) {
			assert('count($end) == 1');
			if( $end[0] == $target ) {
			//	echo "deleting ".$s . ' ---' . $in . '---> ' . $end[0] . "\n";
				unset($m->delta[$s][$in]);
			}
		}
	}
	// remove it
	unset($m->delta[$target]);
	// remove it from start and final states
	$m->start_remove( $target  );
	$m->end_remove( $target );

	return $m;
}


function DFA_find_dead_states($m) {
	$ter = array();

	// find the terminal states
	foreach($m->delta as $s => $d) {
		$is_terminal = true;
		foreach($d as $in => $ends) {
			//assert('count($end) == 1');
			foreach($ends as $end) {
				if( $end[0] != $s ) {
					$is_terminal = false;
					break 2;
				}
			}
		}
		if($is_terminal) {
			$ter[] = $s;
		}
	}
	return $ter;
}



function __get_highest_ratio ($m, $curr, $w0, $w1, $visited, $k) {
// check if we're in the dead state

	if( strlen($w0) > (count($m->delta) +1) ) return array(); // too large to use integers for.
	$a = intval($w0, $k);
	$b = intval($w1, $k);


	if( ($a != 0) && $m->end_contains( $curr ) ) {
		$r = (float)$b/ $a;
		$ans = array(array('a' => $a, 'b' => $b , 'b/a' => $r));
	} else {
		$ans = array();
	}

	// already visited this node
	if( in_array($curr, $visited) ) { 
		if($a == 0 && $b != 0) return array('a'=>$a, 'b'=>$b,'b/a'=>'INF');
		if($a == 0) return array();
		$trans = array_flip($visited);
		$p = $trans[$curr];
		//if( (intval($w0, $k) - intval(substr($w0,0,$p), $k)) == 0 ) {echo 'uv = ' .intval($w0, $k). ' u= ' . substr($w0,0,$p). ' pos= ' . $p . "\n";}
		$r = (float) (intval($w1, $k) - intval(substr($w1,0,$p), $k)) / (intval($w0, $k) - intval(substr($w0,0,$p), $k));
		array_push( $ans, array('a' => substr($w0,0,$p) . '(' . substr($w0,$p) .')*',
										'b' => substr($w1,0,$p) . '(' . substr($w1,$p) .')*',
										'b/a' => $r ));
		return $ans;
	}
	$visited[] = $curr;

	if( !isset($m->delta[$curr]) ) { return array(); }
	$d = $m->delta[$curr];

	foreach($d as $in => $end) {
		$x = input_explode($in);
		assert('count($end) == 1');
		
		$ans = array_merge( $ans, __get_highest_ratio($m, $end[0], $w0 . $x[0], $w1. $x[1], $visited, $k)); 
	}
	
	return $ans;
}

function MDFA_get_highest_ratio($m, $k) {


	$visited = array();
	$start = $m->start();
	assert('count($start) == 1;');
	$ratios = __get_highest_ratio($m, $start[0], '', '', $visited, $k);
//	print_r($ratios);
	return max( array_map( function($rec) { return $rec['b/a']; }, $ratios));
}



function shortest_distance($m, $start, $end) {

	$distance = array();
	$previous = array();
	$todo = new SplQueue();
	$distance[$start] = 0;
	$todo->enqueue($start);
	
	// calculate the distances from the accepting states
	while( !$todo->isEmpty() ) {
		$s = $todo->dequeue();
			
		if( !isset( $m->delta[$s] ) ) continue;
		foreach($m->delta[$s] as $in => $ends) {
			foreach( $ends as $e) {
				if( !isset( $distance[$e] ) ) {
					$distance[$e] = $distance[$s] + 1;
					$previous[$e] = $s;
					$todo->enqueue($e);
				}
			}

		}
	}

//	$depth = max($distance);
	if( !isset( $distance[$end] ) ) return -INF;
	return $distance[$end];

}
$dynamic=array();

function longest_distance($m, $s, $end, $visited = array())  {
global $dynamic;
			
	if( !isset($m->delta[$s]) ) return -INF;
	if( isset($visited[$s]) )  return -INF; 
	if( isset($dynamic[$s] ) ) return $dynamic[$s];
	if( $s == $end ) {
//echo "s=" . $s . ", e=". $end . ", d=". max($distance) . " visited so far ". print_r($visited,true) ."\n";
return 0; 
exit;
}
	

	$distance = array();
	$visited[$s] = true;

	foreach($m->delta[$s] as $in => $ends) {
		foreach( $ends as $e) {
			if( !isset( $distance[$e] ) ) {
				$distance[$e] = longest_distance($m, $e, $end, $visited);
			}
		}
	}
	$dynamic[$s] = max($distance) +1;
//echo "s=" . $s . ", d=". $dynamic[$s] . " visited so far ". print_r($visited,true) ."\n";
	return $dynamic[$s];

}

function measure_guess_depth($m) {

	global $dynamic;
	$r = fsa_reverse($m);
	$distance = array();
	$previous = array();
	$todo = new SplQueue();
	foreach($m->end() as $s) {
		$distance[$s] = 0;
		$todo->enqueue($s);
	}
	
	// calculate the distances from the accepting states
	while( !$todo->isEmpty() ) {
		$s = $todo->dequeue();
			
		if( !isset( $r->delta[$s] ) ) continue;
		foreach($r->delta[$s] as $in => $ends) {
			foreach( $ends as $end) {
				if( !isset( $distance[$end] ) ) {
					$distance[$end] = $distance[$s] + 1;
					$previous[$end] = $s;
					$todo->enqueue($end);
				}
			}

		}
	}

	$depth = max($distance);
	$cyclic = array();
	// we don't count the non-ambiguous states
	foreach($m->delta as $s => $d) {
		$cyc = false;
		foreach($d as $in => $ends) {
			if( in_array( $s, $ends) && count($ends) > 1 ) {
				$cyc = true;
				break;
			}
		}
		if( !$cyc ) {
			unset($distance[$s]);
			//echo "state " .$s. " is not ambiguous!\n";
		} else {
			array_push($cyclic, $s);
		}
	}

	$newdepth = -INF;
	foreach($cyclic as $s) {
		foreach($m->end() as $f) {
			$dynamic = array();
			$newdepth = max( $newdepth, longest_distance($m, $s, $f) );
		}
	}
	if ($depth != $newdepth)  echo "old depth is " . $depth. " new depth is " . $newdepth . "\n";	

	return $newdepth;
}


function factor_count($seq , $name, $bypass =false) {

	if( $bypass ) {
		$d = $bypass;
		$filename = $name;
	} else {
		$filename = 'FactorCount' . '_' . $name;
		$d = process_request( 
			'UniqueFactors' . '_' . $name,
			$seq, 
			array('n', 'j'),
			array('\and',
				array('\not',
					array('\exists', 'l',
						array('\and',
							array('<', 'l', 'j'),
							array('\factor', 'n', 'l', 'j')
						)
					)
				),
			//	array('=', 'n', '2')
			)
		);
	}

	//fsa_visualize($d); exit;
	$r = process_request( 
		"_temp_R".$filename,
		$seq, 
		array('n', 's', 'e', 'l'), 
		array('\or', 
			array('\and',
				array('>', 'l', '0'),
				array('<=', 's', 'e'),
				array('\not', 
					array('\machine', array('n','e'), $d) 
				),
				array('\exists', 'b',
					array('\and',
						array('\or', 
							array('=', 'b', '0'),
							array('\exists', 'a',
								array('\and',
									array('=', 'a+1', 'b'),
									array('\not', 
										array('\machine', array('n','a'), $d) 
									)
								)
							)
						),
						array('<=', 's', 'b'),
						array('=', 'b+l', 'e'),
						array('\forall', 'i',
							array('\or',
								array('<', 'i','s'),
								array('\and',
									array('>=', 'i','s'),
									array('<', 'i','b'),
									array('\not', 
										array('\machine', array('n','i'), $d) 
									)
								),
								array('\and',
									array('>=', 'i','b'),
									array('<', 'i','e'),
									array('\machine', array('n','i'), $d) 
								),
								array('>=', 'i', 'e')
							)
						)
					)
				)
			),
			array('\and',
				array('=', 's', 'e'),
				array('=', 'l', '0'),
				array('\forall', 'i',
					array('\or',
						array('<', 'i', 's'),
						array('\not', 
							array('\machine', array('n','i'), $d) 
						)
					)
				)
			)
		)
	);
	//fsa_visualize($r);

	$m = process_request( 
		"_temp_M_0".$filename,
		$seq, 
		array('n', 'l', 'e'), 
		array('\and',
			array('=', 'l', '0'),
			array('=', 'e', '0')
		)
	);
	//fsa_visualize($m);
	$n = $m;

	for($i = 1; 1; $i++ ){
		$c = process_request( 
			"_temp_M_".$i.$filename,
			$seq, 
			array('n', 'l', 'e'), 
			array('\exists', 's',
				array('\exists', 'k',
					array('\and',
						array('\machine',  array('n','s','e','k'), $r),
						array('\exists', 'j',
							array('\and',
								array('=', 'k+j', 'l'),
								array('\machine', array('n','j','s'), $m)
							)
						)
					)
				)
			)
		);
	/*	$p = process_request( 
			"_temp_N_".$i.$filename,
			$seq, 
			array('n', 'l', 'e'), 
			array('\exists', 's',
				array('\exists', 'k',
					array('\and',
						array('\machine',  array('n','s','e','k'), $r),
						array('\exists', 'j',
							array('\and',
								array('\or', 
									array('\and',
										array('=', 'k','0'),
										array('=', 'j', 'l')
									),
									array('\and',
										array('>', 'k', '0'),
										array('=', 'j+1', 'l')
									)
								),
								array('\machine', array('n','j','s'), $n)
							)
						)
					)
				)
			)
		);
		$n = $p;*/
		echo "Computed iteration #". $i ."\n";
		if (fsa_equal($c, $m)) break;
		$m = $c;
		$e = process_request( 
			"temp_E".$filename,
			$seq, 
			array('n', 'l'), 
			array('\exists', 'e',
				array('\and',
					//array('=','n','12'),
					array('\machine', array('n','l','e'), $n)
				)
			)
		);

	}
	echo "done.\n";

	$e = process_request( 
		$filename,
		$seq, 
		array('n', 'l'), 
		array('\exists', 'e',
			array('\and',
				//array('=','n','12'),
				array('\machine', array('n','l','e'), $m)
			)
		)
	); 

/*	$f = process_request( 
		$filename,
		$seq, 
		array('n', 'l'), 
		array('\exists', 'e',
			array('\and',
				//array('=','n','12'),
				array('\machine', array('n','l','e'), $n)
			)
		)
	);
fsa_visualize(brzozowski(fsa_reverse($f)));*/

	return $e;
}

function show_and_save($n, $filename) {

	$m =brzozowski(fsa_reverse($n));

	fsa_visualize($m, 'results/'.$filename.'.dot');
	shell_exec('dot -Tpng "results/'.$filename.'.dot" -o'.$filename.'.png');

}


?>
