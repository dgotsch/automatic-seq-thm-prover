<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');



$expr = parse_expr('
(\not,
	(\exists, d,
		(\exists, b,
			(\and,
				(>, d, 0),	
				(<, d, n),
				(=, b+d, n),
				(\factor, d, i, i+b),
				(\factor, b, i, i+d)
			)
		)
	)
)
');

//print_r($expr); exit;

foreach( array(/*'Thue-Morse-neg' => $TM_neg,*/'Thue-Morse' => $TM,   'Period-Doubling' => $PD,  'Paper-folding' => $PF, 'Rudin-Shapiro' => $RS ) as $name => $seq ) {
	echo $name . ":\n";
$REUSE=false;
	$filename = 'Primitive' . '_' . $name;
	$primitive = process_request(
		$filename, 
		$seq, 
		array('n','i'),
		$expr
	);
	$filename = 'PrimitiveUnique' . '_' . $name;	
	$c = process_request(
		$filename,
		$seq,
		array('n', 'i'),
		array('\and',
			array('\machine', array('n','i'), $primitive),
			array('\not',
				array('\exists','j',
					array('\and', 
						array('<', 'j', 'i'),
						array('\factor', 'n', 'j', 'i')
					)
				)
			)
		)
	);
/*
	$filename = '_tempUnique' . '_' . $name;	
	$u = process_request(
		$filename,
		$seq,
		array('n', 'i'),
		array('\and',
			array('\not',
				array('\exists','j',
					array('\and', 
						array('<', 'j', 'i'),
						array('\factor', 'n', 'j', 'i')
					)
				)
			)
		)
	);

	for($n=0; $n<50; $n++) {
		for($i=0; $i<120; $i++ ){
			//$tape = make_tape(array($i,$j),2); 
			//print_r($tape); continue;

		 	$ans = dfa_accepts ($c, array($n,$i),  2 );
			$ans2 = dfa_accepts ($u, array($n,$i), 2);
			echo ($ans['accept']) ? '#' : ($ans2['accept'] ? '.' : ' ');

		}
		echo "\n";
	}
	
exit;
//*/
	$m =brzozowski(fsa_reverse($c));
	fsa_visualize($m, 'results/'.$filename.'.dot');
//continue;
	shell_exec('dot -Tpng "results/'.$filename.'.dot" -o'.$filename.'.png');

	$A = fsa_matrix($m, 0, 2);
	echo "v:\n";
	vector_write($A['v']);
	echo "w:\n";
	vector_write($A['w']);
	echo "M_0:\n";
	matrix_write($A['0']);
	echo "M_1:\n";
	matrix_write($A['1']);

//continue;						
	$filename = 'PrimitiveCount' . '_' . $name;
	$r = factor_count($seq , $filename, $c);
	
	$m = fsa_reverse($r);
	$m = brzozowski($m);


	fsa_visualize($m, 'results/'.$filename.'.dot');
	shell_exec('dot -Tpng "results/'.$filename.'.dot" -o'.$filename.'.png');


//	exit;		

}



?>
