<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');



foreach( array('Stewart-choral'=>$SC, 'Baum-Sweet'=>$BS, 'Mephisto-Waltz'=>$MW,'Period-Doubling' => $PD   ,'Paper-folding' => $PF, 'Thue-Morse' => $TM,'Rudin-Shapiro' => $RS) as $name => $seq ) {
	echo $name . ":\n";
$REUSE=false;

	if( $seq == $BS ) {
		$filename = 'Nonrepeating-factors' . '_' . $name;
		$d = process_request(
			$filename, 
			$seq, 
			array('i','n'),
			array('\min', 'n',
				array('\not', 
					array('\exists', 'l',
						array('\and', 
							array('\factor', 'n', 'l', 'i'),
							array('>', 'l', 'i')
						)
					)
				)
			)
		);

	} else {
		$filename = 'Recurrence' . '_' . $name;
	/*	
		$exp = 
		array('\min', 'm',
			array('\forall', 'k',
				array('\forall', 'j',
					array('\exists', 'l',
						array('\and', 
							array('\factor', 'n', 'l', 'j'),
							array('>=', 'm+k', 'n+l'),
							array('>=', 'l', 'k')
						)
					)
				)
			)
		)
		;
		
		$vars = array('n','m');
	
		$TERNARY_LOGIC = true;
		$d2 = process_request(
			$filename, 
			$seq, 
			$vars,
			$exp
		);
		fsa_visualize($d2);
	
		$TERNARY_LOGIC = false;
		$d1 = process_request(
			$filename, 
			$seq, 
			$vars,
			$exp
		);

		$eq = fsa_equal($d1, $d2);
		echo $eq ? "equal\n" : "not equal \n";
		exit;
	
		*/
	
	
		$d = process_request(
			$filename, 
			$seq, 
			array('n','m'),
			array('\min', 'm',
				array('\forall', 'k',
					array('\forall', 'j',
						array('\exists', 'l',
							array('\and', 
								array('\factor', 'n', 'l', 'j'),
								array('>=', 'm+k', 'n+l'),
								array('>=', 'l', 'k')
							)
						)
					)
				)
			)
		);
	}

	for($i=0; $i<30; $i++) {
		for($j=0; $j<50*$i; $j++ ){
		 	$ans = dfa_accepts ($d, array($i,$j),  $d->k );
			if ($ans['accept']) echo base_convert($i, 10, $d->k). ":". $i.",".$j.";\n";
		}
	}
		
	//$m = display_ratio($filename, $d);
	show_and_save($d, $filename, "eps");


	$log_of = 'n-1';
	$log_is = 't';
	$lim = 0;
	if ($seq == $TM ) {
		$log_is = '2*t';
		$relation =	array('=', '9*t+n', 'm+1');
	} else if($seq == $RS) {
		$relation =	array('=', '20*t+n', 'm+1');
	} else if($seq == $PF) {
		$log_of = 'n';
		$relation =	array('=', '8*t+n', 'm+1');
	} else  if($seq == $PD) {
		$relation =	array('=', '7*t+n', 'm+1');
		$log_is = '2*t';
		$log_of = 'n';
	} else if($seq == $MW ) {
		$relation =	array('=', '11*t+n', 'm+1');
	} else if($seq == $SC ) {
		$log_of = 'n';
		$relation =	array('=', '3*t+n', 'm+1');
	}  else if($seq == $BS ) {
		$log_of = 'n+1';
		$log_is = 't';

		$relation =	array('\or', 
						array('\and', 
							array('\machine', array('t'), $log_even),
							array('=', '2*t-n+2', 'm'),
						),
						array('\and',
							array('\not', array('\machine', array('t'), $log_even)),
							array('=', '2*t-n+1', 'm'),
						)
					);
	}
 
	$filename_confirm =  'Recurrence_confirm_' . $name;
	$r = process_request(
		$filename_confirm,
		$seq,
		array('n'),
		array('\exists', 't',
			array('\exists', 'm',
				array('\and',
					array('\machine', array('n','m'), $d),
					array('>=', 'n', $lim),
					array('\machine', array($log_of, $log_is), ceil_log($seq->k)),
					$relation
				)
			)
		)
	);
		
	show_and_save($r, $filename_confirm);

	//exit;

}



?>
