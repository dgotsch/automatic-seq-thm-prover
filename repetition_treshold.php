<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');

$candidates = array(
//	array('0435','2341','3542','3540','4134','4105'),
	array('134', '054', '042', '354', '234', '041'),
//	array('041', '054', '354', '104', '234', '354'),
	array('041', '354', '054', '104', '234', '354'),
	array('041', '134', '234', '104', '354', '234'),
	array('041', '234', '134', '104', '354', '134'),
	array('041', '234', '104', '134', '354', '042'),
	array('042', '234', '134', '104', '354', '134'),
	array('042', '234', '134', '104', '354', '234'),
	array('042', '134', '234', '104', '354', '234'),
	array('041', '134', '234', '104', '354', '234'),
	array('042', '134', '234', '104', '354', '234'),
	array('041', '134', '234', '104', '354', '234'),
	array('042', '134', '234', '104', '354', '234'),//*/
	array('0434', '1054', '1342', '3404', '2354', '1054'), //0
	array('0434', '1354', '1342', '3404', '2354', '1054'), //1
	array('0410', '1234', '0541', '0423', '4354', '5413'), //2
	array('0413', '2354', '1054', '2340', '5434', '4123'), //3
	array('0423', '1341', '0541', '2340', '4354', '4105'), //4 
	array('0434', '1354', '3412', '3404', '1054', '1234'), //5
	array('0434', '2354', '1342', '3404', '1054', '1234'), //6 
	array('0434', '2354', '3412', '3404', '1054', '1354'), //7
	array('0434', '2354', '1042', '3404', '1354', '1234'), //8
	array('0435', '2341', '0542', '3540', '4134', '4105'),
	array('0435', '2341', '3542', '3540', '4134', '4105'), //10
	array('0435', '1054', '0541', '3540', '4234', '4105'),
	array('0435', '0541', '1042', '3540', '4234', '4105'), //12
	array('0435', '0541', '1054', '3540', '4234', '4105'),
	array('0435', '0541', '1342', '3540', '4234', '4105'),
	array('0435', '1054', '1342', '3540', '4234', '4105'),
	array('0543', '2341', '0542', '3405', '4104', '4135'),
	array('0543', '2341', '1342', '3405', '4104', '4135'),
	array('0543', '2341', '1354', '3405', '4104', '4135'),
	array('0543', '2341', '2354', '3405', '4104', '4135'),
	array('0543', '2341', '3542', '3405', '4104', '4135'),
	array('0543', '2341', '1342', '3405', '4104', '4235')   
);
/*
$REUSE=false;
foreach( array( 'Thue-Morse' => $TM,'Samsonov-Shur'=>$SS_msd,'Period-Doubling'=>$PD_msd ) as $name => $seq ) {
	echo $name . ":\n";



	$cand = NFAO_reverse($seq);

	$filename = 'Repetitivity_'.$name;
	$d = process_request( 
		$filename,
		$cand, 
		array('n','i'),
		array('\out=', 'n','i') 
	);

	//fsa_visualize($d);
	
	$e = process_request( 
		$filename,
		DFAO_reverse($seq), 
		array('n','i'),
		array('\out=', 'n','i') 
	);
	//fsa_visualize($e);
	echo fsa_equal($e,$d) ? "equal\n" : "NOT equal\n";
}
*/

// [     ]     [     ]
// i  n     g     m
$expr = parse_expr('
(\exists, n,
	(\exists, g,
		(\exists, p,
			(\and,
				(>, p, 1),
				(\forall, k,
					(\or,
						(\or,
							(\or,
								(\and, 
									(\out=, k+i, k+i+p),
									(<, k+p, n)
								),
								(\and,
									(\out=, k+i+g, k+i+g+p),
									(>=, k, n),
									(<=, k+p, 3*p)
								)
							),
							(\and,
								(\out=, k+i, k+i+g+p),
								(<, k, n),
								(>=, k+p, n),
								(<=, k+p, 3*p)
							)
						),
						(>, k+p, 3*p)
							
					)
				)
			)
		)
	)
)
');
$REUSE=false;

$filename = 'Repetitivity_TM';
	$d = process_request( 
		$filename,
		$TM, 
		array('i'),
		$expr
	);
fsa_visualize($d); 
//exit;

$REUSE=true;

foreach( $candidates as $i => $recp) {

	echo "Candidate #". $i. ":\n";
	$cand_msd = DFAO_builder($recp);
	///fsa_visualize($cand_msd);

	$cand = NFAO_reverse($cand_msd);
	//fsa_visualize($cand);

	$filename = 'Repetitivity' . '_cand' . $i;
	$d = process_request( 
		$filename,
		$cand, 
		array('i'),
//array('\out=', 'k+i', 'k+i+p')
		$expr
		//array('n','i'),
		//array('\out=', 'n','i') 
	);
exit;
}


?>
