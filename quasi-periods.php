<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');

/*
foreach( array('Thue-Morse' => $TM,'Rudin-Shapiro' => $RS, 'Paper-folding' => $PF, 'Period-Doubling' => $PD,'Stewart-choral'=>$SC, 'Mephisto-Waltz'=>$MW) as $name => $seq ) {
$REUSE = false;
		echo $name . ":\n";

		$filename = 'Quasi-Period' . '_' . $name;

		$expr = parse_expr('
		(\forall, l,
			(\exists, j,
				(\and,
					(<=, j, l),
					(>, j+n, l),
					(\factor, n, i, j)
				)
			)
		)');


		$d = process_request( 
			$filename,
			$seq, 
			array('n','i'),
			$expr
		);

		//show_and_save($d, $filename);
		if( count($d->end()) != 0 ) { // the sequence is Ultimately periodic
			$log->entry("\t is quasi periodic.", microtime(true) - $time_start );
		}


}

exit;
//*/

$REUSE = false; // setting REUSE to true ensures all calculations are redone when this code is run (usually previous calculations are cached)
$mode = $REUSE ? 'a' : 'w';
$log = new Log( 'Quasi-Period-Search', $mode );
$total_start = microtime(true);	

for( $i = 0; $i < 128; $i++) {

	$time_start = microtime(true);	

	$i_2 = '0'. str_pad (decbin ( $i ), 7, '0',STR_PAD_LEFT );

	$a = substr($i_2, 0,4);//'01001010';
	$b = substr($i_2, 4,4);//'01010010';

	//echo $a . ";".$b. "\n";
	$seq_msd = DFAO_builder( array('0' => $a, '1' => $b));
	$seq = DFAO_reverse( $seq_msd );


	/* 
	// print out the first 50 terms of the sequence	
	for($i=0; $i<50; $i++) {
	 	$ans = dfa_accepts ($seq, array($i),  2 );
		echo $ans['output'];
	}
	echo "\n";*/

	$name = $a . ',' .$b;
	
	$log->entry('0->'.$a.', 1->'.$b, microtime(true) - $time_start );

	$filename = 'Periodic' . '_' . $name;
	$expr = parse_expr('
		(\and, 
			(>, p, 0),
			(\forall, j,
				(\or,
					(\out=, j, j+p)
				)
			)
	)');

	$d = process_request( 
		$filename,
		$seq, 
		array('p'),
		$expr
	);

	//show_and_save($d, $filename);


	if( count($d->end()) != 0 ) { // the sequence is Ultimately periodic
		$log->entry("\t is periodic.", microtime(true) - $time_start );
	} else 	{
		// ultimately periodic => quasi periodic so there is no point to check

		$filename = 'Quasi-Period' . '_' . $name;

		$expr = parse_expr('
		(\forall, l,
			(\exists, j,
				(\and,
					(<=, j, l),
					(>, j+n, l),
					(\factor, n, i, j)
				)
			)
		)');


		$d = process_request( 
			$filename,
			$seq, 
			array('n','i'),
			$expr
		);

		//show_and_save($d, $filename);
		if( count($d->end()) != 0 ) { // the sequence is Ultimately periodic
			$log->entry("\t is quasi-periodic.", microtime(true) - $time_start );
		}
	
	}

}

$log->entry("[total]", microtime(true) - $total_start );

?>
