<?php
date_default_timezone_set('UTC');

class FSA {
	// private fields
	public $_start = array();
	public $_final = array();

	// property declaration
	public $delta = array();
	public $states = 0;

	// context variables
	public $k = 0;
	public $w = 1;
	public $vars;
	public $ks;

	public $mealy = array();

	public function __construct( $other = NULL ) {
		if( $other != NULL ) {
			if(!isset($other->k)) {print_r($other); exit;}
			$this->k = $other->k;
			$this->w = $other->w;
			$this->vars = $other->vars;
		}
	}
	
	public static function from_array( $ar ) {
		$m = new FSA;
		if(isset ($ar['delta'])) $m->delta = $ar['delta'];
		if(isset ($ar['start'])) $m->_start = array_flip($ar['start']);
		if(isset ($ar['final'])) $m->_final = array_flip($ar['final']);
		if(isset ($ar['output'])) $m->output = $ar['output'];
		if(isset ($ar['w'])) $m->w = $ar['w'];
		if(isset ($ar['k'])) $m->k = $ar['k'];
		if(isset ($ar['mask'])) $m->mask = $ar['mask'];
		$m->states = count($ar['delta']); // hack?
		return $m;
	}

	public function generate_sigma( $ks = null ) {
		if($this->k == 0) {print_r($this);}
		assert('$this->k!= 0');
		$sigma = array();

		if(!isset($ks)) { $ks = array_fill(0, $this->w, $this->k); }
		//else { }
		$range = 1;
		foreach( $ks as $k) {
			$range = $range * intval($k);
		}
//		$range = pow($this->k, $this->w);
		//$start = $include_zero ? 0 : 1;
		for($j = 0; $j < $range; $j++) {
			$in = array();
			$n = $j;
			foreach( $ks as $k) {
				$in[] = $n % $k;
				$n = floor($n/$k);
			}
			//$sigma[] = input_implode(num_split($j, $this->k, $this->w));
			$sigma[] = input_implode($in);
		}
		sort($sigma);
		return $sigma;
	}

	public function zero_label(){
		return input_implode(array_fill(0, $this->w, 0));
	}

	// start state access
	public function start() {
		$ret = array_keys($this->_start);
		sort($ret);
		return $ret;
	}
	public function start_add( $state ) {
		$this->_start[$state] = 1;
	}
	public function start_remove( $state ) {
		unset($this->_start[$state]);
	}
	public function start_contains( $state ) {
		return isset($this->_start[$state]);
	}

	// end state access
	public function end() {
		$ret = array_keys($this->_final);
		sort($ret);
		return $ret;
	}
	public function end_add( $state ) {
		$this->_final[$state] = 1;
	}
	public function end_remove( $state ) {
		unset($this->_final[$state]);
	}
	public function end_contains( $state ) {
		return isset($this->_final[$state]);
	}
	public function end_copy( $state_set ) {
		$this->_final = $state_set;
	}

}



function DFAO_builder( $rel ) {
	$letters = array_keys($rel);
	$map = array_flip($letters);
	
	$ret = new FSA();
	$ret->output = $letters;
	$ret->states = count($letters);
	$ret->start_add( '0' );
	
	$remap = function($in) use (&$map) { return array($map[$in]); };
	foreach( $rel as $from => $to ) {
		$w = str_split($to);
		assert('($ret->k == 0) || ($ret->k == count($w))');
		$ret->k = count($w);
		$s = $map[$from];
		$ret->delta[$s] = array_map( $remap, $w);
		$ret->end_add($s);
	}
	return $ret;
}

function DFAO_reverse( $m ) {
	$D = array_values(array_unique($m->output));
	$d = count($D);
	$map = array_flip($D);
	$remap = function($in) use (&$map) { return $map[$in]; };
	$inputs = array_keys($m->delta['0']);


	$ret = new FSA();
	$ret->k = $m->k;
	$states = $m->states;
	$ret->states = pow($d, $states);

	// create a state map so that the states are numbered nicely
	$state_map = array();
	for($s =0; $s < $ret->states; $s++) {
		$n = array_map( $remap, num_split( $s, $d, $states));
		$state_map[implode(',', $n)] = $s;
		$ret->end_add($s);
	}
	//print_r($state_map);

	// add the start state
	$start = implode(',', $m->output);
	assert('isset($state_map[$start])');
	$ret->start_add( $state_map[$start] );

	// fill out delta
	foreach($state_map as $ar => $s) {
		$n = explode(',', $ar);
		$g = function($in) use (&$n) { return $n[$in]; };

		// set the output
		$ret->output[$s] = $n[0];
		// set delta
		$ret->delta[$s] = array();
		foreach( $inputs as $in ) {
			$e = array();
			foreach( $m->delta as $q => $delta ) {
				$e[] = $g($delta[$in][0]); // THIS ASSUMES THE STATES ARE LABELLED 0 ... n
			}
			$ret->delta[$s][$in] = array($state_map[implode(',', $e)]);
		}
	}
	return $ret;
}

function NFAO_reverse( $m ) {
	// G - Gamma is the output alphabet
	$G = array_values(array_unique($m->output));
	$g = count($G);
	$map = array_flip($G);
	//$remap = function($in) use (&$map) { return $map[$in]; };
	$inputs = array_keys($m->delta['0']);


	$ret = new FSA();
	$ret->k = $m->k;
	$q = $m->states;
	$ret->states = $g * $q;
	
	// state numbering function
	$state_map = function($s, $o) use ($q, $map) { return $map[$o]* $q + $s; };
	
	// add the start states
	for($s = 0; $s < $m->states; $s++) {
		$ret->start_add( $state_map($s, $m->output[$s]) );
	}
	
	assert('count($m->start()) == 1');
	
	// add the final states
	foreach($m->start() as $s){
		foreach($G as $o) {
			$ret->end_add( $state_map($s, $o) );
		}
	}
		
	// create the output function
	for($s = 0; $s < $m->states; $s++) {
		foreach($G as $o) {
			$ret->output[$state_map($s, $o)] = $o;
		}
	}
	
	// populate delta
	for($s = 0; $s < $m->states; $s++) {
		foreach($G as $o) {
			$t = $state_map($s, $o);
			$ret->delta[$t] = array();
		}
	}
	
				
	foreach( $m->delta as $p => $delta ) {
		foreach( $delta as $in => $ends ) {
			foreach($ends as $e) {
				foreach($G as $o) {
					$t = $state_map($e, $o);
					array_add($ret->delta[$t],$in, $state_map($p, $o));
				}
			}
		}
	}

		
	return $ret;
}

//Thue-Morse
$TM = DFAO_builder( array('0' => '01', '1' => '10'));

$TM_neg = DFAO_builder( array('1' => '10','0' => '01'));

//Period-Doubling
$PD_msd = DFAO_builder( array('1' => '10', '0' => '11'));
$PD = DFAO_reverse( $PD_msd );

$PD2_msd = DFAO_builder( array('0' => '01', '1' => '00'));
$PD2 = DFAO_reverse( $PD2_msd );



//fsa_write($PD); exit;

//$RS1 = DFAO_builder( array('A' => 'AB', 'B' => 'AC', 'C' => 'DB', 'D' => 'DC'));
$RS = FSA::from_array( array(
	'start' => array('0'),
	'final' => array('0','1','2','3'),
	'delta' => array(
		'0' => array('0' => array('0'), '1' => array('1')),
		'1' => array('0' => array('0'), '1' => array('3')),
		'2' => array('0' => array('2'), '1' => array('3')),
		'3' => array('0' => array('2'), '1' => array('1'))
	),
	'output' => array('0'=>'0', '1'=>'0', '2'=>'1', '3'=>'1' ),
	'k' => 2
));

//$PF2 =  DFAO_builder( array('11' => '1101', '01' => '1001', '10' => '1100', '00' => '1000') );
 
$PF1 = FSA::from_array(array(
	'start' => array('0'),
	'final' => array('0','1','2','3'),
	'delta' => array(
		'0' => array('0' => array('0'), '1' => array('1')),
		'1' => array('0' => array('3'), '1' => array('2')),
		'2' => array('0' => array('2'), '1' => array('2')),
		'3' => array('0' => array('3'), '1' => array('3'))
	),
	'output' => array('0'=>'x', '1'=>'0', '2'=>'0', '3'=>'1' ),
	'k' => 2
));

$PF = FSA::from_array(array(
	'start' => array('0'),
	'final' => array('0','1','2','3'),
	'delta' => array(
		'0' => array('0' => array('1'), '1' => array('0')),
		'1' => array('0' => array('2'), '1' => array('3')),
		'2' => array('0' => array('2'), '1' => array('2')),
		'3' => array('0' => array('3'), '1' => array('3'))
	),
	'output' => array('0'=>'1', '1'=>'1', '2'=>'1', '3'=>'0' ),
	'k' => 2
));


$BS_msd =  FSA::from_array(array(
	'start' => array('0'),
	'final' => array('0','1','2','3'),
	'delta' => array(
		'0' => array('0' => array('0'), '1' => array('1')),
		'1' => array('0' => array('2'), '1' => array('1')),
		'2' => array('0' => array('1'), '1' => array('3')),
		'3' => array('0' => array('3'), '1' => array('3'))
	),
	'output' => array('0'=>'1', '1'=>'1', '2'=>'0', '3'=>'0' ),
	'k' => 2
));


//$BS =  NFAO_reverse($BS_msd);

$BS =  FSA::from_array(array(
	'start' => array('0'),
	'final' => array('0','1','2'),
	'delta' => array(
		'0' => array('0' => array('1'), '1' => array('0')),
		'1' => array('0' => array('0'), '1' => array('2')),
		'2' => array('0' => array('2'), '1' => array('2'))
	),
	'output' => array('0'=>'1', '1'=>'1', '2'=>'0' ),
	'k' => 2
));

//print_r($BS); exit;

$MW_msd = DFAO_builder(array('0' => '001', '1' => '110')); 
$MW = $MW_msd;//DFAO_reverse($MW_msd);


$SC_msd = DFAO_builder(array('0' => '001', '1' => '011')); 

//$SS = DFAO_reverse($SS_msd);
$SC = FSA::from_array(array(
	'start' => array('0'),
	'final' => array('0','1','2'),
	'delta' => array(
		'0' => array('0' => array('1'), '1' => array('0'), '2' => array('2')),
		'1' => array('0' => array('1'), '1' => array('1'), '2' => array('1')),
		'2' => array('0' => array('2'), '1' => array('2'), '2' => array('2'))
	),
	'output' => array('0'=>'0', '1'=>'0', '2' => '1'),
	'k' => 3
));

$SS = $SC;

$KS = FSA::from_array(array(
	'start' => array('0'),
	'final' => array('0','1','2','3','4','5'),
	'delta' => array(
		'0' => array('0' => array('1'), '1' => array('2'), '2' => array('3')),
		'1' => array('0' => array('0'), '1' => array('4'), '2' => array('4')),
		'2' => array('0' => array('4'), '1' => array('0'), '2' => array('5')),
		'3' => array('0' => array('5'), '1' => array('5'), '2' => array('0')),
		'4' => array('0' => array('2'), '1' => array('1'), '2' => array('1')),
		'5' => array('0' => array('3'), '1' => array('3'), '2' => array('2'))
	),
	'output' => array('0'=>'1', '1'=>'1', '2' => '2', '3'=>'3', '4'=>'2', '5' => '3'),
	'k' => 3
));

$G98 = DFAO_builder(array(
	'a' => 'abcacdcbcdcadbdcbdbabcbdcacbabdbabcabdadcdadbdcbdbabdbcbacbcdbabdcdbdcacdbcbacbcdcacdcbdcdadbdcbca',
	'b' => 'dabdbcbabcbdcacbacadabacbdbadacadabdacdcbcdcacbacadacabadbabcadacbcacbdbcabadbabcbdbcbacbcdcacbabd',
	'c' => 'cdacabadabacbdbadbdcdadbacadcdbdcdacdbcbabcbdbadbdcdbdadcadabdcdbabdbacabdadcadabacabadbabcbdbadac',
	'd' => 'bcdbdadcdadbacadcacbcdcadbdcbcacbcdbcabadabacadcacbcacdcbdcdacbcadacadbdacdcbdcdadbdadcadabacadcdb')
);

$LCH = DFAO_builder(array(
	'0' => '0121021201210',
	'1' => '1202102012021',
	'2' => '2010210120102')
	);


// Characteristic sequence of powers of 2

$POW2 = FSA::from_array( array(
	'start' => array('0'),
	'final' => array('0','1','2'),
	'delta' => array(
		'0' => array('0' => array('0'), '1' => array('1')),
		'1' => array('0' => array('1'), '1' => array('2')),
		'2' => array('0' => array('2'), '1' => array('2'))
	),
	'output' => array('0'=>'0', '1'=>'1', '2'=>'0' ),
	'k' => 2
));

function fsa_TRUE($w, $k) {
	$m = new FSA();
	assert( '$k != 0' );
	$m->k = $k;

	// add the start and final state
	$m->start_add( 0 );
	$m->end_add( 0 );
	$m->states = 1;

	$m->w = $w;

	// populate delta
	foreach( $m->generate_sigma() as $a ) {
		array_add($m->delta[0], $a, 0);	
	}

	return $m;
}



?>

