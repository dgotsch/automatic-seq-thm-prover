<?php



function grail_read($m, $s) {

	$lines = explode("\r\n", $s);
	
	$k = 0; 	
	$states = 0;
	
	foreach( $lines as $line ) {
		$parts = explode(" ", $line);
		$parts = array_filter($parts, 'strlen'); // filter out empty elemets
		if( strpos( $line, '(START) |-') !== false) {
			//echo "\n adding ". $parts[2]. " as start state from '". $line . "'\n";
			$m->start_add( $parts[2] ); 
		} else if(strpos( $line, '-| (FINAL)') !== false) {
			//echo "\n adding ". $parts[0]. " as final state from '". $line . "'\n";
			$m->end_add(  $parts[0] ); 
		} else if(strlen($line)>0) {
			/*if(!isset($m->delta[$parts[0]])) { 
				$m->delta[$parts[0]] = array(); 
			}*/
			if( isset($m->delta[$parts[0]]) && isset($m->delta[$parts[0]][$parts[1]]) ){
				echo "not a DFA!\n";
				exit;
			}
			array_add($m->delta[$parts[0]] , $parts[1] , $parts[2] );
			$states = max( $states, $parts[0] , $parts[2] );
			$k = max($k, $parts[1]);
		}
	
	}
	
	$m->states = $states;
	$m->k = $k+1 ;
	return $m;
}


function grail_write($m, $return = false) {
	$ans = array();
	foreach($m->start() as $s) {
		$ans[] = '(START) |- ' . $s;
	}
	
	foreach( $m->delta as $s => $d) {
		foreach($d as $in => $ends) {
			$label = $in;
			foreach($ends as $end) {
				$ans[] = $s.' '.$label.' '.$end;
			}
		}
	}
	
	foreach($m->end() as $s) {
		$ans[] = $s . ' -| (FINAL)';
	}

	$answer = implode("\n", $ans);
	if( $return ) { return $answer; }
	echo $answer;
}


?>
