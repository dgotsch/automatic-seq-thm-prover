<?php
assert_options(ASSERT_BAIL,     true);

include("fsa_lib.php"); 

$REUSE = true;
$TERNARY_LOGIC = false;
$TERNARY_ZEROES = false;

function DFAO_cartesian_product($m1, $m2, $type, $k) {
	$w = $m1->w + $m2->w;

	$vars = array();
	for($i = 0; $i < $w; $i++){
		$vars[$i] = $i;
	}
	//print_r($vars);

	$n1 = remap_vars($m1, array_slice($vars, 0, $m1->w), $vars, $k);
	$n2 = remap_vars($m2, array_slice($vars, $m1->w), $vars, $k);
	$ret = fsa_cross( $n1, $n2, NULL, $type );
	return $ret;
}

function effective_zero_labels( $vars, $k) {
	
	if(count($vars) == 0) { return array(array()); }
	$var = array_pop($vars);
	$zeroes = effective_zero_labels( $vars , $k) ; // recursive call
//	print_r($zeroes);

	if( strpos($var, '$') === false ) {
		$append = function( &$ar ) { array_push($ar, '0'); return $ar; };
		return array_map($append, $zeroes);
	} else {
		$result = array();
		for($a = 0; $a < $k; $a++) {
			$append = function( &$ar ) use( $a) { array_push($ar, $a); return $ar; };
			$result = array_merge($result, array_map($append, $zeroes));
		}
		return $result;
	}
}

function comparer($w, $a, $b, $final, $k) {
	// HACKS
	$arr = array();
	for($i = 0; $i<$w; $i++){
		$arr[$i] = $i;
	}
	$arr[$a] = 'a';
	$arr[$b] = 'b';
	
	//print_r($arr);print_r(array_flip(array('a','b')));//exit;
	$m = fsa_comparer($final, $k);
	return remap_vars($m, array('a','b'), $arr, $k);
		
}


function extrema($m, $w, $i, $type, $k) {
	//fsa_visualize($m);
	$n = add_digit($m, $i, $k);
	//fsa_visualize($n);	

	//get_new_var( &$var_map )
	$n = fsa_product($n, comparer($w+1, $i+1, $i, $type, $k), 'intersect');
/*	$n = fsa_minimize($n, false); 
	$n = fsa_accept_trailing_zeros($n);*/

	$n = remove_digits($n, array($i+1));
	// also make sure to accept trailing zeroes
	$n = fsa_accept_trailing_zeros($n);
	$n = fsa_minimize($n, false);
	//fsa_visualize($n);	
	$n = fsa_complement($n);

	$n = fsa_product( $n, $m, 'intersect' );
	
	$n->vars = $m->vars;
	return $n;
}


function add_vars_to_map( $var_map, $vars ) {
	foreach( $vars as $var ) {
		assert('!isset($var_map[$var])');
		$var_map[$var] = count($var_map);
	}
	return $var_map;
}

function get_vars($inputs) {
	if( !is_array($inputs) ) {
		$vars = array();
		foreach( explode('+', $inputs) as $part ) {
			$parts = explode('*', $part);
			if( count($parts) == 1 ) {		
				$var = $parts[0];
			} else {
				$var = $parts[1];
			}
			if( !is_numeric($var) ) {
				$vars[] = $var;
			}
		}
		
	} else {
		$vars =  array_map('get_vars', $inputs);
		$vars = call_user_func_array( 'array_merge', $vars );
	}
	return array_values(array_unique( $vars));

}



function var_adder($var_map, $inputs, $k, $logical_run) {
  global $intersection, $TERNARY_ZEROES ;

	if( !is_array($inputs) ) {
		return var_adder_1D($var_map, $inputs, $k, $logical_run);
	}
	$c = count($inputs);
	if( $c == 1 ) {
		return var_adder_1D($var_map, $inputs[0], $k, $logical_run);
	} else {
		$half = ceil($c/2);
		$first = array_slice($inputs, 0, $half);
		$second = array_slice($inputs, $half);
		//print_r($first);
		//print_r($second);
		if( $TERNARY_ZEROES && $logical_run) {
			return fsa_product(	var_adder( $var_map, $first, $k, $logical_run ), 
								var_adder( $var_map, $second, $k, $logical_run ), 'intersect');
		} else {
			return fsa_cross(	var_adder( $var_map, $first, $k, $logical_run ), 
								var_adder( $var_map, $second, $k, $logical_run ), $intersection);
		}
	}
}


function var_adder_1D($var_map, $input, $k, $logical_run) {
	global $TERNARY_ZEROES;
	$constant = 0;
	$recipe = array_fill(0, count($var_map) , 0);
	
	// modify the input to deal with negation
	$input =  preg_replace(array('/-/', '/\+\+/', '/--/'), array('+-','+','+'), $input);
	
	$print = false;
	foreach( explode('+', $input) as $part ) {
		$parts = explode('*', $part);
		$factor = 1;
		if( count($parts) == 1 ) {
			$parts = explode('-', $part);
			if( count($parts) != 1 ) { // special case for variables of the form -n
				$factor = -1;
				$var = $parts[1];
				$print = true;
				//echo $part. " is of the form -n, reading as -".$var."\n";
			} else {
				$var = $parts[0];
			}
		} else {
			$factor = $parts[0];
			$var = $parts[1];
		}
		
		//echo "factor=" . $factor . " var =" . $var ."\n";
		if( !is_numeric($var) ) {
			if(! isset($recipe[$var_map[$var]]) ) { trigger_error("var '" . $var ."' is not defined \n". print_r($var_map, true), E_USER_ERROR); }
			$recipe[$var_map[$var]] += $factor;
		} else {
			if( !is_numeric($var) ) echo "The term '".$var."' in expression '".$input."' is not numeric\n";
			assert('is_numeric($var)'); 
			//echo "adding the constant '" . $var . "' \n";
			$constant += $var * $factor;
		}
	}
	//echo "constant=". $constant . "\n";
	
	if( $TERNARY_ZEROES && $logical_run ) {
		/*trigger_error("Error: Trying to run logical run (ensure zeroes). This is not implemented.\n", E_USER_ERROR);
		if($print) { trigger_error("Error: negative constant not allowed in ternary logic.\n", E_USER_ERROR); }

		print_r($recipe);
		$to_len = function($n) use($k) { return ($mult <= 0) ? 0 : floor(log($n, $k)); };
		$bias = $to_len($constant); 
		$zeroes_needed = function($mult) use ($bias) { return $to_len($mult -1) + $bias; };

		$recipe = array_map($zeroes_needed, $recipe);


		echo "\t after length conversion\n";
		print_r($recipe);*/
		$ret = ensure_length($recipe, $k, $constant); // special case for ensuring the length of parameters
		//fsa_visualize($ret);
	} else {
		$ret = combiner($recipe, $k, $constant);
		//if($print) {print_r($recipe); echo " negative constant.\n"; }
	}
	$ret->vars = array_flip($var_map);	
	return $ret;

}



function process_request( $name, $m, $vars, $req) {
	global $REUSE;
	$var_map = array_flip($vars);
	
	// special cases for input variables
	if( is_numeric($m) ) {
		echo "M is numeric\n";
		$k = $m;
		$m = null;
	} else {
		$k = $m->k;
	}
	if( is_string($req) ) {
		//echo "Parsing the requested expression.\n";
		$req = parse_expr($req);
	}


	$req = preprocess_req( $var_map, $req );


	$mode = $REUSE ? 'a' : 'w';
	$log = new Log( $name, $mode );

	
	$time_start = microtime(true);
	//print_r($req);
	$ret = process_req( $log, $m, $var_map, $req, $k);
	//$ret = fsa_accept_trailing_zeros($ret);
	$ret = fsa_minimize($ret, true);

	$time_end = microtime(true);	
	$entry = '[total] ('. $ret->states . ' states)';
	$log->entry($entry, $time_end - $time_start );

	return $ret;
}

function process_req( $log, $m, $var_map, $req, $k, $logical_run = false ) {
	global $REUSE, $TERNARY_LOGIC,  $TERNARY_ZEROES, $output_equal, $output_lessthan, $always_true;

	// First check if this was computed before.
	$name = md5(print_r($m, true) . print_r($var_map, true) . print_r($req, true) . $k . $logical_run );
	$filename = '/tmp/partial/' . $name . ".fsa";
	$w = count($var_map);


	if( $REUSE && is_readable($filename) ) {
		$ret = fsa_load($filename);
		$ret->k = $k;
		$ret->w = $w;
		return $ret;
	}


	$action =$req[0];
	$var = '';
	$deterministic = true;

	$log->depth ++;
	
	$range = null;
	$v_range = null;

	switch($action) {
		case '\min':
		case '\max':
			$i = 
			$req[1] = array($req[1]); //wrapper for the argument so that the code below in cases \min,\max is reused for \exists
		default:
	}

	switch($action) {
		case '\exists':
			$var_map = add_vars_to_map($var_map, $req[1]);
			if( count($req) == 4) {
				$range = process_req( $log, $m, $var_map, $req[2], $k, false);
				if($logical_run) { $v_range = process_req( $log, $m, $var_map, $req[2], $k, true); }
				array_splice($req, 2, 1); // remove it from the list
			}
		case '\min':
		case '\max':
			if( count($req) != 3 ) { echo "Error: Too many arguments. Can't parse at:"; print_r($req); }
			$var = implode(',', $req[1]);
			$prev = process_req( $log, $m, $var_map, $req[2], $k, $logical_run);
			$idxs = array();
			foreach( $req[1] as $v ) {
				if( !isset($var_map[$v]) ) { trigger_error("Variable '".$v."' is not defined.\n", E_USER_ERROR); }
				$idxs[] = $var_map[$v];
			}
			//echo  $action . ' ' . $var . ' [at ' . $i . '] (' . count($prev->delta) . " states) \n";
			$deterministic = false;
			break;
		case '<>':
		case '>=':
		case '<=':
		case '=':
		case '>':
		case '<':
			assert( 'count($req) == 3;' );
			$comparison = str_split($action);
			$var = $req[1].' '.$req[2];
			break;
		case '\out=':
		case '\out<':
			$type = ($action == '\out=') ? $output_equal : $output_lessthan;
			$var = input_implode($req[1]).' '.input_implode($req[2]);
			if( isset($m->mask) ) {
				$append = function( $a, $b) { return $a.$b; };
				foreach( array(1, 2) as $i ) {
					$req[$i] = array_map( $append, $req[$i], $m->mask);
				}
			}
			break;
		case '\and':
		case '\or':
		//case '\implies':
			if( count($req) != 3 ) { echo "Error1: Can't parse at:"; print_r($req); }
			$left = process_req( $log, $m, $var_map, $req[1], $k, $logical_run);
			$right = process_req( $log, $m, $var_map, $req[2], $k, $logical_run);
			break;
		case '\not':
			$prev = process_req( $log, $m, $var_map, $req[1], $k, $logical_run);
			break;
		case '\machine':
			assert('is_array($req[1])');	
			assert('is_object($req[2])');
			assert('is_a($req[2], "FSA");');
			break;
		default:
			trigger_error("Error2: Can't parse [".$action."]. at:". print_r($req,true), E_USER_ERROR);
	}
	$time_start = microtime(true);
	
	switch($action) {

		case '\exists':
			//$vars = $prev->vars;
			$old_vars = array_flip($var_map);
			$vars = array_diff($old_vars, $req[1]); // remove the desired variables
			//echo "\tremapping\n";
			//$d = remap_vars($prev, $old_vars, $vars, $k);

			if( $req[1] == 'f$' ) {
				echo "\tbefore remove digit\n"; fsa_visualize($prev);
			}
			if( !$TERNARY_ZEROES && $range != null ) {
				if( $logical_run ) {
					echo "WARNING: executing special range validity check.\n";
					$v_prev = process_req( $log, $m, $var_map, $req[2], $k, true);
				//fsa_visualize($range);
				//fsa_visualize($v_range);
				if($req[1] == 'j') fsa_visualize($v_prev);
					$d = fsa_product(fsa_complement($v_prev), $range, 'intersect');
				//fsa_visualize($d);
					$d = fsa_product($d, fsa_complement($v_range), 'union');
					$d = remove_digits($d, $idxs);
				//fsa_visualize($d);
					$d = fsa_minimize($d, false);
					$d = fsa_complement($d);
				if($req[1] == 'j')  fsa_visualize($d);
				} else {
					$d = fsa_product($prev, fsa_complement($range), 'intersect');
					$d = remove_digits($d, $idxs);
				}
			} else {
				$d = remove_digits($prev, $idxs);
			}

			$var_map = array_flip($vars);
			$d->vars = $vars;
			break;
		case '\min':
		case '\max':
			$type = ($action == '\min') ? array('<') : array('>');
			$d = extrema($prev, count($var_map), $idxs[0], $type, $k);
			break;
		case '\and':
		case '\or':
		case '\implies':
			$type = ($action == '\and') ? 'intersect' : 'union';
			//assert('isset($left->vars)');
			//assert('isset($right->vars)');
			//$vars = array_unique(array_merge( $left->vars, $right->vars));
		//echo "\tremapping\n";
			//$left = remap_vars($left, $left->vars, $vars, $k);
			//$right = remap_vars($right, $right->vars, $vars, $k);
			if( !$logical_run ) {
				$d = fsa_product($left, $right, $type);
			} else {
				$d = fsa_product($left, $right, 'intersect');
				//echo "\tandor stuff\n";fsa_visualize($d);
			}
			break;
		case '\not':
			if( !$TERNARY_LOGIC ) {
				$d = fsa_complement($prev);
			} elseif( !$logical_run ) {
				$right = process_req( $log, $m, $var_map, $req[1], $k, true);
				//echo "\tnot stuff\n"; fsa_visualize($right);
				$d = fsa_product( $right, fsa_complement($prev), 'intersect');
			} else {
				$d = $prev;
			}	
			break;
		case '<>':
		case '>=':
		case '<=':
		case '=':
		case '>':
		case '<':
			$vars = get_vars( array($req[1], $req[2]) );
			$left = var_adder($var_map, array($req[1], $req[2]), $k, $logical_run);
			//$left = var_adder(array_flip($vars), array($req[1], $req[2]), $k, $logical_run);
			if( !$logical_run ) {
				$right = fsa_comparer($comparison, $k);
				$d = mealy_composition($left, $right);
				//$d = fsa_accept_trailing_zeros($d);
			} else {
				//$d = fsa_TRUE($w, $k);
				$d = $left;
				$d->mealy = array();
				// echo "\tcompare stuff\n";fsa_visualize($d);
			}
			$d->vars = $vars;
			break;
		case '\out=':
		case '\out<':
			$vars =  get_vars( array_merge($req[1], $req[2]) );
			/*$left = var_adder($var_map, array_merge($req[1], $req[2]), $k, $logical_run); 
			//$left = var_adder(array_flip($vars), array_merge($req[1], $req[2]), $k, $logical_run); 
		//	fsa_write($left);fsa_write($right);
			$d = mealy_composition($left, $right);*/
			if( !$logical_run ) {
				$left = var_adder($var_map, array_merge($req[1], $req[2]), $k, $logical_run); 
				$right = DFAO_cartesian_product($m, $m, $type, $k);
				$right = fsa_minimize($right, false); // TODO: is this correct? why didn't I do do this earlier?
				$d = mealy_composition($left, $right);
			} else {
				//u$left = var_adder($var_map, $req[1][0].'+'.$req[2][0], $k, $logical_run); 
				$left = var_adder($var_map, array_merge($req[1], $req[2]), $k, $logical_run); 
				$d = $left;
				$d->mealy = array();
				//print_r( array_merge($req[1], $req[2]) );
				//echo "\tout stuff\n"; fsa_visualize($d);
			}
			echo "\tfirst minimize " .$d->states ." states \n";
			$d = fsa_minimize($d, false);
			break;
		case '\machine':
			$d = mealy_composition( var_adder($var_map, $req[1], $k, 0), $req[2]);
			break;
	}


	

	
	// Accept trailing zeroes
	$ALL_NUMBERS = strpos(implode(array_keys($var_map)), '$') === false;
	if( !$TERNARY_LOGIC || $ALL_NUMBERS ) {
		//echo "\tbefore accept\n"; fsa_visualize($d);
		$d = fsa_accept_trailing_zeros($d); 
		//echo "\tafter accept\n"; fsa_visualize($d);
		$d2= $d;
	}

	$before = $d->states;

echo "\tgot here " .$d->states ." states \n";
	// Optimize
//echo "\tminimize\n";
	$d = fsa_minimize($d, $deterministic);
//if( !$TERNARY_LOGIC || $ALL_NUMBERS ) {
//echo "\tafter minimize (". ($deterministic ? 'true':'false') .")\n"; //fsa_visualize($d);
//echo "\t they are ". (fsa_equal($d, $d2) ? '': 'not ') . "equal \n";
//}
	// Output statistics
	$time_end = microtime(true);
	$after = $d->states;
	$log->depth--;
	$entry = '['. $action . ' ' . $var . '] ('. $before . ' => ' . $after . ' states)';
	if($logical_run) $entry = '*'.$entry;
	$log->entry($entry, $time_end - $time_start);

	// Save our result for future use
	fsa_save($d, $filename);
	return $d;
}


function get_new_var( &$var_map ) {
	
	$var = "i";
	for( $i=0; isset($var_map['i'.$i]); $i++) {
	}
	
	//$var_map['i'.$i] = count($var_map);
	
	return 'i'.$i;
}

function preprocess_req( $var_map, $req) {
	$action =$req[0];
	$var = '';

	/*if( substr($action, 0, 1) == '\\' ) {
		$action = substr($action, 1);
	}*/

	switch($action) {
		case '\forall':
		case '\exists':
			if( !is_array($req[1]) ) {
				$req[1] = array($req[1]);
			}
			add_vars_to_map( $var_map, $req[1] ); 

		default:
	}

	switch($action) {
		case '\preceedes':
			$same = get_new_var( $var_map );
			$lmt = $req[1];
			$left = $req[2].'+'.$same;
			$right = $req[3].'+'.$same;
			return  preprocess_req( $var_map, 
				array('\exists', $same,
					array('\and', 
						array('<', $same, $lmt),
						array('\factor', $same, $req[2], $req[3]),
						array('\out<', $left, $right)
					) 
				)
			);

		case '\factor':
			$idx = get_new_var( $var_map );
			$lmt = $req[1];
			$left = $req[2].'+'.$idx;
			$right = $req[3].'+'.$idx;
			return  preprocess_req( $var_map, 
				array('\forall', $idx , 
					array('\or', 
						array('>=',  $idx , $lmt),
						array('\out=', $left, $right) 
					) 
				)
			);
		case '\forall':
			if( count($req) == 4 ) {
				return array('\not', array('\exists', $req[1], $req[2], preprocess_req( $var_map, array('\not', $req[3]))));
			} else {
				return array('\not', array('\exists', $req[1], preprocess_req( $var_map, array('\not', $req[2]))));
			}
		case '\exists':
		case '\min':
		case '\max':
			if( count($req) == 4 ) {
				return array($action, $req[1],  $req[2], preprocess_req( $var_map, $req[3]) );
			} else {
				return array($action, $req[1], preprocess_req( $var_map, $req[2] ));
			}
		case '\iff':
			assert('count($req) == 3');
			$left = preprocess_req( $var_map, $req[1] ); 
			$right = preprocess_req( $var_map, $req[2] ); 
			return array('\or',	array('\and', $left, $right),
										array('\and', array('\not', $left), array('\not', $right)));
		case '\implies':
			//assert('count($req) == 3');
		case '\and':
		case '\or':
			$ret = array($action);
			$c = count($req) - 1;
			if( $c == 1 ) {
				return preprocess_req( $var_map, $req[1] ); 
			} else {
				$half = ceil($c/2);
				$first = array_merge($ret, array_slice($req, 1, $half));
				$second = array_merge($ret, array_slice($req, $half+1));
				$ret[1] = preprocess_req( $var_map, $first ); 
				$ret[2] = preprocess_req( $var_map, $second ); 
			}
			return $ret;
		case '\not':

			if($req[1][0] == '\not') { return  preprocess_req( $var_map, $req[1][1] ); }
			return array($action, preprocess_req( $var_map, $req[1] ));
		case '<':
			return array('>', $req[2], $req[1]);
		case '<=':
			return array('>=', $req[2], $req[1]);
		case '=':
		case '>=':
		case '<>':
		case '>':
			return $req;
		case '\out=':
		case '\out<':
			foreach(array(1,2) as $i) {
				if ( !is_array($req[$i]) ) { $req[$i] = array($req[$i]); }
			}
			return $req;
		case '\machine':
			if(is_string($req[2])) {
				$var_name = substr($req[2], 1);
				if( isset( $var_name )) {
					$req[2] = $GLOBALS[$var_name];
				} else {
					trigger_error("Error4: Variable '$" .$var_name ."' is not defined." . print_r($req, true), E_USER_ERROR);
				}
			}
			return $req;
		default:
			trigger_error("Error3: Can't parse at:". print_r($req, true), E_USER_ERROR);
	}
}


function parse_expr( $s ) {

	// remove the comments, newlines, and whitespace
	$s = preg_replace(array('/%.+/', '/\n/', '/ /'), array('','',''), $s);
	
	return read_array($s);
}


function format_time( $sec ) {
	$ms = $sec * 1000;
	$ret = floor(($ms%60000)/1000).'.'.str_pad(floor($ms%1000),3,'0', STR_PAD_LEFT) .'s';
	if( $ms >= 60000) {
		$ret = floor($ms/60000).'m' . $ret;
	}
	return $ret;
}

class Log {

	private $fp;
	public $depth = 0;
	public function __construct($filename, $mode)  {
	  global $REUSE;

		$this->fp = fopen('results/' . $filename . '.log' , $mode);
		if( ! $REUSE ) fwrite($this->fp, 'Starting log at:' . date('l jS \of F Y h:i:s A') ."\n");
	}

	function __destruct() {
		fclose($this->fp);
	}
	public function entry( $s , $sec = 0) {
		$sec = round($sec, 3);
		$s = str_pad('', $this->depth). $s .' in ' . format_time($sec) . "\n" ;
		fwrite($this->fp, $s);
		echo $s;
	}
}

//*/
?>
