<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


foreach( array('Thue-Morse' => $TM, 'Paper-folding' => $PF, 'Rudin-Shapiro' => $RS, 'Leech' => $LCH) as $name => $seq ) {
	echo $name . ":\n";


	$filename = 'Squarefree' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n'),
		array('\not',
			array('\exists', 'j',
				array('\and', 
					array('\factor', 'n', 'j', 'j+n')
				)
			)
		)
	);

	$m = brzozowski(fsa_reverse($d));
	fsa_visualize($m, 'results/'.$filename.'.dot');

}




?>
