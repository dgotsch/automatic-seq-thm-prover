<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');
include('src/fsa_interface_grail.php');


//print_r(read_array('((0,1), (0,3), (2,3), (2,1))')); exit;

$REUSE = false;
//'[{a,b},{a,b},{[a,a],[b,b]}]'
//$m = fsa_generate('[a,b,{a,b}]','-regex'); 
//$m_per = fsa_permutation_language($m);
//fsa_visualize($m_per); exit;

for( $n = 4; $n <= 10; $n++ ) {
	$m = FSA::from_array(array(
		'start' => array('0'),
		'final' => array(),
		'delta' => array(),
		'k' => 2,
		'w' => 1,
		'states' => $n
	));

	$bound = ($n*$n + $n + 1)/3-1;

	//fsa_write($m);
	//fsa_visualize($m); exit;
	//echo $A->states;
	echo "\n\sc(L) = ". $m->states . ", expected bound = ". round($bound+1,2) . "\n";
	iterate_delta($m,0, $bound, array(0=>1));
//	parikh_generate_delta($m, $bound, true);
}

echo "\n\n\n";
exit;


//{[a,a],[b,b]}
$m = fsa_generate('[{a,b},{a,b},{[a,a],[b,b]}]','-regex'); 
//fsa_visualize($m); exit;


function fsa_write_compact($m) {
	$ret = "";

	$n = $m->states;
	for( $s=0; $s < $n; $s++) {
		if( isset($m->delta[$s]) )
			$t = $m->delta[$s];
		else 
			$t = array();

		$tran = array();
		foreach(array('a', 'b') as $a) {
			if( isset($t[$a]) ) 
				$tran[] = $t[$a][0];
			else
				$tran[] = ' ';
		}
		if( $m->end_contains($s) ) {
			$ret .= '['. input_implode($tran).']';
		} else {
			$ret .= '('. input_implode($tran).')';
		}
	}
	return $ret;
}
//echo -en "\r$i"
function iterate_delta($m, $s, &$bound, $reachable) {

	$n = $m->states;

	if($s == $n) {
		test_automaton($m, $bound); // we have a finished delta - we can test it.
	}

	if( !isset( $reachable[$s] ) ) {
		//print_r($reachable);
		return; //if the current state is not reachable, don't bother.
	}

	// this is a HACK
	$m->k = $n - $s;
	$m->w = 2; // size of the alphabet

	$sigma = array('a', 'b');
	foreach( $m->generate_sigma() as $tran ) {
		$new_reachable = $reachable;
		$m->delta[$s] = array(); // wipe the slate clean
		//echo $s. "|". $tran ."\n" ;
		$delta = input_explode($tran);
		$max = 0;
		foreach($delta as $a => $t) {
			//if( $t > 0 and  $t < $max ) continue 2; // no need to consider this delta an equivalent one will be considered
			$max = max( $max, $t);
			if($t > 0) {
				array_add($m->delta[$s], $sigma[$a], $s+$t);
				$new_reachable[$s+$t] = 1; // mark the state as reachable
			}
		}

		if( $max > 0 || $s == $n-1 ) {	// if $max is 0 then this is a dead end state - no need to consider unless it's the last state
			$m->end_add($s);
			iterate_delta($m, $s+1, $bound, $new_reachable); // recursive call with s being final
			$m->end_remove($s);

			if( $s < $n-1 ) { // the last state should be accepting
				iterate_delta($m, $s+1, $bound, $new_reachable); // recursive call
			}
		}
	}

}

function test_automaton($m, &$bound) {

	$n = $m->states;
	//fsa_visualize($m);
	//fsa_write($m);
	echo "\r". fsa_write_compact($m);
//	echo "r=". fsa_write_compact($m)."\n";
	$m_min = fsa_minimize($m, false);

	if( $m_min->states == $n) {
		//echo $n. "state machine\n"; /*
		// try the permutations
		$m_per = fsa_permutation_language($m_min);
		// check if many states resulted
		if($m_per->states > $bound ) {
			echo "\nwhile \sc(\per(L)) = ". $m_per->states . "\n";// exit;
			show_and_save(fsa_reverse($m), 'Permutations-exceed-bound sc(L)='.$n.', sc(per(L))='.$m_per->states, 'eps');
			
			$bound = $m_per->states;
		}//*/
	}		
	return;
}
/*
function test_delta($m, $start, &$bound) {

	$n = $m->states;

	$m->k = 2;
	$m->w = $n- $start; only the last few states need to be checked

	$sigma = array('a', 'b');
	foreach( $m->generate_sigma() as $tran ) {

	for(
	
	return;
}*/


function parikh_generate_delta(&$m, &$bound) {


$iter = array( 'a' => 0, 'b' => 0, 'c'=>0, 'da' => 2, 'db' => 3, 'e' => $m->states);

while( parikh_iterate_delta($m, $iter, $bound) ) {

	test_delta($m, $bound);
	print_r($iter);
}

}

function parikh_iterate_delta(&$m, &$iter, &$bound) {

	extract($iter);


	while( $b < $e ) { // $b marks the end of 'a' transitions
		// add the appropriate states for the 'a' section
		for( $s = $a; $s < $b; $s++) $m->delta[$s] = array('a'=>array($s+1));
		//
		while( $c < $e ) {// $c marks the end of 'b' transitions
			// add the appropriate states for the 'b' section
			for( $s = $b; $s < $c; $s++) $m->delta[$s] = array('b'=>array($s+1));
			//
			
			while( $da < $e-1 ) { // $da marks the end of the 'a' block

				if(!isset($a_iter)) {
					$a_iter = array('a'=>$c+1,'b'=>$c+1,'c'=>$c+1,'da'=>$c+3,'db'=>$c+4,'e'=>$da);
				}
				
				// iterate through all the options for delta in the 'a' block section
				while( parikh_iterate_delta($m, $a_iter, $bound) ) {
					while( $db < $e ) { // $db marks the end of the 'b' block

						// add the transitions leading to the blocks
						$m->delta[$c] = array('a'=>array($c+1),'b'=>array($da));

						// we need to update the transitions point to da to point to db instead
						//echo "s=".fsa_write_compact($m) . "\n"; 
						for( $s = $c+1; $s < $da; $s++) {
							foreach($m->delta[$s] as $sym => &$tran ) {
								foreach($tran as $i => &$out ) {
									if( $out == $da ) $out = $db;
								}
							}
						} 
						//echo "s=".fsa_write_compact($m). "\n"; 

						if(!isset($b_iter)) {
							$b_iter = array('a'=>$da,'b'=>$da,'c'=>$da,'da'=>$da+2,'db'=>$da+3,'e'=>$db);
						}


						// iterate through all the options for delta in the 'a' block section
						while( parikh_iterate_delta($m, $b_iter, $bound) ) {
							//echo "s=".fsa_write_compact($m). "\n"; 

							// the rest is a+b transitions
							// add the appropriate states for the 'a+b' section
							for( $s = $db; $s < $e; $s++) 
								$m->delta[$s] = array('a'=>array($s+1), 'b'=>array($s+1));
							//
							//DONE GENERATING:
							echo "returning delta1\n";	
							$iter = compact('a', 'b', 'c', 'da', 'db', 'a_iter', 'b_iter', 'e');
//print_r($iter);
							return true;

						}
						$db++;
					}
				}
				$da++;
				$db=$da+1;
			}
			// ALTERNATIVELY THERE ARE NO BLOCKS:
			for( $s = $c; $s < $e; $s++) 
				$m->delta[$s] = array('a'=>array($s+1), 'b'=>array($s+1));
			//
			//DONE GENERATING:
			echo "returning delta2\n";

			$c++;
			$da= $c+2;

			$iter = compact('a', 'b', 'c', 'da', 'db', 'e');
//print_r($iter);
			return true;
		}
		$b++;
		$c=$b;
	}

	echo "returning false3\n"; //print_r($iter);
	return false;
}


function fsa_permutation_language($m) {

	$accepted = fsa_execute( $m, '-produce', true);
	$accepted = explode("\n", $accepted);
	array_pop($accepted); // the last entry is the empty string and not part of the language
	//print_r($accepted);

	$parikh = array();

	foreach( $accepted as $w ) {
		$i = substr_count ( $w , 'a' );
		if(!isset($parikh[$i])) $parikh[$i] = array();
		$parikh[$i][substr_count ( $w , 'b' )] = true;
	}

	//print_r( $parikh); exit;

	$n = $m->states;
	//echo "\sc(L) = ". $n . "\n";

	$ret = new FSA($m);
	$ret->start_add(0);
	$ret->states = ($n*$n + $n +1); // actually overkill


	for($i=0; $i<=$n; $i++) {
		for($j=0; $j<=$n; $j++) {
		
			$s = state_mult($j, $i, $n+1);
			if(  ($i+$j) <= $n ) {
				$ret->delta[$s] = array();
		
				if(  ($i+$j) < $n ) {
					$ret->delta[$s] = array();
				//	array_add($ret->delta[$s], 'c', $s);
					array_add($ret->delta[$s], 'a', state_mult($j, $i+1, $n+1));
					array_add($ret->delta[$s], 'b', state_mult($j+1, $i, $n+1));
				} 

			
				if (isset($parikh[$i][$j])) {
					//echo $i . ":" .$j . "=" . $s . "\n";
					$ret->end_add($s);
				}

			}
		}
	}

	//fsa_visualize($ret);
	//fsa_write($ret);

	//fsa_write($M); 
	//$ret = fsa_determinimize($ret);
	$ret = fsa_minimize($ret);
	//echo "while \sc(\per(L)) = ". $ret->states . "\n";
	return $ret;
}

$n = fsa_permutation_language($m);

fsa_visualize($n);

exit;

?>

