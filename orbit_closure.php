<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');

/*
for( $i=0; $i<200; $i++ ) {
	//print_r( make_tape(array($i) ,13));
	$ans = dfa_accepts($LCH, array($i), 13);
	echo $ans['output'];

}

echo "\n";

fsa_visualize($LCH); 
exit;//*/



$expr = parse_expr('
(\exists, i,
	(\and,
		(\forall, j,
			(\or,
				(\exists, d,
					(\and,
						(<, d, n+1), 
						(\factor, d, i, j),
						(\out<, j+d, i+d)
					)
				),
				(\factor, n+1, i, j)
			)
		),
		(\out=, i+n, 0)
	)
)
');


foreach(  array( 'Thue-Morse' => $TM, 'Period-Doubling' => $PD,  'Paper-folding' => $PF, 'Rudin-Shapiro' => $RS) as $name => $seq ) {
	echo $name . ":\n";
//$REUSE = false;

	$filename = 'OrbitClosureMost' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n'),
		$expr 
	);

	for($n=0; $n<80; $n++ ){
	 	$ans = dfa_accepts ($seq, array($n),  2 );
		echo  ($ans['output']) ;
	}
	echo "\n";
	for($n=0; $n<80; $n++ ){
	 	$ans = dfa_accepts ($d, array($n),  2 );
		//if ($ans['accept']) echo decbin($i). ":". $i.",".$n.";\n";
		echo  ($ans['accept']) ? '0' : '1';

	}
	echo "\n";
	show_and_save($d, $filename);//*/

}




?>
