<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


foreach( array( 'Stewart-choral'=>$SC, 'Thue-Morse' => $TM,  'Period-Doubling' => $PD, 'Rudin-Shapiro' => $RS, 'Paper-folding' => $PF) as $name => $seq ) {
	echo $name . ":\n";


$REUSE = false;

	$length = 'n+1';
	if ($seq == $TM ) {
		$length = 'n+1';
	} else if($seq == $RS) {
		$length = '3*n+1';
	} else if($seq == $PD) {
		$length = '3*n';
	} else if($seq == $SC) {
		$length = '2*n';
	}

	$filename = 'Power-Avoidance' . '_' . $name;
	// looking for powers
	//             i  n   n
	$d = process_request( 
		$filename,
		$seq, 
		array('i'),
		array('\exists', 'n', 
			array('\and', 
				array('>', 'n', '0'),
				array('\factor', $length, 'i', 'i+n'),
			)
		)
	);
	show_and_save($d, $filename,'eps');


	$filename = 'Power-Avoidance-confirm' . '_' . $name;
	// looking for powers
	//             i  n   n
	$d = process_request( 
		$filename,
		$seq, 
		array('i','n'),
		//array('\exists', 'n', 
			array('\and', 
				array('>', 'n', '0'),
				array('\factor', $length.'-1', 'i', 'i+n'),
			)
		//)
	);
	show_and_save($d, $filename,'eps');

/*

	$filename = 'Power-Avoidance-largest' . '_' . $name;
	// looking for powers
	//             i  n   n
	$d = process_request( 
		$filename,
		$seq, 
		array('n'),
		array('\exists', 'i',
			array('\and', 
				array('>', 'n', '0'),
				array('\factor', $length.'-1', 'i', 'i+n'),
			)
		)
	);
	show_and_save($d, $filename,'eps');*/


	continue;

/*

	$filename = 'Differing tail' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n'),
		array('\forall', 'i',
			array('\exists', 'j',
				array('\and', 
					array('>', 'j', 'i'),
					array('\not', 
						array('\out=', 'j', 'j+n')
					)
				)
			)
		)
	);
	$m = brzozowski(fsa_reverse($d));
	fsa_visualize($m, 'results/'.$filename.'.dot');
	continue;*/
/*
	$filename = 'Overlap' . '_' . $name;
	// looking for a x a x a
	//             i  n   n
	$d = process_request( 
		$filename,
		$seq, 
		array('i'),
		array('\exists', 'n', // n = |ax|
			array('\and', 
				array('>', 'n', '0'),
				array('\factor', 'n+1', 'i', 'i+n'),
				//array('\not', 
				//	array('\out=', 'i', 'i+n')
				//)
			)
		)
	);
	show_and_save($d, $filename);*/
	
}


?>
