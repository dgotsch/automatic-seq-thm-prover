<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');

/*
for( $i=0; $i<200; $i++ ) {
	//print_r( make_tape(array($i) ,13));
	$ans = dfa_accepts($LCH, array($i), 13);
	echo $ans['output'];

}

echo "\n";

fsa_visualize($LCH); 
exit;//*/



$expr = parse_expr('
(\exists, p,
	(\and,
		(>, p, 0),	
		(\forall, i0,
			(\or,
				(\out=, i0+i, i0+i+p),
				(>=, i0+p, n)
			)
		),
		(=, 15*p, 8*n)
	)
)
');

$expr2 = parse_expr('
(\exists, p,
	(\and,
		(\exists, q,
			(\and,
				(>, p, 0),	
				(=, q+p, n),
				(\factor, q, i, i+p)
			)
		),
		(<, 15*p, 8*n)
	)
)
');

$expr2 = parse_expr('
(\exists, p,
	(\and,
		(>, p, 0),	
		(\forall, i0,
			(\or,
				(\out=, i0+i, i0+i+p),
				(>=, i0+p, n)
			)
		),
		(<, 15*p, 8*n)
	)
)
');


/*
$expr = parse_expr('
(\not,
	(\factor, n, i, i+n)
)
');*/


foreach( array('Thue-Morse' => $TM,  'Leech' => $LCH) as $name => $seq ) {
	echo $name . ":\n";
$REUSE = false;

	$filename = 'Power' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','i'),
		$expr 
	);

	$m =brzozowski(fsa_reverse($d));

	fsa_save($m, 'results/'.$filename.'.fsa');
	show_and_save($d, $filename);//*/

	$filename = 'Power+' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','i'),
		$expr2 
	);

	show_and_save($d, $filename);//*/


}




?>
