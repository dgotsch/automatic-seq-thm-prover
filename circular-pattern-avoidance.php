<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');

/*					(\exists, l, 
						(\and,
							(<=, l, n-r),
							(\factor, r-p, i+l, i+l+p)
						)
					),
					(\exists, s,
						(\and,
							(<=, l, n-r)	
							(\factor, r-p, i+l, i+l+p)
						)
					)
					*/

ini_set("memory_limit","16000M");

foreach(  array('Thue-Morse' => $TM) as $name => $seq ) {
	echo $name . ":\n";

/*
(Ea Eb Ep El Ej
(n>=1)&(l>=1)&(p>=1)&(p<=l)&(i+a<=j)&(l=a+b)&(j+b=i+n)&(2*l=5*p)&
(At (k<l-p)=> (
(((k<b)&(T[j+k]=1))|((k>=b)&(T[i+k-b]=1))) iff
(((k+p<b)&(T[j+k+p]=1))|((k+p>=b)&(T[i+k+p-b]=1))) )) )
&
~( Ea Eb Ep El Ej
(n>=1)&(l>=1)&(p>=1)&(p<=l)&(i+a<=j)&(l=a+b)&(j+b=i+n)&(2*l>5*p)&
(At (k<l-p)=> (
(((k<b)&(T[j+k]=1))|((k>=b)&(T[i+k-b]=1))) iff
(((k+p<b)&(T[j+k+p]=1))|((k+p>=b)&(T[i+k+p-b]=1))) )) )
*/

$expr =
'(\and,
	(\exists, (a, b, p, l, j),
		(\and,
			(>=, n, 1),
			(>=, l, 1),
			(>=, p, 1),
			(<=, p, l),
			(<=, i+a, j),
			(=, l, a+b),
			(=, j+b, i+n),
			(=, 2*l, 5*p),
			(\forall, k,
				(\or, 
					(>=, k, l-p),
					(\iff,
						(\or,
							(\and, 
								(<, k, b),
								(\out=, j+k, 1)
							),
							(\and, 
								(>=, k, b),
								(\out=, i+k-b, 1)
							)
						),
						(\or,
							(\and, 
								(<, k+p, b),
								(\out=, j+k+p, 1)
							),
							(\and, 
								(>=, k+p, b),
								(\out=, i+k+p-b, 1)
							)
						)
					)
				)	
			)
		)
	),
	(\not,
		(\exists, (a, b, p, l, j),
			(\and,
				(>=, n, 1),
				(>=, l, 1),
				(>=, p, 1),
				(<=, p, l),
				(<=, i+a, j),
				(=, l, a+b),
				(=, j+b, i+n),
				(>, 2*l, 5*p),
				(\forall, k,
					(\or, 
						(>=, k, l-p),
						(\iff,
							(\or,
								(\and, 
									(<, k, b),
									(\out=, j+k, 1)
								),
								(\and, 
									(>=, k, b),
									(\out=, i+k-b, 1)
								)
							),
							(\or,
								(\and, 
									(<, k+p, b),
									(\out=, j+k+p, 1)
								),
								(\and, 
									(>=, k+p, b),
									(\out=, i+k+p-b, 1)
								)
							)
						)
					)	
				)
			)
		)
	)
)';

$filename = 'Circular-exponent-locations' . $name;
	$p = process_request( 
		$filename,
		$seq, 
		array('i','n'),
		$expr
	);

	show_and_save($p, $filename, 'png');
	fsa_save($p, 'results/'.$filename.'.fsa');exit;

	
/* 
exists i,j,a,b
	((i+a)<=j)&(p>=1)&(l>=1)&
		((for all k, 0 <= k < l)
			(((k+a<l)&(t[j+k]=1)) | ((k+a>=l)&(t[i+k+a-l]=1))) iff
			(((k+p+a<l)&(t[j+k+p]=1)) | ((k+p+a>=l)&(t[i+k+p+a-l]=1))))
*/
//$REUSE = false;
$expr =
'(\exists, i,
	(\exists, j,
		(\exists, a,
			(\and,
				(<=, i+a, j),
				(>=, p, 1),
				(>=, l, 1),
				(\forall, k,
					(\or, 
						(>=, k, l),
						(\iff,
							(\or,
								(\and, 
									(<, k+a, l),
									(\out=, j+k, 1)
								),
								(\and, 
									(>=, k+a, l),
									(\out=, i+k+a-l, 1)
								)
							),
							(\or,
								(\and, 
									(<, k+p+a, l),
									(\out=, j+k+p, 1)
								),
								(\and, 
									(>=, k+a+p, l),
									(\out=, i+k+a+p-l, 1)
								)
							)
						)
					)	
				)
			)
		)
	)
)';

	$filename = 'Circular-exponent-locations' . $name;
	$p = process_request( 
		$filename,
		$seq, 
		array('l','p'),
		$expr
	);

	show_and_save($p, $filename, 'png');
	fsa_save($p, 'results/'.$filename.'.fsa');
	

exit;
	$den = 7;
	$num = 13;
	

	$filename = 'Circular-exponent-atleast-' .$den .'over'. $num. '_' . $name;
	$p = process_request( 
		$filename,
		$seq, 
		array('i','n'), //s = start of gap, g = length of gap, r = n-g
		'(\exists, p,
			(\exists, s,
				(\exists, g,
					(\and,
						(=, '.$den.'*p+'.$num.'*g, '.$num.'*n),
						(>, p, 0),
						(<, s+g, n),
						(\forall, j,
							(\or,
								(\and, 
									(<, j, s), 
									(<, j+p, s), 
									(\out=, i+j, i+j+p)
								),
								(\and, 
									(<, j, s), 
									(>=, j+p, s), 
									(\out=, i+j, i+j+p+g)
								),
								(\and, 
									(>=, j, s), 
									(>=, j+p, s), 
									(\out=, i+j+g, i+j+p+g)
								),
								(>=, j+p, n-g)
							)
						)	
					)
				)
			)
		)'
	);
	
	


	show_and_save($p, $filename, 'png');

	$filename = 'Circular-exponent-morethan-' .$den .'over'. $num. '_' . $name;
	$q = process_request( 
		$filename,
		$seq, 
		array('i','n'), //s = start of gap, g = length of gap, r = n-g
		'(\exists, p,
			(\exists, s,
				(\exists, g,
					(\and,
						(<, '.$den.'*p+'.$num.'*g, '.$num.'*n),
						(>, p, 0),
						(<, s+g, n),
						(\forall, j,
							(\or,
								(\and, 
									(<, j, s), 
									(<, j+p, s), 
									(\out=, i+j, i+j+p)
								),
								(\and, 
									(<, j, s), 
									(>=, j+p, s), 
									(\out=, i+j, i+j+p+g)
								),
								(\and, 
									(>=, j, s), 
									(>=, j+p, s), 
									(\out=, i+j+g, i+j+p+g)
								),
								(>=, j+p, n-g)
							)
						)	
					)
				)
			)
		)'
	);
	
	show_and_save($q, $filename, 'png');
	
	$filename = 'Circular-exponent-exactly-' .$den .'over'. $num. '_' . $name;
	$q = process_request( 
		$filename,
		$seq, 
		array('i','n'), //s = start of gap, g = length of gap, r = n-g
		'(\and,
			(\machine, (i,n), $p),
			(\not, 
				(\machine, (i,n), $q)
			)
		)'
	);
	
}




?>
