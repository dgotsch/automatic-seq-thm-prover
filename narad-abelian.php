<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');



foreach( array('Thue-Morse' => $TM) as $name => $seq ) {
	echo $name . ":\n";
$REUSE = false;

	$d=process_request( 
		'thue-morse-comp-two-indices',
		$seq, 
		array('i', 'j'),
		'(\out=, i, j)'
	);
//	show_and_save($d, 'thue-morse-comp-two-indices', 'eps');
	//$d = remove_digit($d, 0);
	//show_and_save($d, 'thue-morse-comp-two-indices-exists', 'eps');
	$e=process_request( 
		'even',
		2, 
		array('n'),
		'(\exists, m, (=, n, 2*m))'
	);
	
	//show_and_save($e, 'even', 'eps');

	$o=process_request( 
		'odd',
		2, 
		array('n'),
		'(\exists, m, (=, n, 2*m+1))'
	);
	
/*

  We had a conjecture at the end about the
lengths n for which the Thue-Morse word had an abelian unbordered
factor of length n, as well as a logical formula to describe this set.

e(n) is true iff n is even
o(n) is true iff n is odd
t(n) is the Thue-Morse sequence

(e(i)∧e(n)∧e(k)), 
(e(i)∧e(n)∧o(k)∧t(i+k-1)=t(i+n-k)),
(e(i)∧o(n)∧e(k)∧t(i+n-k)=t(i+n-1)),
(e(i)∧o(n)∧o(k)∧t(i+k-1)=t(i+n-1)), 
(o(i)∧e(n)∧o(k)∧t(i)=t(i+n-1)),
(o(i)∧o(n)∧e(k)∧t(i)=t(i+k-1)), 
(o(i)∧o(n)∧o(k)∧t(i)=t(i+n-k)), or

(o(i)∧e(n)∧e(k)∧[
	(t(i)=t(i+n-k)∧t(i+k-1)=t(i+n-1))
∨	(t(i)=t(i+n-1)∧t(i+k-1)=t(i+n-k))
])
*/

	//$REUSE = false;

	$filename='Abelian-Squares-location' . '_' . $name;
	$m=process_request( 
		$filename,
		$seq, 
		array('n', 'i', 'k'),
		'(\or,
			(\and,
				(\machine, (i), $e),
				(\machine, (n), $e),
				(\machine, (k), $e)
			),
			(\and,
				(\machine, (i), $e),
				(\machine, (n), $e),
				(\machine, (k), $o),
				(\out=, i+k-1, i+n-k)
			),
			(\and,
				(\machine, (i), $e),
				(\machine, (n), $o),
				(\machine, (k), $e),
				(\out=, i+n-k, i+n-1)
			),
			(\and,
				(\machine, (i), $e),
				(\machine, (n), $o),
				(\machine, (k), $o),
				(\out=, i+k-1, i+n-1)
			),
			(\and,
				(\machine, (i), $o),
				(\machine, (n), $e),
				(\machine, (k), $o),
				(\out=, i, i+n-1)
			),
			(\and,
				(\machine, (i), $o),
				(\machine, (n), $o),
				(\machine, (k), $e),
				(\out=, i, i+k-1)
			),
			(\and,
				(\machine, (i), $o),
				(\machine, (n), $o),
				(\machine, (k), $o),
				(\out=, i, i+n-k)
			),
			(\and,
				(\machine, (i), $o),
				(\machine, (n), $e),
				(\machine, (k), $e),
				(\or, 
					(\and,
						(\out=, i, i+n-k),
						(\out=, i+k-1, i+n-1)
					),
					(\and,
						(\out=, i, i+n-1),
						(\out=, i+k-1, i+n-k)
					)
				)
			)
		)'
	);


	$filename='Abelian-Squares' . '_' . $name;
	$d=process_request( 
		$filename,
		$seq, 
		array('n'),
		'(\exists, i,
			(\forall, k,
				(\or, 
					(<, k, 1),
					(>, 2*k, n),
					(\not,
						(\machine, (n, i, k), $m)
					)
				)
			)
		)'
	);

	show_and_save($d, $filename, 'png');

		
}




?>
