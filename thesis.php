<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');



fsa_visualize($MW_msd);   

$d = process_request( 
	'Baum-Sweet',
	$MW, 
	array('n'),
	array('\out=', 'n', '0')
);
fsa_visualize($d);

exit;

$filename = 'Modulo_7';
$d = process_request( 
	$filename,
	2, 
	array('n'),
	array('\exists', 'm',
		array('=', 'n', '7*m+5')
	)
);

show_and_save($d, $filename);		
 exit;

foreach( array('Thue-Morse' => $TM,'Rudin-Shapiro' => $RS, 'Paper-folding' => $PF, 'Period-Doubling' => $PD) as $name => $seq ) {
	echo $name . ":\n";


$REUSE = false;
	$filename = 'Appearance' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','m'),
		array('\min', 'm',
			array('\forall', 'j',
				array('\exists', 'l',
					array('\and', 
						array('\factor', 'n', 'l', 'j'),
						array('>=', 'm', 'n+l')
					)
				)
			)
		)
	);
//exit;

	display_ratio($filename, $d); 


	$log_of = 'n-1';
	$log_is = 't';
	$lim = 4;
	if ($seq == $TM ) {
		$lim = 3;
		$relation =	array('=', 'p+3*t+n', 'm+1');
		//$restriction = '2*t+1';
	} else if($seq == $RS) {
		$relation =	array('=', 'p+13*t+n', 'm+1');
	} else if($seq == $PF) {
		$relation =	array('=', 'p+6*t+n', 'm+1');
		$lim = 5;
		$log_of = 'n';
		$restriction = 't';
	} else {
		$relation =	array('=', 'p+3*t+n', 'm+1');
		$lim = 2;
		$log_is = '2*t';
		$log_of = 'n';
	}

	$r = process_request(
		$filename,
		$seq,
		array('n','p'),
		array('\exists', 't',
			array('\exists', 'm',
				array('\and',
					array('\machine', array('n','m'), $d),
					array('>=', 'n', $lim),
					array('\machine', array($log_of, $log_is), $ceil_log2),
					//array('\machine', array('t'), $powerOfTwo),
					//array('>=', $restriction, 'n'),
					//array('<', $restriction, '2*n'),
					//array('=', 'p', '0'),
					$relation
				)
			)
		)
	);
	$m = brzozowski(fsa_reverse($r));
	fsa_visualize($m, 'results/'.$filename.'_confirm.dot');


	if( $seq == $PD ) {

		for($i=0; $i<30; $i++) {
			for($j=0; $j<13*$i; $j++ ){
				//$tape = make_tape(array($i,$j),2); 
				//print_r($tape); continue;

			 	$ans = dfa_accepts ($d, array($i,$j),  2 );
				if ($ans['accept']) echo decbin($i). ":". $i.",".$j.";\n";

			}
		}
		//exit;
	}

	
}


?>
