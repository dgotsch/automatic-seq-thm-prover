<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');
include('src/fsa_interface_grail.php');




$REUSE= false;
foreach( array( 'Thue-Morse' => $TM /*,'Rudin-Shapiro' => $RS,'Period-Doubling' => $PD,  'Paper-folding' => $PF*/) as $name => $seq ) {
	echo $name . ":\n";
	
	// { (n,i)_2 :  (1 <= i < n) and t[i] = 0 and t[i-1] = 0 }
	$filename = 'Jeff-dfa3-for-paper' . '_' . $name;
	
	$expr = parse_expr('
	(\and,
		(<, i, n),
		(>=, i, 1),
		(\out=, i, 0),
		(\out=, i-1, 0)
	)');
	
	

	$d = process_request( 
		$filename,
		$seq, 
		array('n','i'),
		$expr
	);

	show_and_save($d, $filename, 'png');
	
	$m =brzozowski(fsa_reverse($d));

	$A = fsa_matrix($m, 0, 2);
	echo "v:[".count($A['v'])."]\n";
	vector_write($A['v']);
	echo "w:[".count($A['w'])."]\n";
	vector_write($A['w']);
	echo "M_0:\n";
	matrix_write($A['0']);
	echo "M_1:\n";
	matrix_write($A['1']);
	
	exit;
	
	$filename = 'Jeff-dfa2-for-paper' . '_' . $name;
	//{ (N,p)_2 : N >= 0 and p >= 1 and t[N..N+p-1] = t[N+p..N+2p-1] }
	
	$expr = parse_expr('
	(\and,
		(>=, p, 1),
		(\forall, i,
			(\or,
				(\out=, n+i, n+p+i),
				(>=, i, p)
			)
		)
	)');
	
	

	$d = process_request( 
		$filename,
		$seq, 
		array('n','p'),
		$expr
	);


show_and_save($d, $filename);




	$filename = 'Jeff-dfa-for-paper' . '_' . $name;
	//{ (N,l) : there exists N' such that t[N..N+l-1] = reverse of t[N'..N'+l-1] }

	$expr = parse_expr('
		(\and,
		
		(\forall, i,
			(\or,
				(\out=, n+i, m+l-1*i),
				(>=, i, l)
			)
		)
	)');
	
	/*$expr = parse_expr('
	(\exists, m,
		(\and,
			(\forall, i,
				(\or,
					(>=, i, l),
					(\exists, j,
						(\and,
							(=, j+i, l),
							(\out=, n+i, m+j)			
						)	
					)
				)
			)
		)
	)');*/
	

	$d = process_request( 
		$filename,
		$seq, 
		array('n','l','m'),
		$expr
	);


show_and_save($d, $filename);



}

exit;


	$filename = 'jeff/zd4.txt';


	$fp = fopen($filename, 'r');
	$s = fread($fp, filesize($filename));
	fclose($fp);
	$m = grail_read(new FSA(), $s);
	
	//echo "m has ". $m->states. " states and k=". $m->k ." \n";
	$d = fsa_minimize($m, true);
	//echo "d has ". $d->states. " states\n";
	
	$s = grail_write($d, true);
	
	$fp = fopen('jeff/zd4_min.grail', 'w');

	fwrite($fp, $s);
	fclose($fp);
?>

