<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');

$REUSE = false;



$name ='Stewart-choral';
/*

for( $i=0; $i<200; $i++ ) {
	//print_r( make_tape(array($i) ,3));
	$ans = dfa_accepts($SS, array($i), 3);
	echo $ans['output'];

}

echo "\n";
//fsa_visualize(optimize_states($SS)); //*/
//$SS = $SS_msd;


/*
$d = process_request( 
	'test',
	$SS, 
	array('n'),
	array('\and',
		array('\not',
			array('\exists', 't',
				array('\and', 
					array('\machine', array('t'), $powerOfThree),
					array('\or',
						array('=', 't','n'),
						array('=', '2*t','n')
					)
				)
			)
		),
		array('>=','n','4')
	)
);

$m = brzozowski(fsa_reverse($d));
fsa_visualize($m);




$filename = 'Squarefree' . '_' . $name;
$d = process_request( 
	$filename,
	$SS, 
	array('n'),
	array('\not',
		array('\exists', 'j',
			array('\and', 
				array('\forall', 'i',
					array('\or', 
						array('>=', 'i','n'),
						array('\out=', 'j+i', 'j+n+i')
					)
				)
			)
		)
	)
);

$m = brzozowski(fsa_reverse($d));
fsa_visualize($m, 'results/'.$filename.'.dot');


$filename = 'Cubefree' . '_' . $name;
$d = process_request( 
	$filename,
	$SS, 
	array('n'),
	array('\not',
		array('\exists', 'j',
			array('\and', 
				array('\forall', 'i',
					array('\or', 
						array('>=', 'i','2*n'),
						array('\out=', 'j+i', 'j+n+i')
					)
				),
				array('>','n','0'),
			)
		)
	)
);

$m = brzozowski(fsa_reverse($d));
fsa_visualize($m, 'results/'.$filename.'.dot');*/

$filename = 'xxyyxx-free' . '_' . $name;
$d = process_request( 
	$filename,
	$SC, 
	array('n','m'),
	//array('\not',
		array('\exists', 'i',
			array('\and', 
				array('\factor', 'n', 'i', 'i+n'),
				array('\factor', 'm', 'i+2*n', 'i+2*n+m'),
				array('\factor', '2*n', 'i', 'i+2*n+2*m'),
				array('>','n','0'),
				//array('>','m','0')
			)
		)
	//)
);

show_and_save($d, $filename,'eps');




?>
