<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


foreach( array( 'Rudin-Shapiro' => $RS, 'Thue-Morse' => $TM, 'Paper-folding' => $PF, 'Period-Doubling' => $PD ) as $name => $seq ) {
	echo $name . ":\n";


	$filename = 'Squares' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','i'),
		//array('\exists', 'i',
		array('\and',
			array('\factor', 'n', 'i', 'i+n'),
			array('\not',
				array('\exists', 'j',
					array('\and',
						array('<', 'j', 'i'),
						array('\factor', '2*n', 'i', 'j')
					)
				)
			)
		)
		//)
	);

	$m = brzozowski(fsa_reverse($d));
	fsa_visualize($m, 'results/'.$filename.'.dot');
	shell_exec('dot -Tpng "results/'.$filename.'.dot" -o'.$filename.'.png');

	exit;

}




?>
