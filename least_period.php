<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');



$expr = parse_expr('
(\exists, q,
	(\and,
		(>=, l, p),
		(=, q+p, l), 
		(\factor, q, i, i+p),
		(>, p, 0)
	)
)
');

//print_r($expr); exit;

foreach( array('Thue-Morse' => $TM,  'Rudin-Shapiro' => $RS, 'Period-Doubling' => $PD,  'Paper-folding' => $PF  ) as $name => $seq ) {
	echo $name . ":\n";

$REUSE=false;
	$filename = 'Period' . '_' . $name;
	$period = process_request(
		$filename, 
		$seq, 
		array('p', 'i', 'l'),
		$expr
	);
	//show_and_save($period, $filename);



	$filename = 'ShortestPeriod' . '_' . $name;
	$shortest = process_request(
		$filename, 
		$seq, 
		array('p'),
		array('\exists', 'i', 
			array('\exists', 'l',
				array('\min', 'p', $expr)
			)
		)
	);

	show_and_save($shortest, $filename);

	continue;

	$filename = 'PeriodUnique' . '_' . $name;
	$u = process_request(
		$filename, 
		$seq, 
		array('n', 'i', 'l'),
		array('\and',
			array('\min', 'n', 
				array('\machine', array('n', 'i', 'l'), $period)
			),
		//	array('=', 'n', '3'),
			array('\not', 
				array('\exists', 'j',
					array('\and',
						array('<', 'j', 'i'),
						array('\factor', 'l', 'i', 'j')
					)
				)
			)
		)
	);
continue;
	//show_and_save($u, $filename);
/*
$n = 3;
		for($i=0; $i<30; $i++) {
			for($l=0; $l<40; $l++ ){
			 	$ans = dfa_accepts ($u, array($n,$i,$l),  2 );
				if ($ans['accept']) echo $n. ":". $i.",".$l.";\n";
			}
		}

exit;
*/

	$m =brzozowski(fsa_reverse($u));

	$A = fsa_matrix($m, 0, 2);
	echo "v:[".count($A['v'])."]\n";
	vector_write($A['v']);
	echo "w:[".count($A['w'])."]\n";
	vector_write($A['w']);
	echo "M_0:\n";
	matrix_write($A['0']);
	echo "M_1:\n";
	matrix_write($A['1']);
exit;

}



?>
