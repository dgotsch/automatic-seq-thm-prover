<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


foreach( array( 'Thue-Morse' => $TM,'Period-Doubling' => $PD, 'Rudin-Shapiro' => $RS, 'Paper-folding' => $PF) as $name => $seq ) {
	echo $name . ":\n";

	$filename = 'Separator' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','m'),
		array('\min', 'm',
			array('\and',
				array('>','m','1'),
				array('\not',
					array('\exists', 'j',
						array('\and', 
							array('\factor', 'm', 'j', 'n'),
							array('<', 'j', 'n')
						)
					)
				)
			)
		)
	);


	for($i=0; $i<30; $i++) {
		for($j=0; $j<13*$i; $j++ ){
		 	$ans = dfa_accepts ($d, array($i,$j),  2 );
			if ($ans['accept']) echo decbin($i). ":". $i.",".$j.";\n";
		}
	}


	display_ratio($filename, $d); 

		
	
}


?>
