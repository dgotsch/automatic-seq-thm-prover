<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');

$m3np1 = FSA::from_array(
array(
	'start' => array('0'),
	'final' => array('1'),
	'delta' => array(
		'0' => array('0,0' => array('0'), '1,0' => array('2')),
		'1' => array('0,0' => array('1'), '1,1' => array('3')),
		'2' => array('0,0' => array('3'), '1,1' => array('2')),
		'3' => array('0,1' => array('1'), '1,0' => array('2'))
	),
	'k' => 2	
	)
);
	

	
$m3np1base6 = process_request( 
	'Collatz_base6',
	3, 
	array('n','m'),
	//array('\exists', 'p', 
	//	array('\and', 
			array('=', '2*m', 'n')//,
	//		array('=', 'p', '3*n+1')
	//	)
	//)
);//*/
fsa_visualize($m3np1base6);
fsa_visualize(fsa_reverse($m3np1base6));
exit;

$n = 665215;
$pad = 0;

$i = 0;
do {
	
	while($n % 2 == 0 ) { $n = $n/2; $pad++; }
		
	$s = str_pad(sprintf("%b", $n), 280 - $pad, ' ',STR_PAD_LEFT);
	$s = str_replace('0', ' ', $s);
	$s = str_replace('1', '█', $s);
	//if($i++ % 2 == 0) 
	printf("%12d:%s\n", $n, $s);
//	echo decbin($n) . "\n";
	
	if($n == 1 && $i > 10) break;
	$n = (3*$n+1);
	//$pad++;
	
	//if($n % 2 == 0 ) { $n = $n/2; }
	$i++;
} while(true);

//exit;
$REUSE = false;
	$filename = 'Collatz_2017';

	

	//fsa_visualize($m3np1);
	
	$o=process_request( 
		'odd',
		2, 
		array('n'),
		'(\exists, m, (=, n, 2*m+1))'
	);
	
	
	$m3np1o2 = process_request( 
		$filename,
		2, 
		array('n','m'),
		array('\exists', 'p',
			array('\and', 
				array('\machine', array('n','p'), $m3np1),
				array('=', '2*m', 'p')
			)
		)
	);
	//fsa_visualize($m3np1o2);
	
	/*$m3np1o24 = process_request( 
		$filename,
		2, 
		array('n','m'),
		array('\exists', 'p',
			array('\or',
				array('\and', 
					array('\machine', array('n','p'), $m3np1),
					array('\machine', array('m'), $o),
					array('=', '2*m', 'p')
				),
				array('\and', 
					array('\machine', array('n','p'), $m3np1),
					array('=', '4*m', 'p')
				)
			)
		)
	);
	fsa_visualize($m3np1o24);
	*/
	//exit;
	
	$m_vert_w_o = FSA::from_array(
 	array(
		'start' => array('0'),
		'final' => array('0'),
		'delta' => array(
			'0' => array('0,0,0' => array('0'), '1,1,0' => array('0'), '2,3,0' => array('0'), '3,1,1' => array('1')),
			'1' => array('0,2,0' => array('0'), '1,3,1' => array('1'), '2,2,1' => array('1'), '3,2,0' => array('0'))
		),
		'k' => 4	
		)
	);
	
	$m_vert = process_request( 
		'3np1_vert',
		4, 
		array('n','m'),
			array('\exists', array( 'a'), 
				array('\machine', array('n','m','a'), $m_vert_w_o)
			)
	
	);
		
	//fsa_visualize($m_vert);
	
	$start_vert = FSA::from_array(
 	array(
		'start' => array('0'),
		'final' => array('2'),
		'delta' => array(
			'0' => array('1' => array('1')),
			'1' => array('0' => array('2'), '1' => array('1'), '2' => array('1'), '3' => array('1')),
			'2' => array('0' => array('2'))
		),
		'k' => 4	
		)
	);
	//fsa_visualize($start_vert);
	// iterating the vertical version of the collatz machine
	
	// the end cycle we're looking for is length 2 (1->4->16...)
	$end_vert = FSA::from_array(
 	array(
		'start' => array('0'),
		'final' => array('1'),
		'delta' => array(
			'0' => array('1' => array('0'), '2' => array('1'), '3' => array('1')),
			'1' => array('0' => array('1'))
		),
		'k' => 4	
		)
	);
		
	//fsa_visualize($end_vert);
	//exit;
	$proper_vert = FSA::from_array(
 	array(
		'start' => array('0'),
		'final' => array('1','2'),
		'delta' => array(
			'0' => array('1' => array('0'), '2' => array('1'), '3' => array('1')),
			'1' => array('1' => array('1'), '2' => array('1'), '3' => array('1'), '0' => array('2')),
			'2' => array('0' => array('2'))
		),
		'k' => 4	
		)
	);
	//fsa_visualize($proper_vert);
	
	
	$d = $end_vert;

	$REUSE = true;	
	for($i=1; $i < 16; $i++) {

		echo "=== ITERATION (vertical) # $i  ===\n";
		

		$d = process_request( 
			'3np1_vert_end-'.$i,
			4, 
			array('n'),
			array('\exists', 'm',
				array('\and', 
					array('\machine', array('m'), $d),
					array('\machine', array('n','m'), $m_vert)
				)
			)

		);

		//fsa_visualize($d);


	/*	for($a=1; $a < 100; $a++) {
			$ans = dfa_accepts ($d, array($a),  4);
			//echo ($ans['accept'] == false) ? '0' : '1';
			if ($ans['accept'] != false ) {
				//echo base_convert($a, 10, 4)."\n";
				
			//	echo  "First number not accepted in $i steps was: $a\n";
			//	break;
			}
		}*/
	/*
		$d = process_request( 
			$filename,
			4, 
			array('n','m'),
				array('\exists', array( 'p'), 
					array('\and', 
						array('\machine', array('n','p'), $m_vert),
						array('\machine', array('p','m'), $d)
					)
				)
		
		);
		
		
		$e = process_request( 
			$filename,
			4, 
			array('n'),
				array('\exists', array('m'), 
					array('\and', 
						//array('\machine', array('a'), $powerOfTwo),
						array('\machine', array('n','m'), $d)
					)
				)
	
		);
		fsa_visualize($e);*/
	}
	echo "first not accepted configuration:\n";
	$e = process_request( 
		'3np1_vert_end-'.$i.'eval',
		4, 
		array('n'),
		array('\min', 'n',
			array('\and', 
				array('\not', array('\machine', array('n'), $d)),
				array('\machine', 'n', $proper_vert)
			)
		)

	);

	//fsa_visualize($e);
	
	/*
	$in = array('2','1','1','1');
	
	$m = $m_vert;

	for($k = 0; $k < 80; $k++) {
		echo "input: ";
	
		$ignore = true;
	
		$s = $e->start()[0];
		$out = array();
		
		array_push( $in, '0');
		foreach( $in as $i ) {
			if($ignore && $i == '1' ) echo ' ';
			else { $ignore = false; echo $i; }	
			foreach( array('0','1','2','3') as $j ) {
				
				if( isset( $m->delta[$s][$i.','.$j] ) ) {
					$s = $m->delta[$s][$i.','.$j][0];
					array_push($out, $j);
				}
			}

		}
		$in = $out;	
		echo "\n";
	}
	
	exit;//*/
	
	$e = process_request( 
		'3np1_vert_end-'.$i.'eval',
		4, 
		array('n'),
		array('=', 'n', base_convert('1112', 4, 10))

	);
	//fsa_visualize($e);

		

	for($i=1; $i < 71; $i++) {
		$s = $e->start()[0];
		echo "input: ";

		$ignore = true;
		while($s != $e->end()[0] ) {
			foreach( array('0','1','2','3') as $in ) {
				if( isset( $e->delta[$s][$in] ) ) {

					$s = $e->delta[$s][$in][0];
					if($ignore && $in == '1' ) $in = ' ';
					else $ignore = false;					
					echo $in;
					break;
				}
			}
		}

		echo "\n";
	
		$e = process_request( 
			'3np1_vert_test-'.$i,
			4, 
			array('m'),
			array('\exists', 'n',
				array('\and', 
					array('\machine', array('n'), $e),
					array('\machine', array('n','m'), $m_vert)
				)
			)

		);
		
	}
	
	fsa_visualize($e);
	

	$REUSE = false;
	exit;
	$d = process_request( 
		$filename,
		4, 
		array('n','m'),
			array('\max', 'm', 
				array('\and', 
					//array('\machine', array('n','p'), $m_vert),
					array('\machine', array('n','m'), $d)
				)
			)
	
	);
	fsa_visualize($d);

	exit;	
	// trying to build up the collatz machine exponetially
	$d = $m3np1;
	for($i=1; $i < 3; $i++) {
		echo "=== ITERATION # $i : f^".(2**$i)." ===\n";
		$d = process_request( 
			$filename,
			$TM, 
			array('n','m'),
				array('\exists', 'p', 
					array('\and', 
						array('\machine', array('n','p'), $d),
						array('\machine', array('p','m'), $d),
					)
				)
		
		);
		//fsa_visualize($d);
	}
	for($i=0; $i<30; $i++) {
		for($j=0; $j<300*$i; $j++ ){
			//$tape = make_tape(array($i,$j),2); 
			//print_r($tape); continue;

		 	$ans = dfa_accepts ($d, array($i,$j),  2 );
			if ($ans['accept']) echo decbin($j). ":". $i.",".$j.";\n";

		}
	}
	//*/	
			
	//exit;
	$n = $powerOfTwo;
	//show_and_save($n, $filename, "eps");
	for($i=1; $i < 13; $i++) {
		echo "=== ITERATION # $i ===\n";
		$n = process_request( 
			$filename,
			$TM, 
			array('n'),
		//	array('\or',
			//	array('\machine', array('n'), $n),
				array('\exists', 'm', 
					array('\and', 
						//array('=', 'm', '3*n+1'),
						array('\machine', array('n','m'), $d),
						array('\machine', array('m'), $n)
					)
				)
		//	)
		);
		//show_and_save($n, $filename, "eps");
		//exit;
		for($a=1; $a < 100; $a++) {
			$ans = dfa_accepts ($n, array($a),  2);
			//echo ($ans['accept'] == false) ? '0' : '1';
			if ($ans['accept'] == false ) {
				echo $a."\n";
			//	echo  "First number not accepted in $i steps was: $a\n";
			//	break;
			}
		}	

	}
	

	/*
	$r = process_request( 
		$filename,
		$TM, 
		array('n','m'),
		array('\exists', 'p', 
			array('\and', 
				array('=', 'm', '3*p+1'),
				array('=', 'p', '3*n+1')
			)
		)
	);//*/


	//show_and_save($m, $filename, "eps");
	
	//$r = fsa_reverse($m);

	//show_and_save($r, $filename."_reverse", "eps");

?>
