<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


/*
0 -> 0123
1 -> 1032
2 -> 0132
3 -> 1023
01231032013210231032012310230132
*/

$GS_msd = DFAO_builder(array(
	'0' => '0123',
	'1' => '1032',
	'2' => '0132',
	'3' => '1023')
	);
$GS = NFAO_reverse($GS_msd);



/*
state q   delta(q,0)   delta(q,1)   output
0            1            2            0
1            3            4            0
2            5            2            1
3            3            3            0
4            3            6            0
5            5            7            1
6            8            4            1
7            7            5            0
8            8            8            1
*/

$JS = FSA::from_array(array(
	'start' => array('0'),
	'final' => array('0','1','2','3','4','5','6','7','8'),
	'delta' => array(
		'0' => array('0' => array('1'), '1' => array('2')),
		'1' => array('0' => array('3'), '1' => array('4')),
		'2' => array('0' => array('5'), '1' => array('2')),
		'3' => array('0' => array('3'), '1' => array('3')),
		'4' => array('0' => array('3'), '1' => array('6')),
		'5' => array('0' => array('5'), '1' => array('7')),
		'6' => array('0' => array('8'), '1' => array('4')),
		'7' => array('0' => array('7'), '1' => array('5')),
		'8' => array('0' => array('8'), '1' => array('8'))
	),
	'output' => array('0'=>'0', '1'=>'0', '2'=>'1', '3'=>'0', '4'=>'0', '5'=>'1', '6'=>'1', '7'=>'0', '8' => '1' ),
	'k' => 2
));

/*
	$name = 'Shallit';
	$seq = $JS;
	$filename = 'Squares' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n'),
		array('\and', 
			array('>=', 'n', '1'),
			array('\exists', 'i',
				array('\factor', 'n', 'i', 'i+n')
			)
		)
	);

	show_and_save($d, $filename, 'png');
	
	$filename = 'ComplementSquares' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n'),
		array('\and', 
			array('>=', 'n', '1'),
			array('\exists', 'i',
				array('\forall', 'j',
					array('\or',
						array('>=', 'j', 'n'),
						array('\not', array('\out=', 'i+j', 'i+j+n'))
					)
				)
			)
		)
	);

	show_and_save($d, $filename, 'png');

*/
foreach( array( 'Gareth-Shallit' => $GS, 'Shallit' => $JS,  'Thue-Morse' => $TM) as $name => $seq ) {
	echo $name . ":\n";


	for($i=0; $i<32; $i++ ){
	 	$ans = dfa_accepts ($seq, array($i),  $seq->k );
		echo $ans['output'];
	}
	echo "\n";

	$expr = parse_expr('
	(\exists, i,
		(\and,
			(>=, n, 1), 
			(\or, 
				(\factor, n, i, i+n),
				(\factor, n, i, i+n+1)
			)
		)
	)
	');
/*	$expr = parse_expr('
	(\exists, i,
		(\exists, k,
			(\and,
				(<, k, 2*n),
				(>, n, 0),
				(\forall, j,
					(\or,
						(\and, 
							(<, j, k), 
							(<, j+n, k), 
							(\out=, i+j, i+j+n)
						),
						(\and, 
							(<, j, k), 
							(>=, j+n, k), 
							(\out=, i+j, i+j+n+1)
						),
						(\and, 
							(>=, j, k), 
							(>=, j+n, k), 
							(\out=, i+j+1, i+j+n+1)
						),
						(>=, j, n)
					)
				)
			)
		)
	)
	');*/
	

	$filename = 'Almost-Squares' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n'),
		$expr
	);

	show_and_save($d, $filename, 'png');

		for($i=0; $i<60; $i++) {
			for($j=0; $j<30*$i; $j++ ){
			 	$ans = dfa_accepts ($d, array($i,$j),  $d->k );
				if ($ans['accept']) echo  base_convert($i, 10, $d->k). ":". $i.",".$j.";\n";

			}
		}

	/*for($i=0; $i<60; $i++) {
	 	$ans = dfa_accepts ($d, array($i),  $d->k );
		if ($ans['accept']) echo  base_convert($i, 10, $d->k). ":". $i.";\n";
	}*/
}




?>
