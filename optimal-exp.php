<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


$W_msd = DFAO_builder( array(
	'0' => '0121341243', 
	'1' => '4121012434', 
	'2' => '1213412413',
	'3' => '1213410123',
	'4' => '0131014103'
));
$W = DFAO_reverse( $W_msd );

//DFAO_minimize( $W );
fsa_visualize($W_msd);
fsa_visualize($W);
/*
$expr = parse_expr('
(\not,
	(\exists, p,
		(\and,
			(\exists, q,
				(\and,
					(>, p, 0),	
					(=, q+p, n),
					(\factor, q, i, i+p)
				)
			),
			(=, 2*p, n)
		)
	)
)
');*/
$expr = parse_expr('
(\not,
	(\factor, n, i, i+n)
)
');




//print_r($expr); exit;

foreach( array('W' => $W )  as $name => $seq ){
	echo $name . ":\n";


	$filename = 'OptimalExp' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','i'),
		$expr 
	);

	show_and_save($d, $filename);		
		

}



?>
