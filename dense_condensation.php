<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');

/*
foreach( array( 'Period-Doubling' => $PD, 'Thue-Morse' => $TM, 'Paper-folding' => $PF, 'Rudin-Shapiro' => $RS) as $name => $seq ) {
	echo $name . ":\n";
//$REUSE = false;
$filename = 'DenseCondensation' . '_' . $name;

	$d = process_request( 
		$filename,
		$seq, 
		array('n'),
		array('\exists', 'i',
			array('\exists', 'l',
				array('\and',
					array('\forall', 'j',
						array('\or',
							array('>=', 'j', 'l'),
							array('\not', 
								array('\exists', 'k',
									array('\and', 
										array('<', 'k', 'j'),
										array('\factor','n', 'i+j', 'i+k')
									)
								)
							)
						)
					),
					array('\forall', 'k',
						array('\exists', 'j',
							array('\and', 
								array('\factor', 'n', 'k', 'i+j'),
								array('<', 'j', 'l')
							)
						)
					)
				)
			)
		)
	);


	//display_ratio($filename, $d); 
	
	show_and_save($d, $filename);

	


}*/

$mode = $REUSE ? 'a' : 'w';
$log = new Log( 'Dense-Condensation-Search', $mode );

$k = 2;
$w = 3;
$dof = $k * $w;
for( $n = 0; $n < pow($w, $dof-2); $n++) {

	$time_start = microtime(true);	

	$i_3 = str_pad (base_convert ( $n, 10, $w ), $dof  , '0',STR_PAD_LEFT );

	$a = '01'; //substr($i_3, 0,$k);
	$b = '12';// substr($i_3, 2,$k);
	$c = '22'; //substr($i_3, 4,$k);
	if( strpos($b, '2') === false ) continue; 
	
	/*$i_2 = '0'. str_pad (decbin ( $w ), 7, '0',STR_PAD_LEFT );

	$a = substr($i_2, 0,4);//'01001010';
	$b = substr($i_2, 4,8);//'01010010';
	$seq_msd = DFAO_builder( array('0' => $a, '1' => $b));
	*/
	
	
	//$a = '001'; $b='000';

	//echo $a . ";".$b. ";" . $c. "\n";
	$seq_msd = DFAO_builder( array('0' => $a, '1' => $b, '2' => $c));
	$seq = DFAO_reverse( $seq_msd );

	/*$seq2 = FSA::from_array( array(
		'start' => array('0'),
		'final' => array('0','1','2','3','4','5','6','7'),
		'delta' => array(
			'0' => array('0' => array('1'), '1' => array('2')),
			'1' => array('0' => array('1'), '1' => array('1')),
			'2' => array('0' => array('1'), '1' => array('3')),
			'3' => array('0' => array('1'), '1' => array('4')),
			'4' => array('0' => array('5'), '1' => array('6')),
			'5' => array('0' => array('5'), '1' => array('5')),
			'6' => array('0' => array('5'), '1' => array('7')),
			'7' => array('0' => array('5'), '1' => array('0'))
		),
		'output' => array('0'=>'0', '1'=>'0', '2'=>'0', '3'=>'0', '4'=>'1', '5'=>'1', '6'=>'1', '7'=>'1' ),
		'k' => 2
	));*/

	$seq->mask = array('+4');
	
	// print out the first 50 terms of the sequence	
	for($i=0; $i<80; $i++) {
	 	$ans = dfa_accepts ($seq, array($i),  strlen($a) );
		echo $ans['output'];
	}
	echo "\n";//*/

	$name = $a . ',' .$b. ',' .$c;
	$log->entry($name, microtime(true) - $time_start );


	$filename = 'Ulimately-Period' . '_' . $name;
	$expr = parse_expr('
	(\exists, m,
		(\and, 
			(>, p, 0),
			(\forall, j,
				(\or,
					(<, j, m),
					(\out=, j, j+p)
				)
			)
		)
	)');

	$d = process_request( 
		$filename,
		$seq, 
		array('p'),
		$expr
	);

//continue;
	


//	show_and_save($d, $filename);


	if( count($d->end()) != 0 ) { // the sequence is Ultimately periodic
		$log->entry("\t is ultimately periodic.", microtime(true) - $time_start );
	} else {
		// ultimately periodic => quasi periodic so there is no point to check

		$filename = 'DenseCondensation' . '_' . $name;
	
		//$REUSE = false;
	
	
		$p = process_request( 
			$filename,
			$seq, 
			array('n','i','j'),
			array('\factor', 'n', 'i', 'j')
		);
		
			
		$q = process_request( 
			$filename,
			$seq, 
			array('n','s','j'),
			array('\forall', 'i',
				array('\or',
					array('<' ,'i', 's'),
					array('>=', 'i', 'j'),
					array('\not',
						array('\machine', array('n', 'i', 'j'), $p)
					)
				)
			)
		);
			
		

		$d = process_request( 
			$filename,
			$seq, 
			array('n'),
			array('\exists', 's',
			array('\exists', 'r',				
			array('\forall', 'j',
				array('\and',
					array('\or',	
						array('<', 'j', 's'),
						array('>', 'j', 'r'),
						array('\machine', array('n', 's', 'j'), $q)
					),
					array('\or',
						array('<=', 'j', 'r'),
						array('\not', 
							array('\machine', array('n', 's', 'j'), $q)
						)
					)
				)
			)
			)
			)
		);
		show_and_save($d, $filename);

		$d = process_request( 
			$filename,
			$seq, 
			array('n'),
			array('\exists', 'i',
				array('\exists', 'l',
					array('\and',
						array('\forall', 'j',
							array('\or',
								array('>=', 'j', 'l'),
								array('\not', 
									array('\exists', 'k',
										array('\and', 
											array('<', 'k', 'j'),
											array('\factor','n', 'i+j', 'i+k')
										)
									)
								)
							)
						),
						array('\forall', 'k',
							array('\exists', 'j',
								array('\and', 
									array('\factor', 'n', 'k', 'i+j'),
									array('<', 'j', 'l')
								)
							)
						)
					)
				)
			)
		);

		//*/

		show_and_save($d, $filename);
		if( count($d->end()) != 0 ) { // the sequence is Ultimately periodic
			$log->entry("\t is quasi periodic.", microtime(true) - $time_start );
		}
	
	}

}


?>
