<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


foreach( array('Stewart-choral'=>$SC, 'Paper-folding' => $PF, 'Mephisto-Waltz'=>$MW,'Thue-Morse' => $TM,'Baum-Sweet'=>$BS,'Rudin-Shapiro' => $RS,  'Period-Doubling' => $PD) as $name => $seq ) {
	echo $name . ":\n";

	for($i=0; $i<32; $i++ ){
	 	$ans = dfa_accepts ($seq, array($i),  $seq->k );
		echo $ans['output'];
	}
	echo "\n";

//continue;

$REUSE = false;
	$filename = 'Appearance' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n','m'),
		array('\min', 'm',
			array('\forall', 'j',
				array('\exists', 'l',
					array('\and', 
						array('\factor', 'n', 'l', 'j'),
						array('>=', 'm', 'n+l')
					)
				)
			)
		)
	);
	
	if(  $seq == $SS ) {

		for($i=0; $i<60; $i++) {
			for($j=0; $j<30*$i; $j++ ){
			 	$ans = dfa_accepts ($d, array($i,$j),  $d->k );
				if ($ans['accept']) echo  base_convert($i, 10, $d->k). ":". $i.",".$j.";\n";

			}
		}
		//exit;
	}


	//display_ratio($filename, $d); 

	show_and_save($d, $filename, "eps");

	$log_of = 'n-1';
	$log_is = 't';
	$lim = 0;
	if ($seq == $TM ) {
		$lim = 3;
		$relation =	array('=', '3*t+n', 'm+1');
		//$restriction = '2*t+1';
	} else if($seq == $RS) {
		$lim = 4;
		$relation =	array('=', '13*t+n', 'm+1');
	} else if($seq == $PF) {
		$relation =	array('=', '6*t+n', 'm+1');
		$lim = 5;
		$log_of = 'n';
		$restriction = 't';
	} else if($seq == $MW ) {
		$lim = 3;
		$log_is = '3*t';
		$relation =	array('\or', 
						array('\and', 
							//array('>', $log_of, '2*t'),
							array('\machine', array('n-2'), starts_with($seq->k, '2')),
							array('=', '18*t+n', 'm+1'),
						),
						array('\and',
							//array('<=', $log_of, '2*t'),
							array('\machine', array('n-2'), starts_with($seq->k, '1')),
							array('=', '17*t+n', 'm+1'),
						)
					);
	} else if($seq == $PD ) {
		$relation =	array('=', '3*t+n', 'm+1');
		$lim = 2;
		$log_is = '2*t';
		$log_of = 'n';
	} else if($seq == $SC ) {
		$log_of = 'n';
		$log_is = 't';
		$lim = 2;	
		$relation =	array('=', '5*t+2*n', '2*m+1');
	} else if($seq == $BS ) {
		$log_of = 'n-8';
		$log_is = '2*t';
		$lim = 13;

	//	fsa_visualize($log_even);

		$relation =	array('\or', 
						array('\and', 
							array('\machine', array('t'), $log_even),
							array('=', $log_of, $log_is),
							array('=', '46*t+n', 'm+1'),
						),
						array('\and',
							array('\or',
								array('\not', array('\machine', array('t'), $log_even)),
								array('\not', array('=', $log_of, $log_is))
							),							
							array('=', '23*t+n', 'm+1'),
						)
					);


	}

	$filename_confirm =  'Appearance_confirm_' . $name;

	$r = process_request(
		$filename_confirm,
		$seq,
		array('n'),
		array('\exists', 't',
			array('\exists', 'm',
				array('\and',
					array('\machine', array('n','m'), $d),
					array('>=', 'n', $lim),
					array('\machine', array($log_of, $log_is), ceil_log($seq->k)),
					$relation
				)
			)
		)

	);

/*	$r2 = process_request(
		$filename_confirm,
		$seq,
		array('n'),
		array('\and',
			array('>=', 'n', $lim),
			array('\not', 
				array('\machine', array('n'), $r)
			)
		)
	);

	show_and_save($r2, $filename_confirm);*/

	show_and_save($r, $filename_confirm);

	//exit;

	
	
}


?>
