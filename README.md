## DISCLAIMER

I know, I know, PHP? **Yuck!**

In my defense, I needed a language that is easy to prototype code in which ruled out C++ or Java, and I was fairly comfortable with PHP due to a side job.

The computation-intensive aspects of this code depend on a third-party library (FSA) while PHP is decent enough. At one point, I had to add a small java wrapper to speed up communication between FSA and my code.

# Automated Theorem Prover for *k*-Automatic Sequences

This theorem prover is one or two orders of magnitude faster than previous efforts which resulted in discovery and proofs of several hundred properties of various k-automatic sequences.

See my [thesis](https://uwspace.uwaterloo.ca/bitstream/handle/10012/7884/Goc_Daniel.pdf) for a more detailed explanation.


## Requirements

- PHP 5+ (including the php-cli)
- YAP
- [FSA 6.2](https://www.let.rug.nl/vannoord/Fsa/)

## Building

Open a terminal and execute the following commands:
```bash
sudo apt install yap
sudo apt install php-cli
mkdir fsa6
wget https://www.let.rug.nl/vannoord/Fsa/sources/fsa6-280_src.tar.gz
tar -xvf fsa6-280_src.tar.gz
```

Follow the instructions in `fsa\README`


## Run the program

From the main directory run:

```bash
php paper-folding.php
```

Or any of the other `.php` files. Each corrensponds to a different property of *k*-automatic sequences.
