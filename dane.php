<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


/*		array('\exists', 'j',
			array('\not',
				array('\exists', 'm',
					array('\and', 
						array('\forall', 'i',
							array('\or', 
								array('>=', 'i+m','n'),
								array('\out=', 'j+i', 'j+m+i')
							)
						),
						array('>=', '2*m', 'n'),
						array('<', 'm', 'n')
					)
				)
			)
		)
		*/
$REUSE = false;
foreach( array('Thue-Morse' => $TM, 'Paper-folding' => $PF, 'Rudin-Shapiro' => $RS ) as $name => $seq ) {
	echo $name . ":\n";

	$filename = 'Unbordered-factors' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('n'),
		array('\exists', 'j',
			array('\forall', 'l',
				array('\or', 
					array('\exists', 'i',
						array('\and', 
							array('<', 'i','l'),
							array('\not', array('\out=', 'j+i', 'j+n-l+i'))
						)
					),
					array('>', '2*l', 'n'),
					array('<', 'l', '1')
				)
			)
		)
	);

	show_and_save($d, $filename, 'eps');	
	if( $name == 'Paper-folding') fsa_visualize($d, 'results/'.$filename.'_reverse.dot');

}

?>
