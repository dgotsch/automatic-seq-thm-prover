<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


foreach( array( 'Thue-Morse' => $TM, 'Period-Doubling' => $PD,  'Paper-folding' => $PF, 'Rudin-Shapiro' => $RS) as $name => $seq ) {
	echo $name . ":\n";
//$REUSE = false;
$filename = 'UnborderedConjugates' . '_' . $name;



/*
array('n', 'j'),
		array('\and',
			array('\not',
				array('\exists', 'm',
					array('\and', 
						array('\forall', 'i',
							array('\or', 
								array('>=', 'i+m','n'),
								array('\out=', 'j+i', 'j+m+i')
							)
						),
						array('>=', '2*m', 'n'),
						array('<', 'm', 'n')
					)
				)
			),
			array('\not',
				array('\exists', 'l',
					array('\and',
						array('<', 'l', 'j'),
						array('\factor', 'n', 'l', 'j')
					)
				)
			),
			array('\machine', array('n-1'), $powerOfTwo),
			array('>=','n','5')
			/*,
			array('\exists', 't',
				array('\and',
					array('\machine', array('t'), $powerOfTwo),
					array('=', 'n','3*t')
				)
			)			
		)*/

$expr = parse_expr('
	(\and,
		(>, n, 0),
		(<, i, n),
		(\not, 
			(\exists, b,
				(\and,
					(>, b, 0),
					(<=, 2*b, n),
					(\forall, j,
						(\or,
							(\and, 
								(\out=, i+j, i+j-b),
								(>, n, i+j),
								(>=, i+j, b)
							),
							(\and, 
								(\out=, i+j, n+i+j-b),
								(>, n, i+j),
								(<, i+j, b)
							),
							(\and, 
								(\out=, i+j-n, i+j-b),
								(<=, n, i+j),
								(>=, i+j, b)
							),
							(\and, 
								(\out=, i+j-n, n+i+j-b),
								(<=, n, i+j),
								(<, i+j, b)
							),
							(>=, j, b)
						)
					)
				)
			)
		)
	)');



	$d = process_request( 
		$filename,
		$seq, 
		array('n', 'i'),
		$expr
	);


	//display_ratio($filename, $d); 
	
	show_and_save($d, $filename);
	
	$m =brzozowski(fsa_reverse($d));

	
	$A = fsa_matrix($m, 0, 2);
	//print_r($m->delta); exit;
	//print_r($A['0']);
	echo "v:\n";
	vector_write($A['v']);
	echo "w:\n";
	vector_write($A['w']);
	echo "M_0:\n";
	matrix_write($A['0']);
	echo "M_1:\n";
	matrix_write($A['1']);
	exit;


	exit;

}


?>
