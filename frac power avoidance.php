<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');




$expr = parse_expr('
(\exists, p,
	(\and,
		(>, p, 0),	
		(\forall, i0,
			(\or,
				(\out=, i0+i, i0+i+p),
				(>=, i0+p, n)
			)
		),
		(=, 15*p, 8*n)
	)
)
');

$expr2 = parse_expr('
(\exists, p,
	(\and,
		(\exists, q,
			(\and,
				(>, p, 0),	
				(=, q+p, n),
				(\factor, q, i, i+p)
			)
		),
		(<, 15*p, 8*n)
	)
)
');

$expr2 = parse_expr('
(\exists, p,
	(\and,
		(>, p, 0),	
		(\forall, i0,
			(\or,
				(\out=, i0+i, i0+i+p),
				(>=, i0+p, n)
			)
		),
		(<, 15*p, 8*n)
	)
)
');

$KG_msd =  DFAO_builder(array(
'0' => '012132310',
'1' => '123203021',
'2' => '230310132',
'3' => '301021203')
);

$KG = NFAO_reverse($KG_msd);
//echo $KG->states;


$NN_msd = DFAO_builder(array(
'0' => '01',
'1' => 'F2',
'2' => 'E3',
'3' => 'D4',
'4' => 'C5',
'5' => 'B6',
'6' => 'A7',
'7' => '98',
'8' => '89',
'9' => '7A',
'A' => '6B',
'B' => '5C',
'C' => '4D',
'D' => '3E',
'E' => '2F',
'F' => '10')
);

$NN = NFAO_reverse($NN_msd);

ini_set("memory_limit","16000M");

foreach( array( 'NeedsName' => $NN, 'Krieger' => $KG, 'Kurosaki' => $KS, 'Leech' => $LCH) as $name => $seq ) {
	echo $name . ":\n";
$REUSE = false;


	$den = 1;
	$num = 1;
	if ($seq == $LCH ) {
		$num = 15;
		$den = 8;
	} else if($seq == $TM) {
		$num = 2;
		$den = 1;
	} else if($seq == $KS) {
		$num = 7;
		$den = 4;
	} else if( $seq == $KG ) {
		$num = 3;
		$den = 2;
	} else if( $seq == $NN ) {
		$num = 4;
		$den = 3;
	}

	$filename = 'Power-Avoidance' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('i'),
		'(\exists, n, 
			(\exists, m, 
				(\and, 
					(>, n, 0),
					(>, '.$den.'*m, '.$num.'*n),
					(\factor, m-n, i, i+n)
				)
			)
		)'
	);
	show_and_save($d, $filename, 'eps');


	$filename = 'Power-Avoidance-confirm' . '_' . $name;
	$d = process_request( 
		$filename,
		$seq, 
		array('i','n'),
		//array('\exists', 'n', 
			array('\exists', 'm', 
				array('\and', 
					array('>', 'n', '0'),
					array('=', $den.'*m', $num.'*n'),
					array('\factor', 'm-n', 'i', 'i+n')
				)
			)
	//	)
	);

	show_and_save($d, $filename, 'eps');
//exit;

}




?>
