<?php

include('src/fsa_parse.php'); 
include('src/fsa_diagnostic.php');


foreach( array('Stewart-choral'=>$SC, 'Mephisto-Waltz'=>$MW, 'Period-Doubling' => $PD, 'Thue-Morse' => $TM, 'Paper-folding' => $PF, 'Rudin-Shapiro' => $RS, 'Baum-Sweet'=>$BS) as $name => $seq ) {
	echo $name . ":\n";
$REUSE = false;
$filename = 'Condensation' . '_' . $name;

	
	
	$d = process_request( 
		$filename,
		$seq, 
		array('n','m'),
		array('\min', 'm',
			array('\exists', 'k',
				array('\forall', 'j',
					array('\exists', 'l',
						array('\and', 
							array('\factor', 'n', 'l', 'j'),
							array('>=', 'm+k', 'n+l'),
							array('>=', 'l', 'k')
						)
					)
				)
			)
		)
	);


	if( true ||  $seq == $BS ) {

		for($i=0; $i<60; $i++) {
			for($j=0; $j<30*$i; $j++ ){
			 	$ans = dfa_accepts ($d, array($i,$j),  $d->k );
				if ($ans['accept']) echo  base_convert($i, 10, $d->k). ":". $i.",".$j.";\n";

			}
		}
		//exit;
	}
	//display_ratio($filename, $d); 
	
	show_and_save($d, $filename, "eps");



	$log_of = 'n-1';
	$log_is = 't';
	$lim = 3;
	if ($seq == $TM ) {
		$relation =	array('=', '2*t+2*n', 'm+2');
	} else if($seq == $RS) {
		$lim = 8;
		$relation =	array('=', '8*t+2*n', 'm+2');
	} else if($seq == $PF) {
		$relation =	array('=', '3*t+2*n', 'm+1');
		$lim = 7;
		$log_of = 'n';
	} else if($seq == $PD) {
		$relation =	array('=', 't+2*n', 'm+1');
		$lim = 2;
		$log_is = '2*t';
		$log_of = 'n';
	} else if($seq == $MW ) {
		$lim = 3;
		$log_is = '3*t';
		$relation =	array('\or', 
						array('\and', 
							array('\machine', array('n-2'), starts_with($seq->k, '1')),
							//array('<=', $log_of, '2*t'),
							array('=', '7*t+2*n', 'm+2'),
						),
						array('\and',
							//array('>', $log_of, '2*t'),
							array('\machine', array('n-2'), starts_with($seq->k, '2')),
							array('=', '9*t+2*n', 'm+2'),
						)
					);
	} else if($seq == $SC ) {
		$lim = 1;
		$log_is = '3*t';
		$log_of = 'n';
		$relation =	array('\or', 
						array('\and', 
							array('\machine', array('n-1'), starts_with($seq->k, '1')),
							array('=', '2*t+2*n', 'm+1'),
						),
						array('\and',
							array('\machine', array('n-1'), starts_with($seq->k, '2')),
							array('=', '3*t+2*n', 'm+1'),
						)
					);
	} else if($seq == $BS ) {
		$log_of = 'n-8';
		$log_is = '2*t';
		$lim = 13;
		
	//	fsa_visualize($log_even);

		$relation =	array('\or', 
						array('\and', 
							array('\machine', array('t'), $log_even),
							array('=', $log_of, $log_is),
							array('=', '46*t+n', 'm+1'),
						),
						array('\and',
							array('\or',
								array('\not', array('\machine', array('t'), $log_even)),
								array('\not', array('=', $log_of, $log_is))
							),							
							array('=', '23*t+n', 'm+1'),
						)
					);
	}

	$filename_confirm = 'Condensation_confirm_' . $name;

	$r = process_request(
		$filename_confirm,
		$seq,
		array('n'),
		array('\exists', 't',
			array('\exists', 'm',
				array('\and',
					array('\machine', array('n','m'), $d),
					array('>=', 'n', $lim),
					array('\machine', array($log_of, $log_is), ceil_log($seq->k)),
					$relation
				)
			)
		)

	);
	$d = $r;
		
		for($i=0; $i<60; $i++) {
			for($j=0; $j<30*$i; $j++ ){
			 	$ans = dfa_accepts ($d, array($i,$j),  $d->k );
				if ($ans['accept']) echo  base_convert($i, 10, $d->k). ":". $i.",".$j.";\n";

			}
		}
	show_and_save($r, $filename_confirm);



}

?>
